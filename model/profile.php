<?php 

require_once(CONTROLLER_PATH.'appcontroller.php');

class profileModel extends Appcontroller {
	private $conn;
	private $statement;
	private $userId;
	private $dbHost;
	private $dbUser;
	private $dbPass;
	private $errorMsg;
	function __construct() {

		parent::__construct();
		// load mysql connection with product admin config for product database
		include LIBRARY_ROOT . "adminconfig.php";
		
		$this->dbHost = $dbHost;
		$this->dbUser = $dbUser;
		$this->dbPass = $dbPass;

		try {
			$this->conn = new PDO ( "mysql:host={$this->dbHost};dbname={$dbName}", $this->dbUser, $this->dbPass );

		} catch ( Exception $e ) {
			
			// report error : connection error in profile model of Testcube
			$this->errorMsg =  "connection error in profile model";
			$this->errorReportObj->sendErrorReport($this->errorMsg);	
				
		}
	}


	public function __get($name) {
	
	}
	
	
	public function __set($name,$value) {
	
	}

	//change details of user in profile section
	public function profileDetails($arrData) {
			
		$return="";

		if(isset($arrData['oldPassword']) && !empty($arrData['oldPassword'])) { 
			if(isset($arrData['newPassword']) && !empty($arrData['newPassword'])) {
				if(isset($arrData['userId']) && !empty($arrData['userId'])) {
					$oldPassword = $arrData['oldPassword'];
					$newPassword = $arrData['newPassword'];
					$userId = $arrData['userId'];	
					$passwordUpdateQuery = "UPDATE users 
											SET    password = :newPassword 
											WHERE  password = :oldPassword 
											       AND id = :userId ";

					try{

						$this->statement = $this->conn->prepare($passwordUpdateQuery);
						$this->statement->bindValue(":newPassword", md5($newPassword), PDO::PARAM_STR);
						$this->statement->bindValue(":oldPassword", md5($oldPassword), PDO::PARAM_STR);
						$this->statement->bindValue(":userId", $userId, PDO::PARAM_INT);
						$this->statement->execute();
						$count = $this->statement->rowCount();
						if($count == 1){
							$return=1;
						} else {
							$return="";
						}
						

					} catch(PDOException $e) {
						// Send error email
						$this->errorMsg =  "PDOException in profile model's profileDetails function";
						$this->errorReportObj->sendErrorReport($this->errorMsg);
					}										       

				} else {
					$return="";
				}

			} else {
				$return="";
			}
		} else {
			$return="";
		}

		return $return;
		
	}


	// change profile detail
	public function profileSettings($arrData){

		$return="";
		
		if(isset($arrData['firstName']) && !empty($arrData['firstName'])){
			if(isset($arrData['userId']) && !empty($arrData['userId'])){
				if(isset($arrData['clientId']) && !empty($arrData['clientId'])){
				
					$firstName = $arrData['firstName'];
					$lastName = $arrData['lastName'];
					$contact = $arrData['contact'];
					$userId = $arrData['userId'];
					$clientId = $arrData['clientId'];
					
					$profileAdmindbSettingsQuery = "UPDATE user_profile 
													SET first_name = :firstName,
														last_name = :lastName,
														contact = :contact	 
													WHERE id = :userId ";	

					$profileClientdbSettingsQuery = "UPDATE clientdb0$clientId.users clientTable 
												       INNER JOIN user_profile adminTable 
												               ON clientTable.testcube_id = adminTable.id 
													SET clientTable.first_name = (SELECT first_name 
												                                 FROM   user_profile 
												                                 WHERE  id = :userId),
														clientTable.last_name = (SELECT last_name 
												                                 FROM   user_profile 
												                                 WHERE  id = :userId)";

						try{	

							$this->statement = $this->conn->prepare($profileAdmindbSettingsQuery);
							$this->statement->bindValue(':firstName',$firstName,PDO::PARAM_STR);
							$this->statement->bindValue(':lastName',$lastName,PDO::PARAM_STR);
							$this->statement->bindValue(':userId',$userId,PDO::PARAM_INT);
							$this->statement->bindValue(':contact',$contact,PDO::PARAM_INT);
							$this->statement->execute();
						
							$this->statement = $this->conn->prepare($profileClientdbSettingsQuery);
							$this->statement->bindValue(':userId',$userId,PDO::PARAM_INT);
							$this->statement->execute();
							
							$return = 1;
							} catch(PDOException $e){
								
								$this->errorMsg =  "PDOException in profile model's profileSettings function";
								$this->errorReportObj->sendErrorReport($this->errorMsg);


							}
				} else {
					$return=""; //clientid is null 
				}
												
			} else {
				$return=""; //userId is null	
			} 
			
		} else {
			$return =""; //first name is not loaded in model
		}

		return $return;	
	}



	public function profileLanguage($arrData){
		$return = "";
		if(isset($arrData['profileLanguage']) && !empty($arrData['profileLanguage'])){

			$langUpdateQuery = "UPDATE user_profile 
								SET    language = :profileLanguage,
									   theme = :profileTheme 
								WHERE  id = :userId ";

			try{
				$this->statement = $this->conn->prepare($langUpdateQuery);
				$this->statement->bindValue(":profileLanguage",$arrData['profileLanguage'],PDO::PARAM_STR);
				$this->statement->bindValue(":userId",$arrData['userId'],PDO::PARAM_INT);
				$this->statement->bindValue(":profileTheme",$arrData['profileTheme'],PDO::PARAM_INT); 
				$this->statement->execute();
				$return = 1;
			} catch(PDOException $e){
				$this->errorMsg =  "PDOException in profile model's profileSettings function";
				$this->errorReportObj->sendErrorReport($this->errorMsg);

			}
			

		} else {
			$return = 2;//server error
		}
		return $return;

	}

}



?>