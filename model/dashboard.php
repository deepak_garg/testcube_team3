<?php

require_once(CONTROLLER_PATH.'appcontroller.php');

class dashboardModel extends Appcontroller {
	private $conn;
	private $statement;
	private $userId;
	private $dbHost;
	private $dbUser;
	private $dbPass;
	private $errorMsg;
	function __construct() {

		parent::__construct();

		// load mysql connection with product admin config for product database
		include LIBRARY_ROOT . "adminconfig.php";
		$this->dbHost = $dbHost;
		$this->dbUser = $dbUser;
		$this->dbPass = $dbPass;
		try {
			$this->conn = new PDO ( "mysql:host={$this->dbHost};dbname={$dbName}", $this->dbUser, $this->dbPass );
		} catch ( Exception $e ) {	
			// report error : connection error in dashboard model of Testcube
			
			$this->errorMsg =  "connection error in dashboard model";
			$this->errorReportObj->sendErrorReport($this->errorMsg);

		}
	}

	public function __get($name) {
	
	}
	
	
	public function __set($name,$value) {
	
	}
	

	// this function will return the profile details
	public function getProfile($userId) {
			
		$return="";
		if(isset($userId) && !empty($userId)) {
			$this->userId = $userId;
		} else {
			return $return;
		}
		try {			
			$userProfileQuery = "SELECT usr.id AS id,
									       usr.email AS email,
									       usr.username AS username,
									       usr.registration_date AS regDate,
									       profile.first_name AS fname,
									       profile.last_name AS lname,
									       profile.theme AS theme,
									       CONCAT_WS(' ',profile.first_name,profile.last_name) AS name,
									       profile.contact AS contact,
									       profile.photo AS photo,
									       profile.language AS lang,
									       activity.last_login AS lastlogin
									FROM users AS usr
									INNER JOIN user_profile AS profile
									INNER JOIN user_activity AS activity ON usr.id = profile.user_id
									AND usr.id = activity.user_id
									AND activity.id =
									  (SELECT max(id)-1
									   FROM user_activity)
									WHERE usr.id = :userId ";
			
			$this->statement = $this->conn->prepare($userProfileQuery);
			$this->statement->bindValue(":userId", $this->userId, PDO::PARAM_INT);			
			//execute statement
			$this->statement->execute();
			$count = $this->statement->rowCount();
			
			if($count) {
				$userData = $this->statement->fetch(PDO::FETCH_ASSOC);
				$return = array(
					'id' => $userData['id'],
					'email' => $userData['email'], 
					'username' => $userData['username'],
					'name' => $userData['name'],
					'fname' => $userData['fname'],
					'lname' => $userData['lname'],
					'contact' => $userData['contact'],
					'photo' => $userData['photo'],
					'theme' => $userData['theme'],
					'lang' => $userData['lang'],
					'regDate' => $userData['regDate'],
					'lastlogin' => $userData['lastlogin']
					);
			}
		} catch(PDOExecption $e) {
			$return ="";
			// report error : Invalid userId in function dashboard model dashboard

			$this->errorMsg =  "PDOExecption in dashboard model's getProfile function";
			$this->errorReportObj->sendErrorReport($this->errorMsg);

		}
		return $return;
	}

	//this function will use to gett user's profile
	public function dashboard($userId) {
			
		$return="";
		if(isset($userId) && !empty($userId)) {
			$this->userId = $userId;
		} else {
			return $return;
		}
		$return = $this->getProfile($userId);
		return $return;
	}
			
}