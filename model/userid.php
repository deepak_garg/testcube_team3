<?php

	
	require_once(LIBRARY_ROOT.'clientdbconnection.php');
	require_once(CONTROLLER_PATH.'appcontroller.php');

	class useridModel extends Appcontroller
	{
		private $conn;
		private $dbUser;
		private $dbPass;
		private $dbHost;
		private $statement;		
		private $userId;
		private $errorMsg;

		public function __construct()
		{
			parent::__construct();
			// load mysql connection with product userconfig for product database
			$this->conn = clientDbConnection::$conn;
		
		}

		// this function is used to get the logged in users id for the client's users table
		// created_by and updated_by  should be referenced by the returned value of this function
		public function getUserId($userId)
		{

				$return="";
			$this->userId = $userId;
			$getUserIdQuery = "SELECT id 
								FROM   users 
								WHERE  testcube_id = :userid";

			try {

				$this->statement = $this->conn->prepare($getUserIdQuery);
				$this->statement->bindValue(':userid',$this->userId,PDO::PARAM_INT);
				$this->statement->execute();

				$count = $this->statement->rowCount();
				if($count == 1) {
					
					$userRow = $this->statement->fetch(PDO::FETCH_ASSOC);
					$return = $userRow['id'];
				}

			} catch (PDOException $e) {

					$this->errorMsg =  "pdoexception in useridModel model's getUserId function";
					$this->errorReportObj->sendErrorReport($this->errorMsg);

			}					

			return $return;

		}


	}


