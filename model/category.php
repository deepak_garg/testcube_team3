<?php

require_once(LIBRARY_ROOT.'clientdbconnection.php');
require_once(CONTROLLER_PATH.'appcontroller.php');
require_once(MODEL_PATH.'question.php');

class categoryModel extends Appcontroller {
	private $conn;
	private $dbHost;
	private $dbPass;
	private $dbUser;
	private $statement;
	private $catName;
	private $created_by;
	private $updated_by;
	private $created_on;
	private $updated_on;
	private $errorMsg;

	function __construct(){

		parent::__construct();

		// load mysql connection with product userconfig for product database
		$this->conn = clientDbConnection::$conn;
		
	}

	

	function __get($name){

	}

	function __set($name,$value){

	}


	// list all available(active) categories
	public function categoryList(){
	



		$return="";

		$selectCategoryList = "SELECT cat.id AS id, 
								       cat.name AS categoryName, 

								       Concat(usercreate.first_name, usercreate.last_name) AS created_by, 
								       cat.created_on AS catCreatedOn, 
								       Concat(userupdate.first_name, userupdate.last_name) AS updated_by,
								       cat.updated_on AS catUpdatedOn
								       
								FROM   category cat 
								       LEFT JOIN users usercreate 
										    ON cat.created_by = usercreate.id 
										LEFT JOIN users userupdate 
										    ON cat.updated_by = userupdate.id 
								    WHERE cat.status=0
								    ORDER BY cat.id DESC";

		try{
			$this->statement = $this->conn->prepare($selectCategoryList);
			$this->statement->execute();
			$catList = $this->statement->fetchAll(PDO::FETCH_ASSOC);
			$return = $catList;

			
//exit;

		} catch(PDOException $e){
			// send error using email
			$this->errorMsg =  "pdoexception in Category model's categoryList function";
			$this->errorReportObj->sendErrorReport($this->errorMsg);

		}						
		return $return;	
	}



	//used to retrieve category list for test module

	public function retrieveCatListForTest()
	{

			$catList = "";
			$categoryListQuery = "SELECT id,name 
									FROM category";

			try {

				$this->statement = $this->conn->prepare($categoryListQuery);
				$this->statement->execute();
				$catList =  $this->statement->fetchAll(PDO::FETCH_ASSOC);
			
			} catch(PDOExecption $e) {

					// send error using email
					$this->errorMsg =  "pdoexception in Category model's categoryList function";
					$this->errorReportObj->sendErrorReport($this->errorMsg);

			}	

			return $catList;					 

	}


// used to check for a particular category if it is available or not
	public function checkCategoryAvailable($categoryName){

		$checkCategoryStatus = "SELECT Count(name) AS count 
								FROM   category 
								WHERE  name = :catName 
								       AND status = 0 ";
		try{

			$this->statement = $this->conn->prepare($checkCategoryStatus);
			$this->statement->bindValue(":catName",$categoryName, PDO::PARAM_STR);
			$this->statement->execute();
			$count = $this->statement->fetch(PDO::FETCH_ASSOC);

			return $count['count'];

			} catch(PDOException $e){

			}
	}


// used to add new category 
	public function addCategory($arrData){

		$return="";
		if(isset($arrData['name']) && !empty($arrData['name'])) {
			$this->catName=$arrData['name'];
			$this->created_by=$arrData['created_by'];

			$catStatus = $this->checkCategoryAvailable($this->catName);
			// print_r($catStatus);
			// exit;
			if($catStatus == 0) {
				$newCategoryQuery = "INSERT INTO category 
								            (name, 
								             created_by, 
								             created_on) 
								VALUES      ( :catName, 
								              :created_by, 
								              Now() ) ";


					try{
						$this->statement = $this->conn->prepare($newCategoryQuery);
						$this->statement->bindValue(":catName", $this->catName, PDO::PARAM_STR);
						$this->statement->bindValue(":created_by", $this->created_by, PDO::PARAM_INT);
						$this->statement->execute();
						
						$return = 1;


					} catch(PDOException $e){
						
						//send error email
						$this->errorMsg =  "pdoexception in Category model's addCategory function";
						$this->errorReportObj->sendErrorReport($this->errorMsg);

					}
			} else {
				$return = 2;
			} 

				
	
		} else {
			$return = ""; 
		}
		
		return $return;
	}
	

	//used to retrieve category name for deletion
	public function delCatName($categoryId){
	


		$return="";

		$selectCategoryList = "SELECT  id,
								       name 
								FROM   category 
								
								WHERE id=:catid";

		try{
			$this->statement = $this->conn->prepare($selectCategoryList);
			$this->statement->bindValue(':catid',$categoryId,PDO::PARAM_INT);
			$this->statement->execute();
			$catList = $this->statement->fetch(PDO::FETCH_ASSOC);
			$return = $catList;
			

		} catch(PDOException $e){
			// send error using email
			$this->errorMsg =  "pdoexception in Category model's delCatName function";
			$this->errorReportObj->sendErrorReport($this->errorMsg);


		}						
		return $return;	
	}


// used to check if a question of a specific category is present or not
	public function checkQuestion($categoryId){

		$checkQuestionQuery="SELECT Count(id) AS count 
								FROM   questions 
								WHERE  cat_id = :categoryId 
								       AND status = 0 ";

		$this->statement = $this->conn->prepare($checkQuestionQuery);
		$this->statement->bindValue(":categoryId",$categoryId,PDO::PARAM_INT);
		$this->statement->execute();
		$count = $this->statement->fetch(PDO::FETCH_ASSOC);
		return $count['count'];								       
	}


// used to delete single category
	public function delCategory($categoryId)
		{
			$returnMsg = "";

			if(isset($categoryId) && !empty($categoryId)) {

				$countRes = $this->checkQuestion($categoryId);

				if($countRes == 0){
					$softDeleteCategoryQuery = "UPDATE category 
								  	  SET    status = 2 
								      WHERE  id = :categoryid";


					try {

							$this->statement = $this->conn->prepare($softDeleteCategoryQuery);
							$this->statement->bindValue(':categoryid',$categoryId,PDO::PARAM_INT);
							$this->statement->execute();
							$returnMsg = 1;

						} catch(PDOExecption $e) {

							// PDO Exception // send error email

							$this->errorMsg =  "pdoexception in Category model's delCategory function";
							$this->errorReportObj->sendErrorReport($this->errorMsg);

					}		
				} else {
					$returnMsg = 2; // first delete questions error
				}
				
							  

					
				
			} else {
				$returnMsg = ""; // error
			}



			
			return $returnMsg;
			
		}

// used to retrieve category name for editing
		public function editCatName($categoryId){
	


		$return="";

		$selectCategoryList = "SELECT  id,
								       name 
								FROM   category 
								
								WHERE id=:catid";

		try{
			$this->statement = $this->conn->prepare($selectCategoryList);
			$this->statement->bindValue(':catid',$categoryId,PDO::PARAM_INT);
			$this->statement->execute();
			$catList = $this->statement->fetch(PDO::FETCH_ASSOC);
			$return = $catList;

		} catch(PDOException $e){
			// send error using email

						$this->errorMsg =  "pdoexception in Category model's editCatName function";
						$this->errorReportObj->sendErrorReport($this->errorMsg);	
		}						
		return $return;	
	}

		
// used to edit and save the updated category
		public function editCategory($categoryData)
		{

			$return="";
			if(isset($categoryData['categoryname']) && !empty($categoryData['categoryname'])
			   && isset($categoryData['updated_by']) && !empty($categoryData['updated_by'])
			   && isset($categoryData['categoryid']) && !empty($categoryData['categoryid'])) {

				
				$updateCatQuery="UPDATE category 
									set name=:newCatName,
									updated_by =:updated_by	
										 where id =:categoryid";	
				try{

			$this->statement = $this->conn->prepare($updateCatQuery);
			$this->statement->bindValue(':newCatName',$categoryData['categoryname'],PDO::PARAM_STR);
			$this->statement->bindValue(':categoryid',$categoryData['categoryid'],PDO::PARAM_INT);
			$this->statement->bindValue(':updated_by',$categoryData['updated_by'],PDO::PARAM_INT);
			$this->statement->execute();

			$return=1;


		} catch(PDOException $e){
			
						$this->errorMsg =  "error in Category model's editCategory function";
						$this->errorReportObj->sendErrorReport($this->errorMsg);

		}						
		return $return;	

			}


		}


	// 	public function viewCat($categoryId){
	


	// 	$return="";

	// 	$selectCategoryList = "SELECT  id,
	// 							       name ,created_by,created_on,updated_by,updated_on,status
	// 							FROM   category 
								
	// 							WHERE id=:catid";

	// 	try{
	// 		$this->statement = $this->conn->prepare($selectCategoryList);
	// 		$this->statement->bindValue(':catid',$categoryId,PDO::PARAM_INT);
	// 		$this->statement->execute();
	// 		$catList = $this->statement->fetch(PDO::FETCH_ASSOC);
	// 		$return = $catList;

	// 	} catch(PDOException $e){
	// 		// send error using email

	// 					$this->errorMsg =  "pdoexception in Category model's viewCat function";
	// 					$this->errorReportObj->sendErrorReport($this->errorMsg);
	// 	}						
	// 	return $return;	
	// }


// used to delete multiple categories
	public function deleteMulCat($categoryId)
		{
			$returnMsg = 0;

			if(isset($categoryId) && !empty($categoryId)) {
				foreach ($categoryId as $key => $value) {
					$softDeleteCategoryQuery = "UPDATE category 
								  	  SET    status = 2 
								      WHERE  id = :categoryid";


				try {

						$this->statement = $this->conn->prepare($softDeleteCategoryQuery);
						$this->statement->bindValue(':categoryid',$value,PDO::PARAM_INT);
						$this->statement->execute();
						

					} catch(PDOExecption $e) {

						// PDO Exception // send error email
						$this->errorMsg =  "pdoexception in Category model's deleteMulCat function";
						$this->errorReportObj->sendErrorReport($this->errorMsg);
					}					  

					

				}
				
				$returnMsg = 1;
				
			}
			
			return $returnMsg;
			
		}

		// used to the get updates Data
		public function getUpdates()
		{

				$updates = '';
				$getUpdatesQuery = "SELECT  id,
				                            activity_performed,
				                            entity_created,
										    created_what,
											 created_by,
											 created_when
									FROM activity ORDER By id DESC 
									LIMIT 10";		

			try{

				$this->statement = $this->conn->prepare($getUpdatesQuery);
				$this->statement->execute();	
				$updates = $this->statement->fetchAll(PDO::FETCH_ASSOC);

			}catch (PDOExecption $e) {

			}					
	         
	         return $updates;

	   }

}



