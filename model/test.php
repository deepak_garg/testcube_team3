<?php

	require_once(LIBRARY_ROOT.'clientdbconnection.php');
	require_once(CONTROLLER_PATH.'appcontroller.php');

	class testModel extends Appcontroller
	{

		private $conn;
		private $dbHost;
		private $dbUser;
		private $dbPass;
		private $statement;
		private $testName;
		private $testDescription;
		private $testCreated_by;
		private $testUpdated_by;
		private $errorMsg;

		
		// used for connecting database
		public function __construct()
		{

			parent::__construct();
			$this->conn = clientDbConnection::$conn;
		
		}

		function __get($name) {
	
		}
	
		function __set($name,$value) {
	
		}

		// it is used to retrieve the test lists
		public function showTestList()
		{

			$selectTestListQuery = "SELECT test.id,
											test.name,
											test.description, 
										       Concat(usercreate.first_name, usercreate.last_name) AS created_by, 
										       test.created_on, 
										       Concat(userupdate.first_name, userupdate.last_name) AS updated_by, 
										       test.updated_on 
										FROM   test test 
										       LEFT JOIN users usercreate 
										              ON test.created_by = usercreate.id 
										       LEFT JOIN users userupdate 
										              ON test.updated_by = userupdate.id 
										WHERE  test.status = 0 
										ORDER BY test.id DESC"; 

			try{

				$this->statement = $this->conn->prepare($selectTestListQuery);
				$this->statement->execute();

				$testData = $this->statement->fetchAll(PDO::FETCH_ASSOC);

				/*echo '<pre />';
				print_r($testData);*/

			} catch(PDOExecption $e) {

					$this->errorMsg =  "pdoexception in test model's showTestList function(unable to retrieve test lists)";
					$this->errorReportObj->sendErrorReport($this->errorMsg);

				}
	
			return $testData;
			// function end
		}


		//it is used to insert new test data into the database
		public function addTest($testData)
		{
			
			$returnMsg = 0;
			
			// checking whether the data from controller is set or not
			if(isset($testData['testname']) && !empty($testData['testname']) 
				&& isset($testData['testdescription']) && !empty($testData['testdescription'])
				&& isset($testData['created_by']) && !empty($testData['created_by'])) {

					$this->testName = $testData['testname'];
					$this->testDescription = $testData['testdescription'];
					$this->testCreated_by = $testData['created_by'];

					// if data is set then insert into the database	
					try {	
							$testInsertQuery = "INSERT INTO test 
												            (name, 
												             description, 
												             created_by, 
												             created_on) 
												VALUES     (:name, 
												            :description, 
												            :created_by, 
												            NOW())";
							
				
							$this->statement = $this->conn->prepare($testInsertQuery);

							$this->statement->bindValue(':name',$this->testName,PDO::PARAM_STR);
							$this->statement->bindValue(':description',$this->testDescription,PDO::PARAM_STR);
							$this->statement->bindValue(':created_by',$this->testCreated_by,PDO::PARAM_INT);

							$this->statement->execute();

							} catch(PDOExecption $e) {

									// send error mail

								$this->errorMsg =  "pdoexception in test model's addTest function(unable to add new test in the database)";
								$this->errorReportObj->sendErrorReport($this->errorMsg);

									}

							$returnMsg = 1;			

							}
				
					return $returnMsg;
					// function end

		}



		// it is used to change the status of the test to 2(soft deleted)
		public function deleteTest($testId)
		{
	
			$returnMsg = 0;

			if(isset($testId) && !empty($testId)) {

				$softDeleteTestQuery = "UPDATE test 
								  	  SET    status = 2 
								      WHERE  id = :testid";


				try {
						$this->statement = $this->conn->prepare($softDeleteTestQuery);
						$this->statement->bindValue(':testid',$testId,PDO::PARAM_INT);
						$this->statement->execute();

					} catch(PDOExecption $e) {

						// PDO Exception // send error email

						$this->errorMsg =  "pdoexception in test model's deleteTest function(unable to remove test in the database)";
						$this->errorReportObj->sendErrorReport($this->errorMsg);
				}					  

				$returnMsg = 1;
			}
			return $returnMsg;
			
		}


		// it is used to change the status of multiple test to 2 (soft Delete)

		public function multipleDeleteTest($testId)
		{

			 $returnMsg = 0;

			 $softDeleteTestQuery = "UPDATE test 
								  	  SET    status = 2 
								      WHERE  id = :testid";


			 if(isset($testId) && !empty($testId)) {

			 	  foreach ($testId as $key => $value) {
			 	   		
			 	  		try {

			 	  			$this->statement =  $this->conn->prepare($softDeleteTestQuery);
			 	  			$this->statement->bindValue(':testid',$value);
			 	  			$this->statement->execute();

			 	  		} catch (PDOExecption $e) {

			 	  			$this->errorMsg =  "pdoexception in test model's multipleDeleteTest function(unable to remove tests in the database)";
							$this->errorReportObj->sendErrorReport($this->errorMsg);
			 	  		}
			 	   } 

			 	   $returnMsg = 1;
			 }	

			 return $returnMsg;

		}



		// it is used to update test data  after editing
		public function editTest($testUpdatedData)
		{

			$returnMsg = 0;

			if(isset($testUpdatedData['testname']) && !empty($testUpdatedData['testname'])
			   && isset($testUpdatedData['testdescription']) && !empty($testUpdatedData['testdescription'])
			   && isset($testUpdatedData['updated_by']) && !empty($testUpdatedData['updated_by'])
			   && isset($testUpdatedData['testid']) && !empty($testUpdatedData['testid'])) {

				$editTestQuery =  "UPDATE test 
								      SET name = :testname, 
								 		  description = :testdescription,
								 		  updated_by = :updated_by 
								     WHERE id = :testid";

				try {

					$this->statement = $this->conn->prepare($editTestQuery);
					$this->statement->bindValue(':testname',$testUpdatedData['testname'],PDO::PARAM_STR);
					$this->statement->bindValue(':testdescription',$testUpdatedData['testdescription'],PDO::PARAM_STR);	
					$this->statement->bindValue(':updated_by',$testUpdatedData['updated_by'],PDO::PARAM_INT);
					$this->statement->bindValue(':testid',$testUpdatedData['testid'],PDO::PARAM_INT);
					$this->statement->execute();

				} catch (PDOExecption $e) {

						$this->errorMsg =  "pdoexception in test model's editTest function(unable to edit test in the database)";
						$this->errorReportObj->sendErrorReport($this->errorMsg);

					}	

				$returnMsg = 1;

			}

			return $returnMsg;
			// function end

		}


		// used to retrieve test name for displaying on the test_details page
		// input - testid
		// output - name of the test
		public function retrieveTestName($testId) {

			$testName="";

			$retrieveTestNameQuery = "SELECT name 
										FROM   test 
										WHERE  id = :testid";


			if(isset($testId) && !empty($testId)) {

				try {
					$this->statement = $this->conn->prepare($retrieveTestNameQuery);
					$this->statement->bindValue(':testid',$testId,PDO::PARAM_INT);
					$this->statement->execute();
					$testName = $this->statement->fetch(PDO::FETCH_ASSOC);
				} catch (PDOExecption $e) {

					$this->errorMsg =  "pdoexception in test model's retrieveTestName function";
					$this->errorReportObj->sendErrorReport($this->errorMsg);

				}	
			}							

			return $testName;
		}	


		// used to retrieve questions for the particular test
		// input - test id
		// output - list of questions in the given test
			public function retrieveQuesForTest($testId) 
			{
					
				$returnData = "";

				$retrieveQuesForTestQuery = "SELECT ques.id,
													ques.description, 
											        cate.name,
											        ques.status 
											FROM   assigned_questions assques 
											       INNER JOIN questions ques 
											               ON assques.ques_id = ques.id 
											       INNER JOIN category cate 
											               ON cate.id = ques.cat_id 
											WHERE  assques.test_id =:testid
													AND ques.status=0";

				if(isset($testId) && !empty($testId)) {

					try {

						$this->statement = $this->conn->prepare($retrieveQuesForTestQuery);
						$this->statement->bindValue(':testid',$testId,PDO::PARAM_INT);
						$this->statement->execute();
						$returnData = $this->statement->fetchAll(PDO::FETCH_ASSOC);	
					} catch (PDOExecption $e) {

						$this->errorMsg =  "pdoexception in test model's retrieveQuesForTest function";
						$this->errorReportObj->sendErrorReport($this->errorMsg);	

					}
					
				}

				
				return $returnData;
			}


			// used to retrieve all questions from the question bank except those which are assigned in 
			// the given test.
			// input - testid 
			// output - list of questions from the question bank.

			public function retrieveAllQuestions($testId)
			{
				 $returnData = "";

				 $retrieveAllQuestionsQuery = "SELECT  ques.id, 
												       ques.description, 
												       cate.name 
												FROM   questions ques 
												       INNER JOIN category cate 
												               ON ques.cat_id = cate.id 
												WHERE  ques.id NOT IN(SELECT ques_id 
												                   FROM   assigned_questions 
												                   WHERE  test_id =:testid
												                   		   AND status=0) 
																AND ques.status=0";	

				 if(isset($testId) && !empty($testId)) {

					try {

						$this->statement = $this->conn->prepare($retrieveAllQuestionsQuery);
						$this->statement->bindValue(':testid',$testId,PDO::PARAM_INT);
						$this->statement->execute();
						$returnData = $this->statement->fetchAll(PDO::FETCH_ASSOC);	
					} catch (PDOExecption $e) {

							$this->errorMsg = "pdoexception in test model's retrieveAllQuestions function";
							$this->errorReportObj->sendErrorReport($this->errorMsg);	

					}
					
				}

				/*echo '<pre/>';
				print_r($returnData);*/

				return $returnData;

			}


			// used to assign questions to the given test
			// input- array of question ids and test id from controller
			// output - 1 on success , 0 on failure
			public function assignQuestionsToTest($questionForTest)
			{

				$returnMsg = 0;

				/*echo '<pre />';
						print_r($questionsForTest);
						exit;*/


				$assignQuestionsToTestQuery = "INSERT INTO assigned_questions 
													            (test_id, 
													             ques_id) 
													VALUES     (:testid, 
													            :questionid)";


				if(isset($questionForTest['quesid']) && !empty($questionForTest['quesid'])) {

						$questionsId = $questionForTest['quesid'];
						$testId = $questionForTest['testid'];

						foreach ($questionsId as $key => $value) {
							
								try {

									$this->statement = $this->conn->prepare($assignQuestionsToTestQuery);
									$this->statement->bindValue(':testid',$testId,PDO::PARAM_INT);
									$this->statement->bindValue(':questionid',$value,PDO::PARAM_INT);
									$this->statement->execute();	

								} catch (PDOExecption $e) {

									$this->errorMsg = "pdoexception in test model's assignQuestionsToTest function";
									$this->errorReportObj->sendErrorReport($this->errorMsg);	
	

								}		
						}

							$returnMsg = 1;	
				}

				return $returnMsg;
		}



		//used to delete the given question from test
		// input-question id and test id
		// output- 1 for success or 0 for failure

		public function removeQuestionFromTest($questionIdTestId) {

				$returnMsg = 0;

				$removeQuestionFromTestQuery = "UPDATE assigned_questions 
													SET    status = 2 
													WHERE  test_id = :testid 
													       AND ques_id = :quesid;"; 

				$testId = $questionIdTestId['testid'];
				$quesId = $questionIdTestId['quesid'];
				
				try {

					$this->statement = $this->conn->prepare($removeQuestionFromTestQuery);
					$this->statement->bindValue(':testid',$testId,PDO::PARAM_INT);
					$this->statement->bindValue(':quesid',$quesId,PDO::PARAM_INT);
					$success = $this->statement->execute();

					if($success) {
						$returnMsg = 1;
					}		

				} catch (PDOExecption $e) {

					$this->errorMsg = "pdoexception in test model's removeQuestionFromTest function";
					$this->errorReportObj->sendErrorReport($this->errorMsg);	

				}
				
				return $returnMsg;
													
		}

		public function retrieveLinkName($testId)
		 {
			$return = "";
			$success ="";

			$linkNameQuery = "SELECT 
									link.id,
									link.name
								FROM 
									link 
									INNER JOIN test
								ON link.test_id = test.id
								WHERE 
									test.id = :testId";
			try {

					$this->statement = $this->conn->prepare($linkNameQuery);

					$this->statement->bindValue(':testId',$testId,PDO::PARAM_INT);

					$success = $this->statement->execute();

					if($success) {
						$return =$this->statement->fetchAll(PDO::FETCH_ASSOC);	
					}		

				} catch (PDOExecption $e) {

					$this->errorMsg = "pdoexception in test model's retrieveLinkName function";
					$this->errorReportObj->sendErrorReport($this->errorMsg);	

				}

				return $return;
		}
		
		public function getSettings($linkId) 
		{

			$return ="";

			if(isset($linkId) && !empty($linkId)) {

				$linkSettingsQuery = "SELECT 
											availability,
											attempts,
											password,
											firstname,
											lastname,
											email,
											guidelines,
											custom_instructions_id,
											time_limit,
											total_question,
											randomize,
											answer_mandatory,
											passing_marks,
											email_result

									FROM 
										link_settings
									WHERE 
										link_id = :linkId";

			try {

					$this->statement = $this->conn->prepare($linkSettingsQuery);
					$this->statement->bindValue(':linkId',$linkId,PDO::PARAM_INT);
					$success = $this->statement->execute();

					if($success) {


						$return = $this->statement->fetch(PDO::FETCH_ASSOC);
							

					}		

				} catch (PDOExecption $e) {

					$this->errorMsg = "pdoexception in test model's retrieveLinkName function";
					$this->errorReportObj->sendErrorReport($this->errorMsg);	
				}


			


			} 
			return $return;


		}

		public function linkDetails($testId)
		{
			$return ="";

			$arrReturn =array();
			$arrLinkDetail = array();
			$testDetail = $this->retrieveTestName($testId);
			$testQue = $this->retrieveQuesForTest($testId);
			$totalQue = count($testQue);
			if(isset($testDetail) && !empty($testDetail)) {
				$linkData = $this->retrieveLinkName($testId);


				if(isset($linkData) && !empty($linkData)) {
					foreach ($linkData as $row) {
						$arrLinkDetail['id'] = $row['id'];
						$arrLinkDetail['name'] = $row['name'];
						$settings = $this->getSettings($row['id']);
						$arrLinkDetail['settings'] = $settings;
						$arrReturn[]= $arrLinkDetail;
					}			
				} else {
					$arrReturn ="";
				}
			}
			return array (
					'testId' =>$testId ,
					'testName' =>  $testDetail['name'],
					'totalQue' => $totalQue,
					'linkDetails' => $arrReturn
					);

		}

		// this function returns the name of a link
		public function getLinkDetail($linkId)
		{
			$return ="";
			$success ="";
			$linkNameQuery = "SELECT 
									id,
									name
								FROM 
									link 									
								WHERE 
									id = :linkId";
			try {

					$this->statement = $this->conn->prepare($linkNameQuery);

					$this->statement->bindValue(':linkId',$linkId,PDO::PARAM_INT);

					$success = $this->statement->execute();

					if($success) {
						$return =$this->statement->fetch(PDO::FETCH_ASSOC);	
					}		

				} catch (PDOExecption $e) {

					$this->errorMsg = "pdoexception in test model's getLinkDetail function";
					$this->errorReportObj->sendErrorReport($this->errorMsg);	

				}
				
				return $return;
			
		}

		// this function will create a code for the link (the url code of the test)
		public function generateLinkCode($arrData)
		{

			$return ="";
			if(isset($arrData) && !empty($arrData)) {

				$newCodeQuery = "INSERT INTO link_assign_dates(
												link_id,
												link_code,
												showfrom,
												showuntill)
									VALUES(:linkId,
											:linkCode,
											:fromDate,
											:toDate)";
				try {


						$this->statement = $this->conn->prepare($newCodeQuery);
						$this->statement->bindValue(':linkId',$arrData['linkId'],PDO::PARAM_INT);
						$this->statement->bindValue(':linkCode',$arrData['linkCode'],PDO::PARAM_STR);
						$this->statement->bindValue(':fromDate',$arrData['from'],PDO::PARAM_STR);
						$this->statement->bindValue(':toDate',$arrData['to'],PDO::PARAM_STR);

						$success = $this->statement->execute();

						if($success) {
							$return =1;
						} else {
							$return =0;
						}	

					} catch (PDOExecption $e) {

						$this->errorMsg = "pdoexception in test model's generateLinkCode function";
						$this->errorReportObj->sendErrorReport($this->errorMsg);	

					}
				} else {
					$return =0;
				}
					
				return $return;
		}
			
		public function quizDetails($linkId)
		{
			$return ="";

			if(isset($linkId) && !empty($linkId)) {



				$quizQuery = "SELECT 									
									linkDetail.testId AS testId,
									linkDetail.testName AS testName,
									linkDetail.linkId AS linkId,
									setting.custom_instructions_id AS guidline,
									setting.time_limit AS time,
									setting.display_questions AS queDisplay,
									setting.randomize AS randomize,
									setting.passing_marks AS passMarks,
									setting.total_question as queTotal 
								FROM
									(select 
										test.id AS testId,
										test.name AS testName,
										link.id AS linkId
									 from 
									 	link 
									 inner join 
									 	test 
									 on 
									 	link.test_id= test.id 
									 where 
									 	link.id=:linkId) linkDetail  
								 
									 INNER JOIN link_settings setting
								ON
									 linkDetail.linkId = setting.link_id ";
				
								
				try {


						$this->statement = $this->conn->prepare($quizQuery);
						$this->statement->bindValue(':linkId',$linkId,PDO::PARAM_INT);
						
						
						$success = $this->statement->execute();

						


						if($success) {

						$return = $this->statement->fetchAll(PDO::FETCH_ASSOC);

						

						} else {
							$return ="";
						}	

					} catch (PDOExecption $e) {

						$this->errorMsg = "pdoexception in test model's quizDetails function";
						$this->errorReportObj->sendErrorReport($this->errorMsg);	

					}
				} else {
					$return ="";
				}
					
				return $return;
		}

		//returns array of questions id for the quiz
		public function getArrQuestion($data)
		{
			$return ="";
			$testId ="";
			$randomize = "";
			$totalQue = "";
			if(isset($data) && !empty($data)) {
		
				$testId = $data['testId'];
				$totalQue = $data['queTotal'];
				$randomize = $data['randomize'];
				$arrQue = $this->retrieveQuesForTest($testId);
				$arrQueId = array();
				foreach ($arrQue as $x) {
					$arrQueId[] = $x['id'];
				}
				if($randomize == 1) {
					shuffle($arrQueId);
				}
				$return = array_slice($arrQueId, 0,$totalQue);
				
			}

			return $return;
		}

		//register student for test
		public function registerStudent($arrData)
		{
			$return ="";
			if(isset($arrData) && !empty($arrData)) {

				$studQuery = "INSERT INTO student(
											fname,
											lname,
											email)
									VALUES (
										 	:fname,
										 	:lname,
										 	:email)";
					try {


						$this->statement = $this->conn->prepare($studQuery);
						$this->statement->bindValue(':fname',$arrData['fname'],PDO::PARAM_STR);
						$this->statement->bindValue(':lname',$arrData['lname'],PDO::PARAM_STR);
						$this->statement->bindValue(':email',$arrData['email'],PDO::PARAM_STR);
						
						$success = $this->statement->execute();
						
						if($success) {
							$return = $this->conn->lastInsertId();
						} else {
							$return ="";
						}	

					} catch (PDOExecption $e) {

						$this->errorMsg = "pdoexception in test model's registerStudent function";
						$this->errorReportObj->sendErrorReport($this->errorMsg);	

					}
			} else {
				$return ="";
			}
					
			return $return;

		}


		public function getLinkCodeTime($code) {

				$data="";

				$getLinkCodeTimeQuery = "SELECT showfrom,showuntill FROM link_assign_dates WHERE link_code=:code";

				try {

						$this->statement = $this->conn->prepare($getLinkCodeTimeQuery);
						$this->statement->bindValue(':code',$code,PDO::PARAM_STR);
						$this->statement->execute();
						$data = $this->statement->fetch(PDO::FETCH_ASSOC);

				} catch(PDOExecption $e) {

						$this->errorMsg = "pdoexception in test model's getLinkCodeTime function";
						$this->errorReportObj->sendErrorReport($this->errorMsg);	

				}

				return $data;	

		}

		public function recentLink($data){


			$return="";

				$recentLinkQuery = "SELECT users.id AS clientid, 
									       link.id AS linkid, 
									       link_assign_dates.id AS id,
									       link_assign_dates.showfrom AS start,
									       link_assign_dates.showuntill AS stop,
									       Concat('http://local.testcube.com/quiz/',users.id,'/',link.id,'/', link_assign_dates.link_code)  AS linkurl
									FROM   link INNER JOIN 
									       link_assign_dates INNER JOIN  
									       users 
									ON  link.id = link_assign_dates.link_id 
									
									ORDER BY link_assign_dates.id LIMIT 3";

			try{

				$this->statement = $this->conn->prepare($recentLinkQuery);
				$this->statement->bindValue(':id',$data,PDO::PARAM_INT);
				$this->statement->execute();
				$linkData = $this->statement->fetchAll(PDO::FETCH_ASSOC);
			} catch(PDOExecption $e) {

						$this->errorMsg = "pdoexception in test model's recentLink function";
						$this->errorReportObj->sendErrorReport($this->errorMsg);	

			}
			// echo "<pre>";
			// print_r($linkData);
			// exit;
			
			return $linkData;
		}

		public function linkHistory($data){


			$return="";


					$recentLinkQuery = "SELECT users.id AS clientid, 
										link.id AS linkid, 
										link_assign_dates.id AS id, 
										link_assign_dates.showfrom AS start, 
										link_assign_dates.showuntill AS stop, 
										concat('http://local.testcube.com/quiz/',users.id,'/',link.id,'/', link_assign_dates.link_code) AS linkurl 
										from link INNER JOIN 
										link_assign_dates INNER JOIN users 
										ON link.id = link_assign_dates.link_id 
										and link.id = :id";


			try{

				$this->statement = $this->conn->prepare($recentLinkQuery);
				$this->statement->bindValue(':id',$data,PDO::PARAM_INT);
				$this->statement->execute();
				$linkData = $this->statement->fetchAll(PDO::FETCH_ASSOC);
			} catch(PDOExecption $e) {

						$this->errorMsg = "pdoexception in test model's recentLink function";
						$this->errorReportObj->sendErrorReport($this->errorMsg);	

			}
			// echo "<pre>";
			// print_r($linkData);
			// exit;
			return $linkData;
		}



		
		public function recentTest()
		{

			$selectTestListQuery = "SELECT id,
											name
										FROM test
										WHERE status = 0 
										ORDER BY id DESC LIMIT 3"; 

			try{

				$this->statement = $this->conn->prepare($selectTestListQuery);
				$this->statement->execute();

				$testData = $this->statement->fetchAll(PDO::FETCH_ASSOC);

				/*echo '<pre />';
				print_r($testData);*/

			} catch(PDOExecption $e) {

					$this->errorMsg =  "pdoexception in test model's showTestList function(unable to retrieve test lists)";
					$this->errorReportObj->sendErrorReport($this->errorMsg);

				}
		
			return $testData;
			// function end
		}



}

