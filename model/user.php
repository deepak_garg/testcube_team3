
<?php

require_once(CONTROLLER_PATH.'appcontroller.php');

class userModel extends Appcontroller {
	private $conn;
	private $statement;
	private $username;
	private $password;
	private $email;
 	private $dbHost;
 	private $dbUser;
 	private $dbPass;
 	private $errorMsg;
	function __construct() {

		parent::__construct();
		// load mysql connection with product admin config for product database
		include LIBRARY_ROOT . "adminconfig.php";
		$this->dbHost = $dbHost;
		$this->dbUser = $dbUser;
		$this->dbPass = $dbPass;
		try {
			$this->conn = new PDO ( "mysql:host={$this->dbHost};dbname={$dbName}", $this->dbUser, $this->dbPass );
		} catch ( Exception $e ) {


			// report error : connection error in user model of Testcube
			$this->errorMsg =  "connection exception in User model";
			$this->errorReportObj->sendErrorReport($this->errorMsg);

			// report error : connection error in model:user of Testcube
			
			


		}
	}
	
	
	public function __get($name) {
	
	}
	
	
	public function __set($name,$value) {
	
	}

	//this function will create a new client account
	public function createAccount($arrData) {
		global $conn;
		$return="";
		if(isset($arrData['username']) && !empty($arrData['username'])) {
			$this->username = $arrData['username'];
		} else {
			$this->username = "";
		}
		if(isset($arrData['email']) && !empty($arrData['email'])) {
			$this->email = $arrData['email'];
		} else {
			$this->email = "";
		}
		if(isset($this->username) && !empty($this->username)
			|| isset($this->email) && !empty($this->email)) {

			// generate a new 10 digits access code
			include_once LIBRARY_ROOT . "passwordGenerator.php";
			
			$this->password = generate_password();
			
			try {			
			$newUserQuery = "INSERT INTO users
					            (email,
					             username,
					             password,
					             registration_date)
					VALUES     (:email,
							:username,
					            :password,
					            NOW())";
			
			$this->statement = $this->conn->prepare($newUserQuery);

			$this->statement->bindValue(":email", $this->email, PDO::PARAM_STR);
			$this->statement->bindValue(":username", $this->username, PDO::PARAM_STR);
			$this->statement->bindValue(":password", md5($this->password), PDO::PARAM_STR);
			
			//start transaction
			$this->conn->beginTransaction(); 
			//create new account
			$this->statement->execute();
			//get the clientId 
			$clientId = $this->conn->lastInsertId();
			
			if(isset($clientId) && !empty($clientId)) {

				// load profile with default configurations
				include_once LIBRARY_ROOT . "defaultProfile.conf.php";
				if(isset($arrData['firstname']) && !empty($arrData['firstname'])) {
					$firstName = $arrData['firstname'];
				}
				if(isset($arrData['lastname']) && !empty($arrData['lastname'])) {
					$lastName = $arrData['lastname'];
				}
				if(isset($arrData['contact']) && !empty($arrData['contact'])) {
					$contact = $arrData['contact'];
				}
				if(isset($arrData['photo']) && !empty($arrData['photo'])) {
					$photo = $arrData['photo'];
				}
				if(isset($arrData['language']) && !empty($arrData['language'])) {
					$language = $arrData['language'];
				}
				try {
					//set clientId as self (userID)
					$updateClientIdQuery = "UPDATE 
												users
					           				SET
					           					client_id = :clientId
					           				WHERE
					           					id = :userId";

					$this->statement = $this->conn->prepare($updateClientIdQuery);
					$this->statement->bindValue(":clientId", $clientId, PDO::PARAM_INT);
					$this->statement->bindValue(":userId", $clientId, PDO::PARAM_INT);
					$this->statement->execute();


					//new profile query for the client
					$newProfileQuery = "INSERT INTO user_profile
					            (user_id,
					             first_name,
								 last_name,
								 contact,
								 photo,
					             language)
					VALUES     (:userId,
					            :firstName,
								:lastName,
								:contact,
								:photo,
					            :language)";
					$this->statement = $this->conn->prepare($newProfileQuery);
					$this->statement->bindValue(":userId", $clientId, PDO::PARAM_INT);
					$this->statement->bindValue(":firstName", $firstName, PDO::PARAM_STR);
					$this->statement->bindValue(":lastName", $lastName, PDO::PARAM_STR);
					$this->statement->bindValue(":contact",$contact , PDO::PARAM_INT);
					$this->statement->bindValue(":photo",$photo , PDO::PARAM_STR);
					$this->statement->bindValue(":language",$language , PDO::PARAM_STR);
					//create new profile
					$this->statement->execute();

					//create client user database
					if(file_exists(LIBRARY_ROOT."dbQuery.php")) {
						include_once LIBRARY_ROOT."dbQuery.php";
					} else {
						$createDbQuery ="";
					}
					
					$this->statement = $this->conn->prepare($createDbQuery);
					$this->statement->execute();
					
					//create client user tables
					if(file_exists(LIBRARY_ROOT."clientdb.sql")) {
						$sqlConn = "mysql -u$this->dbUser -p$this->dbPass -h$this->dbHost clientdb0$clientId < ".LIBRARY_ROOT."clientdb.sql";
						exec($sqlConn);
							try {
								$newdbUserQuery = "INSERT INTO clientdb0$clientId.users
								            (testcube_id,
								             first_name,
											 last_name,
											 email,
											 created_on,
								             language)
								VALUES     (:clientId,
								            :firstName,
											:lastName,
											:email,
											NOW(),
								            :language)";
								
								$this->statement = $this->conn->prepare($newdbUserQuery);
								$this->statement->bindValue(":clientId", $clientId, PDO::PARAM_INT);
								$this->statement->bindValue(":firstName", $firstName, PDO::PARAM_STR);
								$this->statement->bindValue(":lastName", $lastName, PDO::PARAM_STR);
								$this->statement->bindValue(":email",$this->email , PDO::PARAM_INT);
								$this->statement->bindValue(":language",$language , PDO::PARAM_STR);
								//create new profile
								if($this->statement->execute()) {

									$defaultInstruction = "INSERT INTO clientdb0$clientId.custom_instructions
								            (description)
								VALUES     ('Only 1 attempt allowed')";
								$this->statement = $this->conn->prepare($defaultInstruction);
								$this->statement->execute();
								
								}
						} catch(PDOExecption $e) {
							$this->conn->rollback();
					//report error : $e->getMessage() newdbUserQuery execution error in model:user & function:createAccount of Testcube
								$this->errorMsg =  "pdoexception in User model createAccount function";
								$this->errorReportObj->sendErrorReport($this->errorMsg);

						}
					} else {
						$this->conn->rollback();
						//report error : Client database script not found in model:user & function:createAccount of Testcube
					}
					//end transaction					
					$this->conn->commit();
					//set password as return value					
					$return = $this->password;
				} catch(PDOExecption $e) {
				//mysql exception
				$this->conn->rollback();
				//report error : $e->getMessage() newProfileQuery execution error in model:user & function:createAccount of Testcube
								$this->errorMsg =  "pdoexception in User model createAccount function";
								$this->errorReportObj->sendErrorReport($this->errorMsg);

				}
			} else {
				//Email or username already exist 
				$this->conn->rollback();
				//report error : Email or username already exist  in model:user & function:createAccount of Testcube
			
			}
			
			} catch(PDOExecption $e) {
				//mysql exception
				$this->conn->rollback();
				//report error : $e->getMessage() newUserQuery execution error in model:user & function:createAccount of Testcube
								$this->errorMsg =  "pdoexception in User model's createAccount function";
								$this->errorReportObj->sendErrorReport($this->errorMsg);

			}
		} 
		//return the value to contoller
		return $return;
		
	}


	//this function will return client id as logged in user 
	public function findUserId($arrData) {
		$return="";
		$txtUserName="";

		if(isset($arrData['password']) && !empty($arrData['password'])) {

			$this->password = $arrData['password'];

			if(isset($arrData['username']) && !empty($arrData['username'])) {
				// user has entered his registered username
				 $txtUserName = $arrData['username'];
				 $selectUserQuery = "SELECT 
								id,client_id 
						     FROM
								users 
						     WHERE 
								username =:txtUserName 
								and password =:password";

			} else if(isset($arrData['email']) && !empty($arrData['email'])) {
				// user has entered his registered email Address
				 $txtUserName = $arrData['email'];
				 $selectUserQuery = "SELECT 
								id,client_id
						     FROM
								users 
						     WHERE 
								email =:txtUserName 
								and password =:password";
			} else {
				// server validation error
				//report error server validation error for username/email in model:user & function:findUserId of Testcube
				$return="";
				break;
			}
			try {

				$this->statement = $this->conn->prepare($selectUserQuery);
				$this->statement->bindValue(":txtUserName", $txtUserName, PDO::PARAM_STR);
				$this->statement->bindValue(":password", md5($this->password), PDO::PARAM_STR);
				
				
				$this->statement->execute();
				$count = $this->statement->rowCount();
				if($count == 1) {
					
					$userRow = $this->statement->fetch(PDO::FETCH_ASSOC);
					$userId = $userRow['id'];
					$clientId = $userRow['client_id'];
					// update user login activity
					$userActivityQuery = "INSERT INTO user_activity
											(user_id)
						     			VALUES
						     				(:userId)";
					$this->statement = $this->conn->prepare($userActivityQuery);
					$this->statement->bindValue(":userId", $userId, PDO::PARAM_INT);
					if($this->statement->execute()) {
						$return = array('userId' => $userId,'clientId' => $clientId );
					}
					
				}
			} catch(PDOExecption $e) {
				//report error as PDO exception error in model:user & function:findUserId of Testcube

					$this->errorMsg =  "pdoexception in User model's findUserId function";
					$this->errorReportObj->sendErrorReport($this->errorMsg);

				
			}
			
			
		} else {
			$return="";		
		}
		
		return $return;
	}

	//this function will reset the password
	function resetPassword($arrData) {
		$return="";
		$txtUserName="";

		if(isset($arrData['email']) && !empty($arrData['email'])) {
			$txtUserName = $arrData['email'];
			// generate a new 10 digits access code
			include LIBRARY_ROOT . "passwordGenerator.php";
			
			$this->password = generate_password();
			
			try {			
			$resetPasswordQuery = "UPDATE 
										users 
									SET					            
					             		password = :password
					             WHERE 
					             		email = :email";
			
			$this->statement = $this->conn->prepare($resetPasswordQuery);
			$this->statement->bindValue(":email", $txtUserName, PDO::PARAM_STR);
			$this->statement->bindValue(":password",md5($this->password),PDO::PARAM_STR);
			$this->statement->execute();
			$return = $this->password;

				}
			 catch(PDOExecption $e) {

					$this->errorMsg =  "pdoexception in User model's resetPassword function";
					$this->errorReportObj->sendErrorReport($this->errorMsg);

			} 
		return $return;
		}
	}
	
	//this function will return the availability of username
	public function isAvaialableUsername($arrData) {
		$return=0;
		if(isset($arrData['username']) && !empty($arrData['username'])) {
			// get username
			$txtUserName = $arrData['username'];
			// this Query will update the password corresponding to the email entered
			$userNameQuery = "SELECT
								id
						     FROM
								users
						     WHERE
								username =:txtUserName";
			try {
			
				$this->statement = $this->conn->prepare($userNameQuery);
				$this->statement->bindValue(":txtUserName", $txtUserName, PDO::PARAM_STR);			
				//execute userNameQuery
				$this->statement->execute();
				$count = $this->statement->rowCount();
				if(intval($count) == 0) {
					//username not found in database => return value 1 as true 
						$return = 1;
				}
			} catch(PDOExecption $e) {
					//report error : userNameQuery execution error in model:user & function:isAvaialableUsername of Testcube

				$this->errorMsg =  "pdoexception in User model's isAvaialableUsername function";
				$this->errorReportObj->sendErrorReport($this->errorMsg);

			}
								
		}
		//return the value to contoller
		return $return;
		
	}
	
	//this function will return the availability of email address
	public function isAvaialableEmail($arrData) {
		$return=0; //default return value
		if(isset($arrData['email']) && !empty($arrData['email'])) {
			// get email address
			$txtEmail = $arrData['email'];
			$emailQuery = "SELECT
								id
						     FROM
								users
						     WHERE
								email =:txtEmail";
			try {
					
				$this->statement = $this->conn->prepare($emailQuery);
				$this->statement->bindValue(":txtEmail", $txtEmail, PDO::PARAM_STR);
				//execute emailQuery	
				$this->statement->execute();
				$count = $this->statement->rowCount();
				if(intval($count) == 0) {
					//email Id not found in database => return value 1 as true 
					$return = 1;
				}
			} catch(PDOExecption $e) {
					
					//report error : emailQuery execution error in model:user & function:isAvaialableEmail of Testcube
					$this->errorMsg =  "pdoexception in User model isAvaialableEmail function";
					$this->errorReportObj->sendErrorReport($this->errorMsg);

			}
	
		}
		return $return;
	}
}

?>

