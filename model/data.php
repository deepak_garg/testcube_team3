<?php

require_once(LIBRARY_ROOT.'clientdbconnection.php');
require_once(LIBRARY_ROOT.'excelreader/reader.php');
require_once(CONTROLLER_PATH.'appcontroller.php');

require_once 'question.php';

class dataModel extends Appcontroller {
	private $conn;
	private $statement;
	private $clientId;
	private $dbHost;
	private $dbUser;
	private $dbPass;
	private $errorMsg;

	function __construct() {

		parent::__construct();
		// load mysql connection with product userconfig for product database
		$this->conn = clientDbConnection::$conn;
		
	}


	public function __get($name) {
	
	}
	
	
	public function __set($name,$value) {
	
	}


	// this function will return the count values of active category,questions,tests and users

	public function countTotal() {
			
		$return=0;
		
		try {	
			
			$countRowQuery = "SELECT 
									(select count(*) from category where status='0') as totalCategory,
									(select count(*) from questions where status='0') as totalQuestion,
									(select count(*) from test where status='0') as totalTest,
									(select count(*)-1 from users where status='0') as totalUsers";

			$this->statement = $this->conn->prepare($countRowQuery);		
			//execute statement
			$this->statement->execute();
			$rowCount = $this->statement->fetch(PDO::FETCH_ASSOC);
			$return = $rowCount;
			
		} catch(PDOExecption $e) {		
			// report error : Invalid userId in function dashboard model dashboard

			$this->errorMsg =  "PDOExecption in data model's countTotal function";
			$this->errorReportObj->sendErrorReport($this->errorMsg);
		}

		return $return;
	}

	// this function will load questions from excel sheet to the mysql datatbase
	// capable of creating new category if categiry doesn't exist
	public function loadDataFromExcel($arrData) 
	{
		 
		 $count =0;
		 $inputFile = $arrData['file'];
		 $userId = $arrData['userId'];
   		 $excel = new PhpExcelReader;
		 $excel->read($inputFile);    
    	 $rows = $excel->sheets[0]['numRows'];
 		 $cols = $excel->sheets[0]['numCols'];
		
		 if($rows != 111 || $cols != 17 )
		 {
		 	//report error : Invalid Excel sheet Template 
		 	return false;
		 } else {

		 	
			// First row of the template
			$x =11;
			$this->conn->beginTransaction();
    		while($x<=111) {      	

    				//fetch row data for row no. $x	 		
     			 	$cellRow = $excel->sheets[0]['cells'][$x];
			 
									
					 if(isset($cellRow[3]) 
					 	&& isset($cellRow[4]) 
					 	&& isset($cellRow[5]) 
					 	&& isset($cellRow[6]) 
					 	&& isset($cellRow[7]) 
					 	&& isset($cellRow[16]) 
					 	&& isset($cellRow[17]) ) {
					 	

					 		//start transaction
					 		

					 	$cellCatId = mysql_real_escape_string($cellRow[3]);

					 	$arrCatData = array(
					 						'cell' => $cellCatId,
					 						'userId' => $userId );
					 	$catId = $this->getCategory($arrCatData);
					 	$queDesc = mysql_real_escape_string($cellRow[4]);
					 	$queType = mysql_real_escape_string($cellRow[5]);
					 	$answer = mysql_real_escape_string($cellRow[16]);
					 	$points = mysql_real_escape_string($cellRow[17]);
					 	$arrOpt = array();
					 	$arrAns = explode(',', $answer);
					 	
					 	if($queType == 0 ) {	

					 			//build option array for Multiple choice question				 		
						 		for($i = 6;$i<16;$i++) {
						 			if(isset($cellRow[$i]) && !empty($cellRow[$i])) {
						 				$arrOpt[] = mysql_real_escape_string($cellRow[$i]);
						 			} else {
						 				break;
						 			}						 		 
						 		}

					 	} else if($queType == 1) {

					 		// Buuild option array for true/False type question
					 		$arrOpt[] = mysql_real_escape_string($cellRow[6]);
					 		$arrOpt[] = mysql_real_escape_string($cellRow[7]);

					 	} else {
					 		$this->conn->rollback();
					 		//report error : Invalid question type
					 		return false;
					 	}
					 	$quesData = array(
							'cat_id'	 => $catId,
							'question'   => $queDesc,
							'type' 		 => $queType,
							'created_by' => $userId,
							'points'	 => $points,
							'options' 	 => $arrOpt,
							'answers'	 => $arrAns,
							'excel'		 => 1
						);
						$queObj = new questionModel();
					 	$queId = $queObj->addQuestion($quesData);
					 	if(isset($queId) && !empty($queId)) {
					 		if(isset($arrData['testId']) && !empty($arrData['testId'])) {

					 			$testQuestionQuery = "INSERT INTO
					 										assigned_questions(
					 											test_id,ques_id)
													VALUES(
														:test_id,:ques_id)";
								try {
									$this->statement = $this->conn->prepare($testQuestionQuery);
									$this->statement->bindValue(":test_id", $arrData['testId'], PDO::PARAM_INT);
									$this->statement->bindValue(":ques_id", $queId, PDO::PARAM_INT);
									$this->statement->execute();
								} catch (PDOException $e) {
									//report error : pdo error e->getMessasge 
									$this->conn->rollback();

									$this->errorMsg =  "PDOExecption in data model's loadDataFromExcelfunction";
									$this->errorReportObj->sendErrorReport($this->errorMsg);

								}
							}
					 		$count++;
					 		$x++;
					 	} else {
					 			// report error: Unable to add question from excel sheet
					 			$this->conn->rollback();
					 			return false;
						 }

					 } else {
					 		// Invalid or Null data for row $x

					 		 break;
					 }						 
   		 		}
   		 		if($count>0) {
					 			// commit the changes in database
					 			
					 			$this->conn->commit();
					 			return $count;

				}
   		}
   	}


   	// this fumction will return the category id of the category
   	// creates new if doesn't exist
   		public function getCategory($cellData)
   		{
   			$return;
   			$cell = $cellData['cell'];
   			$userId = $cellData['userId'];
   			if(isset($cell) && !empty($cell)) {

					 				 try {	
			
										$catIdQuery = "SELECT 
															id
														FROM
															category
														WHERE
															name=:catName 
															AND status = 0";
									
										$this->statement = $this->conn->prepare($catIdQuery);	
										$this->statement->bindValue(":catName", strtoupper($cell), PDO::PARAM_STR);		
										$this->statement->execute();
										$row = $this->statement->fetch(PDO::FETCH_ASSOC);
										$catId = $row['id'];

										if(empty($catId)) {

											$newCategoryQuery = "INSERT INTO category 
																            (name, 
																             created_by, 
																             created_on) 
																VALUES      ( :catName, 
																              :created_by, 
																              Now() ) ";

											$this->statement = $this->conn->prepare($newCategoryQuery);
											$this->statement->bindValue(":catName", strToUpper($cell), PDO::PARAM_STR);
											$this->statement->bindValue(":created_by", $userId, PDO::PARAM_INT);
											$this->statement->execute();
											$catId = $this->conn->lastInsertId();
											
											
										 }

										} catch (PDOException $e) {
											// report error : PDO Exception Error...
											$this->errorMsg =  "PDOExecption in data model's getCategory function";
											$this->errorReportObj->sendErrorReport($this->errorMsg);
		
										}
										$return = $catId;

								} else {
									return false;
								}
								return $return;
   		}

	
			
}
