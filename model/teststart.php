<?php

	require_once(LIBRARY_ROOT.'clientdbconnection.php');

	class teststartModel 
	{

			private $conn;
			private $statement;


			public function __construct()
			{

				// load mysql connection with product userconfig for product database
				$this->conn = clientDbConnection::$conn;

			}

			public function loadQuestion($StuIdAndQuestionId)
			{

					$stu_id = $StuIdAndQuestionId[0];
					$questionId = $StuIdAndQuestionId[1]; 
					$selectedOptionId = 0;

					$retrieveQuestionQuery = "SELECT ques.id,
											       ques.description,
											       ques.points,
											       ques.type,
											       cat.name AS category,
											       Group_concat(ques_option.id) AS options_id,
											       Group_concat(ques_option.question_option) AS options
											FROM   questions ques
											       INNER JOIN category cat
											               ON ques.cat_id = cat.id
											       INNER JOIN question_options ques_option
											               ON ques_option.question_id = ques.id
											WHERE  ques.id = :questionId
											       AND ques.status = 0";	


					try {

						$this->statement = $this->conn->prepare($retrieveQuestionQuery);
						$this->statement->bindValue(':questionId',$questionId,PDO::PARAM_INT);
						$this->statement->execute();
						$questionsData = $this->statement->fetchAll(PDO::FETCH_ASSOC);


					} catch (PDOException $e) {

					}

					$retrieveSelectedOptionQuery = "SELECT selected_option_id
													FROM   attempts
													WHERE  stu_id = :stuId
													       AND ques_id = :questionId";


					try {

						$this->statement = $this->conn->prepare($retrieveSelectedOptionQuery);
						$this->statement->bindValue(':stuId',$stu_id,PDO::PARAM_INT);	
						$this->statement->bindValue(':questionId',$questionId,PDO::PARAM_INT);
						$this->statement->execute();

						$selectedOptionId = $this->statement->fetchAll(PDO::FETCH_ASSOC);

						if($selectedOptionId===null) {

							$selectedOptionId = 0;	
						}
						//print_r($questionsData);

					} catch (PDOException $e) {

					}								       	
													       
					$data = array($questionsData,$selectedOptionId);

					return $data;						

			}

			public function addSelectedOption($arrData)
			{

				$return = 0;
				$insertIntoAttemptsQuery = "INSERT INTO attempts
											            (link_code,
											             stu_id,
											             ques_id,
											             selected_option_id)
											VALUES     (:link_code,
											            :stu_id,
											            :ques_id,
											            :selected_option_id)";		


				try {

					$this->statement = $this->conn->prepare($insertIntoAttemptsQuery);
					$this->statement->bindValue(':link_code',$arrData[3],PDO::PARAM_STR);
					$this->statement->bindValue(':stu_id',$arrData[0],PDO::PARAM_INT);
					$this->statement->bindValue(':ques_id',$arrData[1],PDO::PARAM_INT);
					$this->statement->bindValue(':selected_option_id',$arrData[2],PDO::PARAM_INT);
					if($this->statement->execute()) {

						$return = 1;		
					}
					
				} catch(PDOException $e) {

				}

					return $return;

			}


			public function resultProcessing($arrData) 
			{

				$resultProcessingQuery = "SELECT Count(correct) as score
											FROM   (SELECT ques_option.correct
											        FROM   question_options ques_option
											               INNER JOIN attempts attmp
											                       ON ques_option.id = attmp.selected_option_id
											        WHERE attmp.stu_id = :stu_id 
											        	AND attmp.link_code = :linkCode) AS correctTable
											WHERE  correctTable.correct = 1";						


				try {

					$this->statement = $this->conn->prepare($resultProcessingQuery);
					$this->statement->bindValue(':linkCode',$arrData[0],PDO::PARAM_INT);
					$this->statement->bindValue(':stu_id',$arrData[1],PDO::PARAM_INT);
					$this->statement->execute();	
					$resultForStudent = $this->statement->fetchAll(PDO::FETCH_ASSOC);	

				} catch (PDOException $e) {

				}

				return $resultForStudent;	
					
			}	
			
	}
