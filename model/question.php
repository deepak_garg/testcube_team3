<?php

require_once(LIBRARY_ROOT.'clientdbconnection.php');
require_once(CONTROLLER_PATH.'appcontroller.php');

class questionModel extends Appcontroller{
	private $conn;
	private $dbHost;
	private $dbPass;
	private $dbUser;
	private $statement;
	private $catName;
	private $created_by;
	private $updated_by;
	private $created_on;
	private $updated_on;
	private $quesDescription;
	private $type;
	private $cat_id;
	private	$points;
	private $options;
	private $answers;
	private $ques_id;
	private $opt_id;
	private $errorMsg;
	function __construct(){

		parent::__construct();
		// load mysql connection with product userconfig for product database
		$this->conn = clientDbConnection::$conn;
		
	}

	

	function __get($name){

	}

	function __set($name,$value){

	}

// list all active questions
	public function questionList(){
		$return="";

		$selectQuestionList =  "SELECT ques.id,
								       ques.cat_id,
								       ques.created_by,
								       ques.created_on,
								       (SELECT usr.first_name
								        FROM   users usr
								        WHERE  ques.updated_by = usr.id) AS first_name_updatedby,
								       ques.updated_on,
								       ques.description,
								       ques.points,
								       ques.status,
								       ques.type,
								       cat.name,
								       usr.first_name
								FROM   questions ques
								       INNER JOIN category cat
								               ON ques.cat_id = cat.id
								       INNER JOIN users usr
								               ON ques.created_by = usr.id
								WHERE  ques.status = 0
								ORDER  BY ques.id DESC  ";

		try{
			$this->statement = $this->conn->prepare($selectQuestionList);
			$this->statement->execute();
			$quesList = $this->statement->fetchAll(PDO::FETCH_ASSOC);
			$return = $quesList;


		} catch(PDOException $e){
			// send error using email


						$this->errorMsg =  "PDOExecption in questions model's questionList function";
						$this->errorReportObj->sendErrorReport($this->errorMsg);
		}						
		return $return;	
	}


	// fetch complete details of question
	public function questionFullList($quesId){
		$return="";

		$selectQuesFullList =  "SELECT q.id,       
								       q.description, 
								       q.points, 
								       q.status, 
								       q.type, 
								       c.name,

								       Group_concat(qo.correct, ',', qo.question_option) as options
								FROM   questions q 
								       INNER JOIN category c 
								               ON q.cat_id = c.id 
								       INNER JOIN question_options qo 
								               ON qo.question_id = q.id 
								WHERE  q.id = $quesId 
								AND    q.status = 0 
								ORDER  BY q.id DESC";

		try{
			$this->statement = $this->conn->prepare($selectQuesFullList);
			$this->statement->execute();
			$quesList = $this->statement->fetch(PDO::FETCH_ASSOC);
			$return = $quesList;


		} catch(PDOException $e){
			// send error using email

					$this->errorMsg =  "PDOExecption in questions model's questionFullList function";
					$this->errorReportObj->sendErrorReport($this->errorMsg);
		}						
		return $return;	
	}

	// avail question's full details for editing purpose
	public function questionFullEditList($quesId){
		$return="";

		$selectQuesFullList =  "SELECT q.id,       
								       q.description, 
								       q.points, 
								       q.status, 
								       q.type, 
								       c.id as name,
								  	   Group_concat(qo.id) as optid,
								       Group_concat(qo.correct, ',', qo.question_option) as options
								FROM   questions q 
								       INNER JOIN category c 
								               ON q.cat_id = c.id 
								       INNER JOIN question_options qo 
								               ON qo.question_id = q.id 
								WHERE  q.id = $quesId 
								AND    q.status = 0 
								ORDER  BY q.id DESC";

		try{
			$this->statement = $this->conn->prepare($selectQuesFullList);
			$this->statement->execute();
			$quesList = $this->statement->fetch(PDO::FETCH_ASSOC);
			$return = $quesList;


		} catch(PDOException $e){
			// send error using email

						$this->errorMsg =  "PDOExecption in questions model's questionFullEditList function";
						$this->errorReportObj->sendErrorReport($this->errorMsg);
		}						
		return $return;	
	}

	//Add question and options
	public function addQuestion($arrData){
		
		$strChecker =0;
		$strChecking =0;
		$return = 0;
		if(isset($arrData['question']) && !empty($arrData['question'])) {
			$this->quesDescription=ucfirst(mysql_real_escape_string($arrData['question']));
			$this->type=$arrData['type'];
			$this->created_by=$arrData['created_by'];
			$this->cat_id=$arrData['cat_id'];
			$this->points=$arrData['points'];
			$this->options=$arrData['options'];
			$this->answers=$arrData['answers'];

			
			$insertQuestionQuery = "INSERT INTO questions
									            (cat_id, 
									             created_by,
									             created_on, 
									             description,
									             points,
									             type) 
										 VALUES ( :category, 
									              :created_by,
									               NOW(),
									              :question,
									              :points,
									              :type)";

			$insertOptionQuery	=	"INSERT INTO question_options 
									             (question_id, 
									             question_option, 
									             correct) 
									      VALUES (:ques_id, 
									              :ques_option, 
									              :correct)"; 


					try{
						$this->statement = $this->conn->prepare($insertQuestionQuery);
						$this->statement->bindValue(":category", $this->cat_id, PDO::PARAM_INT);
						$this->statement->bindValue(":created_by", $this->created_by, PDO::PARAM_INT);
						$this->statement->bindValue(":question", $this->quesDescription, PDO::PARAM_STR);
						$this->statement->bindValue(":points", $this->points, PDO::PARAM_INT);
						$this->statement->bindValue(":type", $this->type, PDO::PARAM_INT);
						if($this->statement->execute()) {
							$lastId = $this->conn->lastInsertId(); //get id of last inserted question

							$this->statement = $this->conn->prepare($insertOptionQuery);

							$optionCount = count($this->options); //count the number of options
							$answerCount = count($this->answers);
							
							$this->statement->bindValue(":ques_id", $lastId, PDO::PARAM_INT);

							for($i=0; $i < $optionCount; $i++)
							{
								$strChecker+=1;
								$this->statement->bindValue(":ques_option", $this->options[$i], PDO::PARAM_STR);
								
								for($j=0; $j < $answerCount; $j++)
								{
									if($this->answers[$j] == $i) {
									$this->statement->bindValue(":correct", 1, PDO::PARAM_INT);
									break;
									}
									else{
									$this->statement->bindValue(":correct", 0, PDO::PARAM_INT);
									}
								}

								$strChecking += $this->statement->execute();

							}
							if($strChecker == $strChecking) {
								if(isset($arrData['excel']) && $arrData['excel'] == 1) {
									$return = $lastId;
								} else {
									$return = 1;
								}	//return success status
							}
						}


					} catch(PDOException $e){
						
						$this->errorMsg =  "PDOExecption in questions model's addQuestion function";
						$this->errorReportObj->sendErrorReport($this->errorMsg);
						exit;
					}			
		} else {
			$return = 0;
		}

		return $return;
	}


	//EDIT ---------->>>><<<<<---------------- question and options
	public function editQuestion($arrData){
		$return = 0;
		if(isset($arrData['question']) && !empty($arrData['question'])) {
			$this->ques_id   	  	=	$arrData['ques_id'];
			$this->quesDescription	=	$arrData['question'];
			$this->type 			=	$arrData['type'];
			$this->updated_by 		=	$arrData['updated_by'];
			$this->cat_id 			=	$arrData['cat_id'];
			$this->points 			=	$arrData['points'];
			$this->opt_id 			=	$arrData['optid'];
			$this->options 			=	$arrData['options'];
			$this->answers 			=	$arrData['answers'];
			echo "<pre>";
			print_r($arrData);
			$updateQuestionQuery = "UPDATE questions
									SET    cat_id 		= :category,
									       updated_by 	= :updated_by,
									       description 	= :question,
									       points 		= :points
									WHERE  id 			= :id";

			$updateOptionsQuery  = "UPDATE 	question_options
									  SET   question_option = :ques_option,
									        correct 		= :correct
									WHERE   question_id 	= :ques_id
									  AND   id 				= :opt_id";

			$InsertOptionsQuery	=	"INSERT INTO question_options 
											            (question_id, 
											             question_option, 
											             correct) 
											VALUES     (:ques_id, 
											            :ques_option, 
											            :correct)";
			$GetOptionCountsQuery		=	"SELECT id
 											FROM 	question_options
											WHERE 	question_id = :question_id";   

					try{
						$this->statement = $this->conn->prepare($updateQuestionQuery);
						$this->statement->bindValue(":id", $this->ques_id, PDO::PARAM_INT);
						$this->statement->bindValue(":category", $this->cat_id, PDO::PARAM_INT);
						$this->statement->bindValue(":updated_by", $this->updated_by, PDO::PARAM_INT);
						$this->statement->bindValue(":question", $this->quesDescription, PDO::PARAM_STR);
						$this->statement->bindValue(":points", $this->points, PDO::PARAM_INT);
						
						
						if($this->statement->execute()) {
							$this->statement = $this->conn->prepare($GetOptionCountsQuery);
							$this->statement->bindValue(":question_id", $this->ques_id, PDO::PARAM_INT);
						
						echo "<pre>";

					
			    			$this->statement->execute();
						    $oldCount    = $this->statement->rowcount();
						    $updateOpt   = array_slice($this->options,0, $oldCount); //options for update
						    $insertOpt   = array_slice($this->options,$oldCount,$oldCount,true);	//options for insert
							echo $updateCount = count($updateOpt); //count the number of options to update
							echo $insertCount = count($insertOpt); //count the number of options to insert
							$answerCount = count($this->answers);
							
							print_r($updateOpt);
							print_r($insertOpt);

							//Updates old options
							$this->statement = $this->conn->prepare($updateOptionsQuery);
							$this->statement->bindValue(":ques_id", $this->ques_id, PDO::PARAM_INT);
							for($i=0; $i < $updateCount; $i++)
							{
								$this->statement->bindValue(":opt_id",$this->opt_id[$i], PDO::PARAM_INT);
								$this->statement->bindValue(":ques_option", $updateOpt[$i], PDO::PARAM_STR);
								
								for($j=0; $j < $answerCount; $j++)
								{
									if($this->answers[$j] == $i) {
									$this->statement->bindValue(":correct", 1, PDO::PARAM_INT);
									break;
									}
									else{
									$this->statement->bindValue(":correct", 0, PDO::PARAM_INT);
									}
								}
								var_dump($this->statement->execute());

							}

							//insert new options
							$this->statement = $this->conn->prepare($InsertOptionsQuery);
							$this->statement->bindValue(":ques_id", $this->ques_id, PDO::PARAM_INT);

							foreach($insertOpt as $key => $value)
							{
								$this->statement->bindValue(":ques_option",$value, PDO::PARAM_STR);
								
								for($j=0; $j < $answerCount; $j++)
								{
									if($this->answers[$j] == $key) {
									$this->statement->bindValue(":correct", 1, PDO::PARAM_INT);
									break;
									}
									else{
									$this->statement->bindValue(":correct", 0, PDO::PARAM_INT);
									}
								}
								var_dump($this->statement->execute());

							}
							$return = 1;
							}

						
						


					} catch(PDOException $e){
						
						//send error email
						$this->errorMsg =  "PDOExecption in questions model's editQuestion function";
						$this->errorReportObj->sendErrorReport($this->errorMsg);
					}			
		} else {
			$return = 0;
		}

		return $return;
	}

	//Delete question
	public function delQuestion($questionId)
		{

			$returnMsg = 0;

			if(isset($questionId) && !empty($questionId)) {
				
				$softDeleteQuestionQuery = "UPDATE  questions 
										  	   SET  status = 2 
										     WHERE  id = :questionid";


				try {

						$this->statement = $this->conn->prepare($softDeleteQuestionQuery);
						$this->statement->bindValue(':questionid',$questionId,PDO::PARAM_INT);
						$this->statement->execute();
						$returnMsg = 1;

					} catch(PDOExecption $e) {

				// PDO Exception // send error email
						$this->errorMsg =  "PDOExecption in questions model's delQuestion function";
						$this->errorReportObj->sendErrorReport($this->errorMsg);
				}					 
			}
			
			return $returnMsg;
			
		}

		public function deleteMulques($questionId)
		{
			$returnMsg = 0;

			if(isset($questionId) && !empty($questionId)) {
				foreach ($questionId as $key => $value) {
					$softDeleteQuestionQuery = "UPDATE questions 
								  	  SET    status = 2 
								      WHERE  id = :questionId";


				try {

						$this->statement = $this->conn->prepare($softDeleteQuestionQuery);
						$this->statement->bindValue(':questionId',$value,PDO::PARAM_INT);
						$this->statement->execute();
						

					} catch(PDOExecption $e) {

						// PDO Exception // send error email
						$this->errorMsg =  "PDOExecption in questions model's deleteMulques function";
						$this->errorReportObj->sendErrorReport($this->errorMsg);
					}					  
				}
				
				$returnMsg = 1;
			}
			
			return $returnMsg;	
		}

		
		

}

?>
