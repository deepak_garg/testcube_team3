<?php

require_once(LIBRARY_ROOT.'clientdbconnection.php');
require_once(CONTROLLER_PATH.'appcontroller.php');

class settingsModel extends Appcontroller {
	private $conn;
	private $statement;
	private $testId;
	private $linkId;
	private $linkName;
	private $created_by;
	private $updated_by;
	private $created_on;
	private $updated_on;
	private $errorMsg;

	function __construct() {

		parent::__construct();
		// load mysql connection with product userconfig for product database
		$this->conn = clientDbConnection::$conn;
		
	}


	// this function will create a new link for the test and returns the id
	public function createLink($arrData) {

		$return ="";
		if(isset($arrData['name']) && !empty($arrData['name'])) {
			$this->linkName=$arrData['name'];
			$this->created_by=$arrData['created_by'];
			$this->testId=$arrData['testId'];

			$createlinkQuery = "INSERT INTO link 
								            (test_id,
								            	name, 
								             created_by, 
								             created_on) 
								VALUES      ( :testId,
											  :linkName, 
								              :created_by, 
								              Now() ) ";


					try{
						$this->statement = $this->conn->prepare($createlinkQuery);
						$this->statement->bindValue(":testId", $this->testId, PDO::PARAM_INT);
						$this->statement->bindValue(":linkName", $this->linkName, PDO::PARAM_STR);
						$this->statement->bindValue(":created_by", $this->created_by, PDO::PARAM_INT);

						if($this->statement->execute()) {

							$return = $this->conn->lastInsertId();
						}

					} catch(PDOException $e){
						
						//report error : pdoexception in createLink
						$this->errorMsg =  "pdoexception in Settings model's createLink function";
						$this->errorReportObj->sendErrorReport($this->errorMsg);
						
					}
			
					
		} else {
			$return = "";
		}

		return $return;
	}

	// this function will add a custom instruction with msg as an input
	public function addNewInstruction($msg) {

		$return ="";
		if(isset($msg) && !empty($msg)) {

			$instructionQuery = "INSERT INTO custom_instructions 
								            (description) 
								VALUES      ( :msg ) ";


					try{
						$this->statement = $this->conn->prepare($createlinkQuery);
						$this->statement->bindValue(":msg", $msg, PDO::PARAM_STR);
						if($this->statement->execute()) {

							$return = $this->conn->lastInsertId();
						}

					} catch(PDOException $e){
						
						//report error : pdoexception in addNewInstruction
						$this->errorMsg =  "pdoexception in Settings model's addNewInstruction function";
						$this->errorReportObj->sendErrorReport($this->errorMsg);
						
					}
			
					
		} else {
			$return = "";
		}

		return $return;
	}
	

	// this function will create new settings for the test link
	public function newSettings($arrData) {

		$return = "";
		$availability=0;
		$attempts = 1;
		$custom_instruction_id = 1;
		$password="";
		$rqrFirstName =0;
		$rqrLastName =0;
		$rqrEmail=0;
		$guidlines =0;
		$timeLimit ="";
		$randomize = 0;
		$rqrAnswer = 0;
		$passMark = 50;
		$emailResult = 0;
		$totalQue;


		

		if(isset($arrData) && !empty($arrData)) {

			
			$this->linkId = $this->createLink($arrData['linkData']);
			if(empty($this->linkId)) {
				// Invalid Link ID 
				 //report error : linkId not found 
				return $return;
			} else {
				if(isset($arrData['availability']) && !empty($arrData['availability']))  {
						$availability = 1;
				}
				if(isset($arrData['attempts']) && !empty($arrData['attempts']))  {
						$attempts = $arrData['attempts'];
				}
				if(isset($arrData['password']) && !empty($arrData['password']))  {
						$password = $arrData['password'];
				}
				if(isset($arrData['custom_instruction']) && !empty($arrData['custom_instruction']))  {

						$custom_instruction_id = $this->addNewInstruction($arrData['custom_instruction']);
						
				}
				if(isset($arrData['timeLimit']) && !empty($arrData['timeLimit']))  {

						$timeLimit = $arrData['timeLimit'];
				}
				if(isset($arrData['randomize']) && !empty($arrData['randomize']))  {

						$randomize = $arrData['randomize'];
				}
				if(isset($arrData['rqrFirstName']) && !empty($arrData['rqrFirstName']))  {

						$rqrFirstName = $arrData['rqrFirstName'];
				}
				if(isset($arrData['rqrLastName']) && !empty($arrData['rqrLastName']))  {

						$rqrLastName = $arrData['rqrLastName'];
				}
				if(isset($arrData['rqrAnswer']) && !empty($arrData['rqrAnswer']))  {

						$rqrAnswer = $arrData['rqrAnswer'];
				}
				if(isset($arrData['passMark']) && !empty($arrData['passMark']))  {

						$passMark = $arrData['passMark'];
				}
				if(isset($arrData['emailResult']) && !empty($arrData['emailResult']))  {

						$emailResult = $arrData['emailResult'];
				}
				if(isset($arrData['totalQue']) && !empty($arrData['totalQue']))  {

						$totalQue = $arrData['totalQue'];
				}


				$newSettingQuery = "INSERT INTO	link_settings
													(link_id,
													availability,
													attempts,
													password,
													firstname,
													lastname,
													email,
													guidelines,
													custom_instructions_id,
													time_limit,
													total_question,
													randomize,
													answer_mandatory,
													passing_marks,
													email_result)
											VALUES
												(:link_id,
													:availability,
													:attempts,
													:password,
													:firstname,
													:lastname,
													:email,
													:guidelines,
													:custom_instruction_id,
													:time_limit,
													:totalQue,
													:randomize,
													:answer_mandatory,
													:passing_marks,
													:email_result)";

				try{
						$this->statement = $this->conn->prepare($newSettingQuery);
						$this->statement->bindValue(":link_id", $this->linkId, PDO::PARAM_INT);
						$this->statement->bindValue(":availability", $availability, PDO::PARAM_INT);
						$this->statement->bindValue(":attempts", $attempts, PDO::PARAM_INT);
						$this->statement->bindValue(":password", $password, PDO::PARAM_STR);
						$this->statement->bindValue(":firstname", $rqrFirstName, PDO::PARAM_INT);
						$this->statement->bindValue(":lastname", $rqrLastName, PDO::PARAM_INT);
						$this->statement->bindValue(":email", $rqrEmail, PDO::PARAM_INT);
						$this->statement->bindValue(":guidelines", $guidlines, PDO::PARAM_INT);
						$this->statement->bindValue(":custom_instruction_id", $custom_instruction_id, PDO::PARAM_INT);
						$this->statement->bindValue(":totalQue", $totalQue, PDO::PARAM_INT);
						$this->statement->bindValue(":time_limit", $timeLimit, PDO::PARAM_INT);
						$this->statement->bindValue(":randomize", $randomize, PDO::PARAM_INT);
						$this->statement->bindValue(":answer_mandatory", $rqrAnswer, PDO::PARAM_INT);
						$this->statement->bindValue(":passing_marks", $passMark, PDO::PARAM_INT);
						$this->statement->bindValue(":email_result", $emailResult, PDO::PARAM_INT);

						
						if($this->statement->execute()) {


							$return = $this->conn->lastInsertId();
						}

					} catch(PDOException $e){
						
						//report error : pdoexception in newSettings
						$this->errorMsg =  "pdoexception in Settings model's newSettings function";
						$this->errorReportObj->sendErrorReport($this->errorMsg);
					}
				return $return;
			}
		} else {

			return $return;
		}

	}
}
?>