<?php
 if(isset($arrData) && !empty($arrData)) {
   $arrData = $arrData;
 } else {
    $arrData['countData']['totalQuestion'] = 0;
    $arrData['countData']['totalCategory'] = 0;
    $arrData['countData']['totalTest'] = 0;
 }
?>
            
        </div><!-- end of dash-right-content-->
        <div id="dash-right-content">
        
        	<div id="content-border" class="blue-background"></div>
            <div id="content-header">
            	<div id="text">information about all modules at one place</div>
                <div id="heading"><span class="icon glyphicon glyphicon-inbox margin-right5"></span><span>DASHBOARD</span></div>
            </div> <!-- end of content header-->
            <div id="header-arrow" ></div>
            
            <div id="content">
            	<div id="row">
            		<div id="section1"></div>
                	<div id="section2">
                		<div id="divDate"><span class="glyphicon glyphicon-calendar margin-right15 lightBlue"></span>
                    	<span id="text"><?php print(Date("F d, Y")); ?></span></div>	<!-- end of divDate-->
                    
                    	<div id="divGraph2"><div id="donutchart"></div></div>
                	</div>	<!-- end of section2-->
                </div> <!-- end of row -->
                <div id="row">
                  <div id="recentBox">
                    <div id="recentHeader"><strong>RECENT</strong></div>
                    <div id="recentContent">
                      <div class="panel-group fontapply" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading" id="accordion-heading">
                              <h4 class="panel-title">
                               <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" id="ancrTest">
                          <span class="glyphicon glyphicon-file margin-right10"></span>RECENT TESTS
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                  <div class="panel-body">
                                  <?php 
                                      foreach ($arrData['testData'] as $key => $value) {
                                          // print_r($value);
                                          // exit;
                                          echo "<ul>";
                                          echo '<li>OSSCube <a href="'.SITE_PATH.'index.php?controller=dashboard&function=testDetail&testid='.$value['id'].'" >'. ucfirst($value['name']) .'</a></li>';
                                          echo "</ul>";
                                        }
                                        ?>
                                      
                                  
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-default">
                                <div class="panel-heading" id="accordion-heading">
                                  <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                      <span class="glyphicon glyphicon-link margin-right10"></span>RECENT LINKS
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse">
                                  <div class="panel-body">
                                  <?php 
                                  krsort($arrData['linkData']);
                                    foreach ($arrData['linkData'] as $key => $value) {
                                      echo "<ul>";
                                        echo "<li>".$value['linkurl']."</li>";
                                      echo "</ul>";
                  
                                  }?>
                                        
                                  </div>
                                </div>
                              </div>
                              <div class="panel panel-default">
                                <div class="panel-heading" id="accordion-heading">
                                  <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                     <span class="glyphicon glyphicon-check margin-right10"></span> RECENT RESULTS
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse">
                                  <div class="panel-body">
                                   	<ul>
                                   	<li>Results of OSSCubeMysql 14</li>
                                    <li>Results of OSSCubePhp 14</li>
                                    <li>Results of OSSCube Employement efficiency</li>
                                   </ul>
                                  </div>
                                </div>
                              </div>
                        </div>	<!-- end of accordin -->
                       </div> <!-- end of recentContent-->
                    </div> <!-- end of recentBox-->
                    
                    <div id="updatesBox">
                    	<div id="updatesHeader"><strong>UPDATES</strong></div>
                        <div id="updatesContent">

                            	<ul id="vertical-ticker" style="overflow: hidden;">
                              <?php 

                                  $updatesDataCount = count($arrData['updatesData']); 

                                  for($i=0; $i<$updatesDataCount; $i++) {  
                                    $updateString = $arrData['updatesData'][$i]['created_by']." ".$arrData['updatesData'][$i]['activity_performed']." ".$arrData['updatesData'][$i]['entity_created']." ";
                                    //$createdWhat = ucfirst(strtolower($arrData['updatesData'][$i]['created_what']));
                                      $createdWhat = $arrData['updatesData'][$i]['created_what'];
                                     ?> 
                                        <li  class="ticker-odd" style="margin-top: 0px;"><span class="glyphicon glyphicon-user margin-left5 margin-right5"></span><?php echo $updateString ?><strong><?php echo $createdWhat ?></strong></li>
                                 <?php
                                     }
                                 ?>                           
                                </ul>
              
                       </div> <!-- end of updatesContent-->
                    </div> <!-- end of updatesBox-->
                </div>	<!-- end of row-->
                </div>	<!-- end of content -->
     	   <?php require_once("footer.php"); ?>	<!-- include footer and scripts -->
        </div>	<!-- end of dash-right-content-->
    </div> <!-- end of dash-content -->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);

       function drawChart() {
		    var data = google.visualization.arrayToDataTable([
          ['Task', 'TestCube Stats'],
          ['Questions',     <?php echo $arrData['countData']['totalQuestion'];?>],
          ['Categories',      <?php echo $arrData['countData']['totalCategory'];?>],
          ['Tests',  <?php echo $arrData['countData']['totalTest'];?>],
          ['Certificates', 0],
          ['Results',  0]
        ]);

        var options = {
          title: 'TESTCUBE STATISTICS',
          pieHole: 0.4,
          colors: ['#4D5360', '#FDB45C', '#46BFBD', '#949FB1', '#F7464A']
        };

        var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
        chart.draw(data, options);
        
        var data2 = google.visualization.arrayToDataTable([
          ['Weekday', 'Test', 'Results'],
          ['MON',  		10,		8],
          ['TUE',  		5,		5],
          ['WED',  		20,		17],
          ['THU',  		21,		5],
          ['FRI',  		15,		10],
          ['SAT',  		25,		25],
          ['SUN',  		30,		10],
        ]);

        var options2 = {
          title: 'ACTIVE DAILY TEST (24 - 31 March)',
          colors:['#4D5360','#F7464A'],
          pointSize: 7,

        };

        var chart2 = new google.visualization.LineChart(document.getElementById('section1'));
        chart2.draw(data2, options2);
      }

      $('#vertical-ticker').totemticker({
  row_height  : '35px',
  next    : '#ticker-next',
  previous  : '#ticker-previous',
  stop    : '#stop',
  start   : '#start',
    });

    </script>
<script>
$('#footer').css("padding-top","30px");
</script>

 <script type="text/javascript">
        introJs().setOption('doneLabel', 'Next page').start().oncomplete(function() {
          window.location.href = 'second.html?multipage=true';
        });
    </script>