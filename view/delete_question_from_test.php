<?php

$quesIdTestId = explode(',',$_POST['quesIdTestId']);

?>
<div id="divCreate">
	<Span id="lblName">Are you sure want to remove this question?</Span>
	<p></p>
	<input type ="hidden" id='quesId'value="<?php echo $quesIdTestId[0] ?>">
	<input type ="hidden" id='testId' value="<?php echo $quesIdTestId[1] ?>">
	<input type="button" id="btnDeleteQuesFromTest" value="Confirm" class="btn margin-left50 margin-top10" />
	<p></p>
	<div id="errMsg"></div>
</div>

<script>


// used to send question id and test id to the test controller's removeQuestionFromTest function
$('#btnDeleteQuesFromTest').click(function () {

	var quesId = $('#quesId').val();
	var testId = $('#testId').val();

	$.ajax({

		type:"POST",
		url:base_url+'index.php?controller=test&function=removeQuestionFromTest',
		data:{quesId:quesId,testId:testId},
		datatype:'html',

		success: function(response) {

					if(response==1) {
						document.location.reload(true);			
					}

				}
	});



});



</script>