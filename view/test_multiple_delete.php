<div id="divCreate">
	<Span id="lblName">Are you sure want to delete?</Span>
	<p></p>
	<input type="button" id="btnMulDeleteTest" value="Confirm" class="btn margin-left50 margin-top10" />
	<p></p>
	<div id="errMsg"></div>
</div>

<script>

$('#btnMulDeleteTest').click(function() {

	var testId = $('input[name="deleteall"]:checked').map(function(){

		return $(this).attr('value');

	}).get();

	if(testId.length!=0) {

			$.ajax({

				type:"POST",
				data:{testId:testId},
				url:base_url+"index.php?controller=test&function=multipleDeleteTest",

				datatype: 'html',

				success: function(response) {

					if(response==1) {
						document.location.reload(true);			
					}

				}

			});
	    } else {

	    		$('#errMsg').html("No Id selected");
	    }

});

</script>