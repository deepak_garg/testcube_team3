<?php
$linkName = "";
$linkId   = "";
$host     = "";
$countLinks = "";
// echo "<pre>";
// print_r($arrData);
// exit;
if(isset($arrData) && !empty($arrData)) {
    $linkName = $arrData['linkDetails']['name'];
    $linkId = $arrData['linkDetails']['id'];
    $host   = $arrData['linkDetails']['host'];
    $countLinks = count($arrData['recentLink']);

}

?>

</div><!-- end of dash-right-content-->
  <div id="dash-right-content">
    <div id="content-border" class="blue-background"></div>
      <div id="content-header">
         	<div id="text">Links Generated for Test</div>
            <div id="heading"><span class="icon glyphicon glyphicon-file margin-right5"></span><span>TESTS</span></div>
      </div> <!-- end of content header-->
    <div id="header-arrow" ></div>
            
    <div id="content">
        <div class="row-mid">
                <div id="main-container">
                
                <div id="content-head">
                        <div class="test-title" id="ques-content-head">
                        <div class="float-left" id="content-head-left">
                        <p><strong><span class="white"><?php echo $linkName; ?></span> : Assign</strong> </p>
                        </div> <!-- end of content-head-left -->
        
                        <div class="float-right" id="content-head-right">
                
                        </div> <!-- end of content-head-right -->
                        </div> <!-- end of content-head-->
              </div>
                <!-- end of content-head-->
                
                <div>
                        <div class="float-left" id="content-head-left margin-left0">
                 <div class="row-mid div-align-center"> 
                     
                 
                    <div class="row-mid">
                        <div class="review-green">
                            <p class="ptextlrg">Your Test is ready to give. </p>
                
                            <p class="ptextlrg">You can assign the test to your students now. Click 
                                <span class="greenclick text-success" href="#link1" data-parent="#review-green" data-toggle="collapse">here</span><span > to view the link's history. </span></p>
                        </div>
       
                        <div id="link1" class="panel-collapse collapse" >
                          <div class="linklist-header">
                          <ul class="links-head">
                            <li>Links</li>
                            <li>From</li>
                            <li>Until</li>
                            </ul>
                            <ul>
                               <?php 
                                for($i=0;$i<$countLinks;$i++){?>
                                  <li><span class="link-link"><?php echo $arrData['recentLink'][$i]['linkurl']; ?>
                                  </span><span class="link-dateFrom"><?php $date=date_create($arrData['recentLink'][$i]['stop']); echo date_format($date,'Y-m-d');?></span>
                                  <span class="link-dateUntil"><?php $date=date_create($arrData['recentLink'][$i]['start']); echo date_format($date,'Y-m-d');?></span></li>
                                <?php }
                              ?>
                            
                            </ul>
                          </div>
                        </div>

                    </div>
                    
                    <div class="clear-both">
                   
                    <div class="row-mid">
                    <fieldset class="review-grey">
                        <form method="get" action="assign_test_process.php">
            
                
                <div class="content">
                    <p class="ptextlrg">Show from</p>
                    <div class='input-group date fontapply'>
                        <div class='col-md-5'>
                            <div class="form-group">
                                <div class='input-group date' id='datetimepicker8'>
                                    <input type='text' id= "datetimepicker8Value" disabled="disabled" class="form-control margin-top0" placeholder="mm/dd/yyyy" />
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-5'>
                            <div class="form-group">
                                <div class='input-group date' id='datetimepicker9'>
                                    <input type='text' id= "datetimepicker9Value" disabled="disabled" class="form-control margin-top0" maxlength=7 placeholder="Hr:Mins AM/PM" />
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end of input-group date-->
                    <p></p>
                    <p class="ptextlrg">Show Until</p>
                      <div class='input-group date'>
                        <div class='col-md-5'>
                            <div class="form-group">
                                <div class='input-group date' id='datetimepicker10'>
                                    <input type='text' id= "datetimepicker10Value" disabled="disabled" class="form-control margin-top0" placeholder="mm/dd/yyyy" />
                                    <span class="input-group-addon "><span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-5'>
                            <div class="form-group">
                                <div class='input-group date' id='datetimepicker11'>
                                    <input type='text' id="datetimepicker11Value" disabled="disabled" class="form-control margin-top0" placeholder="Hr:Mins AM/PM" />
                                    <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end of input-group date-->
                    <p></p>
                </div> <!--  end of content -->
                <hr>
        
                            <p><strong>Assign and Email link to Users:</strong></p>
                            <p class="hint-text">Please provide comma speperated email ids of users</p>                    
                            <input type="hidden" value="8" name="linkid">
                            <textarea placeholder="for ex. abc@osscube.com, cde@osscube.com," id="review-userstext" class="float-left" name="users"></textarea>

                            <input type="button" id="btnFinish" class="btn margin-left20" value="Finish">
                            <input type="button" id="btnPreview" class="btn" value="Preview Test">

                           </form> 
                        </fieldset>
                        </div>
                    
                    </div>
                        
                        
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                  </div>
                  <div id="modal-body" class="modal-body">
                    

                  </div>
                </div>
              </div>
            </div>
        
            </div>
            <!-- end of main container -->
        </div>
        <!-- end of row-large -->
    </div>
    <!-- end of content -->
 

</div>
    </div>
    <!-- end of content -->
     	   <?php require_once("footer.php"); ?>	<!-- include footer and scripts -->
        </div>	<!-- end of dash-right-content-->
    </div> <!-- end of dash-content -->
    <script type="text/javascript">
        $(function () {
            var today = new Date();
            //alert(today.getHours()%12);
            $('#datetimepicker8').datetimepicker({
                
                    pickTime: false,
                    
                     minDate:today, 
                   // minDate:today,

                });
            $('#datetimepicker9').datetimepicker({
                    pickDate: false
                });

             $("#datetimepicker8").on("dp.change",function (e) {
                if(validateDateFormat('datetimepicker8Value') === false){
                    alert("Invalid date format");
                }
            });

             $('#datetimepicker8Value,#datetimepicker10Value').blur(function(){

                 if(validateDateFormat('datetimepicker8Value') === false || validateDateFormat('datetimepicker10Value') === false){
                    alert("Invalid date format");
                    $('#datetimepicker8Value').val("");
                    $('#datetimepicker10Value').val("");
                 }
             });

              $('#datetimepicker9Value,#datetimepicker11Value').blur(function(){

                 if(validateTimeFormat('datetimepicker9Value') === false || validateTimeFormat('datetimepicker11Value') === false){
                    alert("Invalid date format");
                    $('#datetimepicker9Value').val("");
                    $('#datetimepicker11Value').val("");
                 }
             });

            $('#btnFinish').on("click",function(){
               // alert("hi");
               var date8Value = $('#datetimepicker8Value').val();
                var date10Value = $('#datetimepicker10Value').val();
                var date9Value = $('#datetimepicker9Value').val();
                var date11Value = $('#datetimepicker11Value').val();
                //alert(date11Value);

                if(isNull('datetimepicker8Value') === true ||isNull('datetimepicker10Value') === true){
                    alert("Please enter date");

                } else if(isNull('datetimepicker9Value') === true ||isNull('datetimepicker11Value') === true){
                    alert("Please enter time");
                } else if(date8Value == date10Value){
                   var pm9 = date9Value.substring(5,8);
                   var pm11 = date11Value.substring(5,8);
                   var time9 = date9Value.substring(0,5);
                   var time11 = date11Value.substring(0,5);
                   // alert(pm9);
                   // alert(pm11);
                   // alert(time9);
                   // alert(time11);
                   if(pm9 == 'PM' && pm11 == 'AM'){
                        alert("invalid time");
                   }
                   else
                   {
                          var from = $('#datetimepicker8Value').val()+" "+$('#datetimepicker9Value').val();
                           var to = $('#datetimepicker10Value').val()+" "+$('#datetimepicker11Value').val();
                           var linkId = <?php echo $linkId; ?>;
                           var host = <?php echo $host  ?>

                            $.ajax({
                                type: "POST",
                                url: base_url+'index.php?controller=test&function=generateLinkCode',     
                                data: {linkId : linkId, from :from, to:to},
                                dataType: 'html',            
                                success: function(data) {
                                    if(data) {
                                      $('.modal-title').html("success");
                                      //$('#modal-body').html("Link Created: <br/><hr>"+base_url+"quiz/"+host+"/"+linkId+"/"+data+"</p>");
                                      $('#myModal').modal();

                                      $('<h4>', {
                                         class: 'fontapply',
                                          text: "Link Created:" ,
                                          id: 'pln'
                                              }).appendTo('#modal-body');

                                  var lnk = base_url+"quiz/"+host+"/"+linkId+"/"+data;
                              $('#modal-body').append( $('<input type="text">').attr({'id':'txtLink','readonly':'readonly','class':'form-control','value':lnk}) );
                                     
                                     $('<h4>', {
                                         class: 'fontapply',
                                          text: "Note: use this link to assign test" ,
                                          id: 'pln-small'
                                              }).appendTo('#modal-body');

                                      $('#modal-body').append( $('<input type="button">').attr({'id':'btnOk','class':'btn','value':'OK'}) );
                                    }
                                }, 
                            }); 
                   }
                }
                else
                {
                      var from = $('#datetimepicker8Value').val()+" "+$('#datetimepicker9Value').val();
                       var to = $('#datetimepicker10Value').val()+" "+$('#datetimepicker11Value').val();
                       var linkId = <?php echo $linkId; ?>;
                       var host = <?php echo $host  ?>

                        $.ajax({
                            type: "POST",
                            url: base_url+'index.php?controller=test&function=generateLinkCode',     
                            data: {linkId : linkId, from :from, to:to},
                            dataType: 'html',            
                            success: function(data) {
                                if(data) {
                                  $('.modal-title').html("success");
                                  //$('#modal-body').html("Link Created: <br/><hr>"+base_url+"quiz/"+host+"/"+linkId+"/"+data+"</p>");
                                  $('#myModal').modal();

                                  $('<h4>', {
                                     class: 'fontapply',
                                      text: "Link Created:" ,
                                      id: 'pln'
                                          }).appendTo('#modal-body');

                              var lnk = base_url+"quiz/"+host+"/"+linkId+"/"+data;
                          $('#modal-body').append( $('<input type="text">').attr({'id':'txtLink','readonly':'readonly','class':'form-control','value':lnk}) );
                                 
                                 $('<h4>', {
                                     class: 'fontapply',
                                      text: "Note: use this link to assign test" ,
                                      id: 'pln-small'
                                          }).appendTo('#modal-body');

                                  $('#modal-body').append( $('<input type="button">').attr({'id':'btnOk','class':'btn','value':'OK'}) );
                                }
                            }, 
                        }); 
                }
            });
           
            $('#datetimepicker10').datetimepicker({

                    pickTime: false,
                    minDate:today,   

                });
            $('#datetimepicker11').datetimepicker({
                    pickDate: false
                });

            $("#datetimepicker8").on("dp.hide",function (e) {

                //$('#datetimepicker8').data("DateTimePicker").setMinDate(e.date);
                $('#datetimepicker10').data("DateTimePicker").setMinDate(e.date);
               $('#datetimepicker10Value').val($('#datetimepicker8Value').val());
               $('#datetimepicker8').data("DateTimePicker").setMinDate({ defaultDate: -1 });
                  
            });

            $("#datetimepicker10").on("dp.hide",function (e) {
               $('#datetimepicker8').data("DateTimePicker").setMinDate({ defaultDate: -1 });
              
            });
        });
    </script>
<script type="text/javascript">
    $().ready(function(){

    $('#footer').css("padding-top","30px");
    $('#btnPreview').click(function(){
       var path=base_url+"index.php?controller=quiz&function=preview&id=<?php echo $linkId; ?>";
       window.open(path,'_blank');
    });

    $(document).on('click', '#btnOk',function(){
      location.reload();
    });
     $('#btnFinish').click(function(){
         
    });

    // $('.greenclick').click(function(){
    //   $linkid = <?php echo $_GET['id'] ?>;
    //   $.ajax({

    //     type: "POST",
    //     data: {linkId:$linkid},
    //     url: base_url+"index.php?controller=test&function=linksListing",
    //     dataType: "html",

    //     success:function(response){
    //       alert(response);
    //     },
    //   });
    // });
   
});
</script>
