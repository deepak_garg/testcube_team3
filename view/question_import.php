</div><!-- end of dash-right-content-->
  <div id="dash-right-content">
    <div id="content-border" class="blue-background"></div>
      <div id="content-header">
         	<div id="text">Import Questions from excel file</div>
            <div id="heading"><span class="icon glyphicon glyphicon-th margin-right5"></span><span>QUESTIONS</span></div>
      </div> <!-- end of content header-->
    <div id="header-arrow" ></div>
            
    <div id="content">
      	<div class="row-mid">
            <div id="wizard1" class="wizard">Step 1: Download the template for adding questions in file
              <a id="file-link" href="<?php echo SITE_PATH. 'data/template.xls'?>"><div id="file-download" ></div></a>
              <span class="txtNotice">Note: Must Follow the template to import questions or else import will not work.</span>
            </div> <!-- end of wizard 1 -->
           <form  id="upload">
            <div id="wizard2" class="wizard">Step 2: Browse the file having questions to import
              <input type="file" name="file" class="form-control fileinpt" data-trigger="fileinput" />
              <span class="txtNotice">Note: Maximum file size upto 20MB.</span>
            </div>
            <div id="wizard3" class="wizard">Step 3: Import Questions from File. 
                <div class="fileinpt filebtn"><input type="submit" class="btn" value="Import Questions" id="btnFileUpload" /></div>
               
             <span class="txtNotice"> Note: Category not available in list will be created automatically.</span>
              <input type = "hidden" id="fileHid" />
              </form>
            </div>
    </div>	<!-- end of content -->
    <?php require_once("footer.php"); ?>   <!-- include footer and scripts -->
    </div>  <!-- end of dash-right-content-->
    </div> <!-- end of dash-content -->
<script type="text/javascript"> 
$('#footer').css("padding-top","30px");

$( '#upload' ).submit( function( e ) {
    $.ajax( {
      url: base_url + 'index.php?controller=file&function=fileUpload',
      type: 'POST',
      data: new FormData( this ),
      processData: false,
      contentType: false,
      
      success : function(data){
          $('#fileHid').val(data);

          $.ajax({
            type: "POST",
             data: {fileP:data}, //you can insert url argumnets here to pass to api.php for example "id=5&parent=6"
            url: base_url+'index.php?controller=file&function=addQuestion', //the script to call to get data          
           
            dataType: 'html',
            
            success: function(data) {
               
                if(data == 1) {
                  window.location.replace(base_url + 'index.php?controller=dashboard&function=question');
                }
                
            }, 
        });    
      },
    } );
    e.preventDefault();
  } );




// $("#btnFileUpload").click(function(e){
// //alert("hi");
//     $.ajax({

//         type: "POST",
//         data: $('#upload').serialize(),
//         url: base_url + 'index.php?controller=file&function=fileUpload',
//         dataType: 'html',

//         success:function(data){


//         },  
//     });
//     e.preventDefault();
// });


</script>
