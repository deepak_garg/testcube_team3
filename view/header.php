<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<html>
	<head>

	<title>TestCube - Common Adaptive Test</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">	
	<link rel="stylesheet" href="<?php echo CSS_PATH;?>layout.css" />
	<link rel="stylesheet" href="<?php echo CSS_PATH;?>style.css" />
	<link rel="stylesheet" href="<?php echo CSS_PATH;?>bootstrap.css" />
	
	</head>
	<body>
	
	 <div id="wrapper">
<div id="header">
	   <div id="header-logo">
	   <span id="logo-text"><strong><?php echo $lang->testcubeLogo; ?></strong></span>
	   </div> <!-- end of header-logo -->
	   <div id="header-menu">
	     <div id="nav">
	     <ul class="nav-menu">
	     	<li class="none nav-menu-links"><a href="index.php" class="nav-menul nav-current"><?php echo $lang->headerLoginText; ?></a></li>
	     	<li class="none nav-menu-links"><a href="#" class="nav-menul"><?php echo $lang->headerAboutText; ?></a></li>
	     	<li class="none nav-menu-links"><a href="#" class="nav-menul"><?php echo $lang->headerServiceText; ?></a></li>
	     	<li class="none nav-menu-links"><a href="#" class="nav-menul"><?php echo $lang->headerHelpText; ?></a></li>
	     </ul>
	     
	      </div> <!-- end of nav -->
	   </div> <!-- end of menu -->
	  </div> <!-- end of header -->

