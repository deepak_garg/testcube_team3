
<?php 
  $quesData = $arrData[0];
  $categoryData = $arrData[1];

?>
</div><!-- end of dash-right-content-->
  <div id="dash-right-content">
    <div id="content-border" class="blue-background"></div>
      <div id="content-header">
         	<div id="text">Question Bank for the Test</div>
            <div id="heading"><span class="icon glyphicon glyphicon-th margin-right5"></span><span>QUESTIONS</span></div>
      </div> <!-- end of content header-->
    <div id="header-arrow" ></div>
            
    <div id="content">
      	<div class="row-mid">
        <div id="content-head">
                          <div class="float-left" id="content-head-left"></div>
                          <!-- end of content-head-left -->
                          <div class="float-right" id="content-head-right">
                              <a class="btn font-apply inlineBlock" href="index.php?controller=dashboard&function=question">Back to
                                  Question Bank</a>
                          </div>
                          <!-- end of content-head-right -->
                      </div>
                      <span class="fontapply">Question Type: </span> 
        
                      <select name="q_type" id="q_type" class="form-control width150" disabled="">
                          <option value="0" <?php if($arrData['type'] == 0){echo "selected";} ?> >Multiple Choice</option>
                          <option value="1" <?php if($arrData['type'] == 1){echo "selected";} ?> >True/False</option>
                      </select> 

                     <span class="fontapply margin-left20">Category <span class="text-danger">*</span>:</span>
                        <select class="form-control width150" id="categoryselect" class="lstbox" required>
                            <option value="">--Select Category--</option>
                            <?php foreach($categoryData as $x)
                              {
                                 $removeCharacter = array('\r\n');
                             ?>
                            <option value="<?php echo $x['id']; ?>" <?php if($x['id'] == $quesData['name']){echo "selected";} ?> ><?php echo $x['categoryName']; ?></option>
                            <?php } ?>              
                        </select>
                        
                      


                      <p></p>


                        <fieldset class="fontapply question-field">
                         <div id="divMCQ">
                              <form id="frmEdit" action="index.php?controller=question&function=editQuestion" method="POST">
        
                                <h3>Question <span class="text-danger">*</span></h3>
                                <textarea id="mtxtQuestion" name="txtQuestion" class="question-txtarea" focus ><?php echo str_replace($removeCharacter, " ", htmlspecialchars(stripslashes($quesData['description']),ENT_QUOTES, 'UTF-8'));?></textarea>
  
                                <h3>Answers</h3>
                              
                                <ol class="olAnswers">
                                <?php 

                                  $option_id=explode(',',$quesData['optid']);
                                  $options=explode(',', $quesData['options']);
                                  $len=count($options);
                                  $optCount=0;

                                  for($i=0;$i<$len;$i++)
                                    {

                                    
                                ?>
                                  <li>
                                  <p>
                                    <input type="hidden" name="optid[]" value="<?php echo $option_id[$optCount]; ?>">
                                   <input type="checkbox" name="answer[]" value='<?php echo ($i==0?"0":$i/2); ?>' <?php if($options[$i] == 1){echo "checked";} $i++; ?> > Mark Correct
                                  </p>
                                  <textarea class="answer-txtarea" name="opt[]" ><?php echo $options[$i]; ?></textarea>
                                    </li>
                                  <?php $optCount++;} ?>
                                </ol>                              
                                <input type="hidden" name="type" value="<?php echo $quesData['type']; ?>">
                                <input type="hidden" name="id"    value="<?php echo $quesData['id']; ?>">
                                <input id="category" type="hidden" name="category" value="<?php echo $quesData['name']; ?>">
                                <span id="ptsText">Pts</span><input type="text" value="<?php echo $quesData['points']; ?>" id="txtPoints" name="txtPoints" />
                                <input id="sbmtSaveQuestion" type="submit" value="Save" class="btn">
                            
                                  </span>
                              </form>
                              <a id="ancrAddOpt"> <div class="add-btn"><span>Add More Options</span></div></a>    
                        </div>
                          <!-- end of mcq-div -->
                      </fieldset>
                

                </div>
                   
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Status</h4>
                  </div>
                  <div id="modal-body" class="modal-body">
                    <h3 class="fontapply h3Status">Question Added</h3>
                    <input type="button" data-dismiss="modal" aria-hidden="true" class="btn btnStatus" value="OK"> 
                  </div>
                </div>
              </div>
            </div>
                </div>	<!-- end of content -->
     	   <?php require_once("footer.php"); ?>	<!-- include footer and scripts -->
        </div>	<!-- end of dash-right-content-->
    </div> <!-- end of dash-content -->
    

    <script type="text/javascript">

    //function to check question add status
    <?php if(isset($_GET['success']) && ($_GET['success'] == 1)) {
      echo "$('#myModal').modal('show');";
    } ?>

//function to set category hidden type values from category dropdownlist
$('#categoryselect').change(function() {
  var val=$("#categoryselect option:selected").val();
        $('#category, #category1').attr("value",val);
});


//function to validate submission
$('#frmMCQ').submit(function(e){

  var ques=$('#mtxtQuestion').val();
  var cat = $('#categoryselect :selected').val();
  if(!cat)
  {
    e.preventDefault();
    $('#categoryselect').focus();
  }
  
  if(!ques) {
    e.preventDefault();
  $('#mtxtQuestion').focus();
  }
});

$(document).ready(function () {
        var counter = 4;
        $('#ancrAddOpt').click(function(){

            $('<li/>').html(
                $('<p>').html( $('<input />', { type: 'checkbox', name: 'answer[]', value: counter })).append(" Mark Correct"))
                .append( $('<textarea>').attr({'name':'opt[]'}).addClass("answer-txtarea") )
                .appendTo( '.olAnswers' );      
                counter++;
            });

  
});

 
$(document).ready(function() {


    $('#categoryselect').change( function() { 
            oTable.fnFilter( $(this).val() ); 
       });


    $('.view-btn, .hide-btn').click(function () {
         

         if ($(this).attr("class") == "view-btn") {
            $(this).removeClass("view-btn");
            $(this).addClass("hide-btn");
            
        }
        else{
            $(this).removeClass("hide-btn");
            $(this).addClass("view-btn");
        }


        var nTr = $(this).parents('tr')[0];
        if ( oTable.fnIsOpen(nTr) )
        {
            /* This row is already open - close it */
            this.src = "../examples_support/details_open.png";
            oTable.fnClose( nTr );
        }
        else
        {
            /* Open this row */
            this.src = "../examples_support/details_close.png";
            oTable.fnOpen( nTr, fnFormatDetails(oTable, nTr), 'details' );
        }
    } );
	 
} );

$.fn.dataTableExt.oStdClasses["sFilter"] = "Dfilter";
$('#example_wrapper').css("margin-top","50px");
</script>
<script>

$(document).ready(function() {
     $('#chkAll').click (function () {
          $(':checkbox[name=deleteall]').prop('checked', this.checked);
        });
});  


//load delete question page in popup on "delete icon"
$('.delete-btn, .deletebtn').click(function(){
    $('.modal-dialog').css("width","800px");
    $('#myModalLabel').html("Delete Question");
    $.ajax({
            type: "POST",
            url: base_url+'index.php?controller=question&function=questionDelete', //the script to call to get data          
            data: "", //you can insert url argumnets here to pass to api.php for example "id=5&parent=6"
            dataType: 'html',
            
            success: function(data) {
                $('#modal-body').html(data);
            }, 
        });                        
});

//load view question in popup on question name
$('.ancrQuestion').click(function(){
    $('.modal-dialog').css("width","800px");
    $('#myModalLabel').html("Question Details");
    $.ajax({
            type: "POST",
            url: base_url+'index.php?controller=question&function=questionView', //the script to call to get data          
            data: "", //you can insert url argumnets here to pass to api.php for example "id=5&parent=6"
            dataType: 'html',
            
            success: function(data) {
                $('#modal-body').html(data);
            }, 
        });                        
});

function addClassCurrent(element)
{
	var el="#ico"+element;
	$(el).addClass("current");
}

function removeClassCurrent(element)
{
	var el="#ico"+element;
	$(el).removeClass("current");
}
$('#footer').css("padding-top","30px");
</script>
