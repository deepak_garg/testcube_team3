<?php
$username ="";
$name ="";
$mytheme ="0";
if(isset($arrData) && !empty($arrData)) {
	$username = $arrData['username'];
	$name = $arrData['fname']." ".$arrData['lname'];
	$mytheme = $arrData['theme'];
} else {
	$name="guest";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<html>
	<head>
	
    
	<title>TestCube - Common adaptive test</title>	
	<link rel="stylesheet" href="<?php echo CSS_PATH;?>bootstrap.css" />
	<link rel="stylesheet" href="<?php echo CSS_PATH;?>layout.css" >
	<link rel="stylesheet" href="<?php echo CSS_PATH;?>style.css" />
	<link rel="stylesheet" href="<?php echo CSS_PATH;?>DT_bootstrap.css" />
	<link rel="stylesheet" href="<?php echo CSS_PATH;?>bootstrap-datetimepicker.min.css" />

	<?php if($mytheme == "1"){ ?>
	<link rel='stylesheet' id="theme" href="<?php echo CSS_PATH;?>themes/red.css" />
	<?php } else if($mytheme == "2"){ ?>
	<link rel='stylesheet' id="theme" href="<?php echo CSS_PATH;?>themes/green.css" />
	<?php } else if($mytheme == "3"){ ?>
	<link rel='stylesheet' id="theme" href="<?php echo CSS_PATH;?>themes/yellow.css" />
	<?php } else { ?>
	<link rel='stylesheet' id="theme" href="<?php echo CSS_PATH;?>themes/blue.css" />
	<?php } ?>
 
    
   
	</head>
	<body>
	
	 <div id="wrapper">
<div id="header">
	   <div id="header-logo">
	   <span id="logo-text"><strong>Test</strong>Cube</span>
	   </div> <!-- end of header-logo -->
	   <div id="header-menu">
	     <div id="nav">
	     <ul class="nav-menu">
	     	<li class="none nav-menu-links"><a href="index.php" class="nav-menul font-small">HELP/SUPPORT</a></li>
	     	<li class="none nav-menu-links"><a href="index.php" class="nav-menul font-small">SETTINGS</a></li>
	     	<li class="none nav-menu-links">
            <div class="dropdown">
            <a id="dropdownMenu1" href="#" data-target="#" data-toggle="dropdown" class=" nav-menul nav-current font-small user-text"><span class="glyphicon glyphicon-user margin-right10 blue font-size15"></span><?php echo $name; ?><span class="glyphicon glyphicon-play margin-left10 text-rotate"></span></a>
            <ul class="dropdown-menu customDrop1" role="menu" aria-labelledby="dropdownMenu1">
    		<li role="presentation"><a role="menuitem" tabindex="-1" href="index.php?controller=dashboard&function=profile" class="textnone fontapply">Account Details</a></li>
    		<li role="presentation"><a role="menuitem" tabindex="-1" href="#" class="textnone fontapply">Settings</a></li>
    		<li role="presentation"><a role="menuitem" tabindex="-1" href="#" class="textnone fontapply">Plan info</a></li>
    		<li role="presentation" class="divider"></li>
    		<li role="presentation"><a role="menuitem" tabindex="-1" href="index.php?controller=login&function=logout" class="textnone fontapply">Sign Out</a></li>
  		</ul>
</div></li>
	     </ul>
         

	      </div> <!-- end of nav -->
	   </div> <!-- end of menu -->
	  </div> <!-- end of header -->
  	<div id="dash-content">
    	<div id="dash-left-content">
