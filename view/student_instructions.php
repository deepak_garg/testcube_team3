<?php

$testName = "Test Name";
$time = "20 Minutes";
$passMarks ="50";
 if(isset($arrData) && !empty($arrData)) {

    $testName = $arrData['testName'];

    $time = $arrData['time'];
    if($time >3600) {
      $hr = intval($time/3600);
      $min = $time%3600;
      $min = intval($min/60);
      $time = $hr." Hours ".$min;
    } else {
       $time = intval($time/60);
    }
    $time .= " Minutes";
    $passMarks = $arrData['passMarks'];

 }

?>
  <div id="content">

   <div id="main-container">
       <div class="row-mid margin0auto">
   		 <h2><span class="black">Test:</span> <?php echo $testName;?></h2>
      
   		<fieldset class="fieldbox"><legend class="legendtext">Instructions</legend>
   			<ul class="instructions">
          <h3>This test</h3>
   				<li>should be finished in single sitting only</li>
          <li>will not allow you to go back and make changes</li>
          <li>has mandatory to select answer</li>
   				<li>has a limit of <?php echo $time; ?></li>
   				<li>has a passing mark of <?php echo $passMarks; ?>%</li>
   			</ul>
   			<br><br>
   			<a href="<?php echo SITE_PATH; ?>index.php?controller=quiz&function=startTest" id="btnStart" class="btn fontapply float-right"> start test</a>
   		</fieldset> <!-- end of fieldset -->
    
   </div> <!-- end of main container -->
  
  </div> <!-- end of content -->
   <?php
require_once("student_footer.php")
?>
 </div> <!-- end of wrapper -->
<script type="text/javascript">

/*$('#btnStart').click(function(){
   $.ajax({
            type: "POST",
            url: "<?php echo SITE_PATH; ?>index.php?controller=quiz&function=startTest",     
            data: '',
            dataType: 'html',            
            success: function(data) {
                $('#content').replaceWith(data);
            }, 
        });       
});*/

</script>

</body>
</html>