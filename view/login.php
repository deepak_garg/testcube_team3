<?php 	
    require_once(LIBRARY_ROOT.'recaptchalib.php');
    $publickey = "6LfidfASAAAAAJotaDZfMz_MdRBlG2OLJdN6CHEs"; // you got this from the signup page
    if(isset($arrData['username']) && !empty($arrData['username'])){
    	$USERNAME_MSG = $arrData['username'];
    } else {
    	$USERNAME_MSG = "";
    }
    if(isset($arrData['validateLoginFlag']) && ($arrData['validateLoginFlag'] == 1)) {
        $LOGIN_ERROR_MSG = $lang->errorWrongCred; 
    } else {
        $LOGIN_ERROR_MSG = "";
    }
    
        $REGISTER_ERROR_MSG = "";

?>	
  <script type="text/javascript">
	var RecaptchaOptions = {
		theme : 'white' 
	};


    </script>
	  <div id="content">
	  	<div id="login-window">
	  	  <div id="login-top">
	  	  	<div id="user-icon"></div>	<!-- user icon -->
	  	  	<span id="login-text"><?php echo $lang->loginText; ?></span>
	  	  </div> <!-- end of lg-top -->
	  	  <hr />
	  	  <p id="login-text2"><?php echo $LOGIN_ERROR_MSG; ?></p>
	  	  <div id="lg-middle">
	  	  <form id="login" action="<?php echo SITE_PATH; ?>index.php?controller=login&function=loginValidate" method="POST" >
	  	    <input type = "text" id="txtUserName" name="txtUserName" tabindex=1  minlength=5 maxlength=150 
	  	    placeholder="<?php echo $lang->placeHolderUsername; ?>" value="<?php echo $USERNAME_MSG;?>" />
            <span class = "errorTxt" id="errUserName"></span>
	  	    <input type="password"  id="txtPassword" minlength=8 tabindex=2  autocomplete="off"  name="txtPassword" 
	  	    placeholder="<?php echo $lang->placeHolderPassword; ?>" />
	  	     <span class="errorTxt" id="errPassword"></span>
	  	    <input type="submit" class="button-style" value="<?php echo $lang->signinText; ?>" tabindex=3 id="btnSubmit"/>
	  	  </form>
	  	  </div> <!-- end of lg-middle -->
          
          
	  	  <div id="lg-bottom">
         
	  	    <div id="oss-logo"></div>
	  	    
	  	    <div id="login-links">
	  	      <a id="ancrRegister" href="#"><span><?php echo $lang->registerText; ?></span></a>
	  	      <a id="ancrForgetPassword" href="#" data-toggle="modal" data-target="#myModal" class="margin-left15 margin-right25"><span><?php echo $lang->forgetpasswordText ?></span></a>

	  	     
            </div>
             <a href="#" class="btn-google"><?php echo $lang->loginGoogle; ?></a>
	 
            </div> <!-- end of lg-bottom -->
        </div> <!-- end of login window -->

    </div> <!-- end of content -->

	  	     <!-- Register Popup Content -->
		<div id="popupContent">
		  	 <!-- start of lightbox -->
            <div id="light" class="light-box">
      	     <div class="light-box-banner"><span class="float-left margin-left25"><?php echo $lang->headingRegisterNewUser; ?></span>
		<a href="index.php" >
            		<div id="closePopup" class="close-btn float-right"></div></a>                
            	</div> <!-- end of white-content-banner -->
            	
            	
            	<div class="light-box-content">

                 <form id="frmRegister" name="frmRegister"  action="<?php echo SITE_PATH?>index.php?controller=login&function=registerValidate" method="POST">
                 <span><?php echo $REGISTER_ERROR_MSG; ?></span>
                 <span><?php echo $lang->labelUsername; ?></span>
                	<input type="text" name="txtUsername" tabindex=20 id="txtRegUsername"  minlength=5 maxlength=20 placeholder="<?php echo $lang->placeholderUsername ?>" autocomplete="off" 
                	class="light-box-content-textbox" />
                	<span class="formInfo"><a id="one" class="jTip" href="#" data-container="body" title="<?php echo ucfirst($lang->infoUnameInfoHead); ?>"  data-content="<ol><li><?php echo $lang->infoUnameInfo1; ?></li>
									  <li><?php echo $lang->infoUnameInfo2; ?></li>
									  <li><?php echo $lang->infoUnameInfo3; ?></li>
									  <li><?php echo $lang->infoUnameInfo4; ?></li></ol>" >?</a></span>
                	<span class="regiser-errorTxt" id="errRegUserName"></span>
                	<p></p>
                	
                	<span><?php echo $lang->labelEmailText; ?></span>
                	<input type="text" name="txtEmail" tabindex=21 id="txtEmail" placeholder="<?php echo $lang->placeholderEmail; ?>" maxlength=150 autocomplete="off" 
                	class="light-box-content-textbox"  />
                	<span class="formInfo"><a id="two" class="jTip" href="#" data-container="body" title="<?php echo $lang->infoEmailInfoHead; ?>"
                	data-content="<ol><li><?php echo $lang->infoEmailInfo1; ?></li>
									  <li><?php echo $lang->infoEmailInfo2; ?></li>
                                  </ol>" >?</a></span>
									  
                	<span class="regiser-errorTxt" id="errEmail"></span>
                	<p></p>
                	<span><?php echo $lang->labelFirstName; ?></span>
                	<input type="text" name="txtFirstName" tabindex=22 id="txtFirstName" maxlength=20 placeholder="<?php echo $lang->placeholderFirstname; ?>" autocomplete="off" 
                	class="light-box-content-textbox"  />
                	<span class="formInfo"><a id="three" class="jTip" href="#" data-container="body" 
						  title="<?php echo $lang->infoNameInfoHead; ?>" 
						  data-content="
								  <ol>
								    <li><?php echo $lang->infoNameInfo1; ?></li>
									<li><?php echo $lang->infoNameInfo2; ?></li>
									<li><?php echo $lang->infoNameInfo3; ?></li>
									<li><?php echo $lang->infoNameInfo3; ?></li>
								</ol>" >?</a></span>
                	<span class="regiser-errorTxt" id="errFirstName"></span>
                	<p></p>
                	<span><?php echo $lang->labelLastName; ?></span>
                	<input type="text" name="txtLastName" tabindex=23 id="txtLastName" 
						   maxlength=20 placeholder="<?php echo $lang->placeholderLasttname; ?>"  autocomplete="off" class="light-box-content-textbox" />
                	
					<span class="regiser-errorTxt" id="errLastName"></span>
                	<p></p>
                	<span id="captchaPlaceholder"><?php echo recaptcha_get_html($publickey); ?></span>

                	<p></p>
                    <span class="regiser-errorTxt" id="errCaptcha"></span>
			        <span id="progressBar"></span>
                	
                	<p><span class="regiser-errorTxt" id="errCaptcha"></span></p>
					 <span id="progressBar"></span>

                	<input type="hidden" name="type" value="admin">
                	
                	<input type="submit" id="btnRegister" tabindex=25 class="btn float-left margin-left20" value="<?php echo $lang->buttonRegister; ?>" />
                	<input type="reset" id="btnReset" tabindex=26 class="btn float-left margin-left20" value="<?php echo $lang->buttonReset; ?>" />
			
               	</form>

            	</div> <!-- end of white-content-content-->

      		</div> <!-- end of light-box  -->

      		 <div id="fade" class="black_overlay"></div>
	  </div> <!-- end of popup-content -->

      <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                  </div>
                  <div id="modal-body" class="modal-body">
                    ...
                  </div>
                </div>
              </div>
            </div>

	 
	 
	 
	
