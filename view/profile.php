<?php 
if(isset($arrData) && !empty($arrData)){
   
    $id = $arrData['id'];
    $email = $arrData['email'];
    $userName = $arrData['username'];
    $regDate = $arrData['regDate'];
    $lastlogin = $arrData['lastlogin'];
    $fname = $arrData['fname'];
    $lname = $arrData['lname'];
    $mytheme = $arrData['theme'];
    $name = $arrData['name'];
    $contact = $arrData['contact'];
    $photo = $arrData['photo'];
    $language = $arrData['lang'];

    //print_r($arrData);
}

?>


        <link rel="stylesheet" href="<?php echo CSS_PATH;?>profile.css" />
</div><!-- end of dash-right-content-->
  <div id="dash-right-content">
    <div id="content-border" class="blue-background"></div>
      <div id="content-header">
         	<div id="text">User Profile</div>
            <div id="heading"><span class="icon glyphicon glyphicon-user margin-right5"></span><span>USERS</span></div>
      </div> <!-- end of content header-->
    <div id="header-arrow" ></div>
            
    <div id="content">
    
    
    <body>
        <div id="profile_wrapper">
            <div id="left_pane">
                <div id="left_top">
                    <div id="user-pic"></div>
                </div>
                <div id="left_bottom">
                <div id="proText" class="proText"><?php echo strtoupper($name);?></div>
                    <div id="info">INFO
                        <p>Registration Date: <?php $date = date_create($regDate); echo date_format($date,'d-M-Y'); ?></p>
                        <p>Last Login: <?php $date = date_create($lastlogin); echo date_format($date,'d-M-Y H:i:s'); ?></p>
                        <p>Account Type: Admin</p>

                    </div>
                </div>
            </div>
            <div id="right_pane">
                <div id="selectedTabBox"></div>
                <div id="nav-tabs">
                    <ul class="nav nav-tabs" >
                        <li class="active"><a href="#home" data-toggle="tab">General</a></li>
                        <li><a href="#profile" data-toggle="tab">Settings</a></li>
                        <li><a href="#messages" data-toggle="tab">Change Password</a></li>
                        <li><a href="#settings" data-toggle="tab">Users</a></li>
                    </ul>

<!-- Tab panes -->
                        <div class="tab-content">
                        <div class="tab-pane active" id="home">
                            <div id="generalContent">
                            <p>Email <input type="text" class="form-control" value="<?php echo $email;?>" disabled="disable"/></p>
                            <p>Username <input type="text" class="form-control" value="<?php echo $userName;?>" disabled="disable" /></p>
                            <p>First Name <input type="text" class="form-control" value="<?php echo ucfirst($fname); ?>" id="txtFname" required /></p>
                            <div id="errFnameMsg" class="text-success" ></div>
                            <p>Last Name <input type="text" class="form-control"  value="<?php echo ucfirst($lname); ?>" id="txtLname"/></p>
                            <div id="errLnameMsg" class="text-success" ></div>
                            <p>Phone <input type="text" class="form-control" value="<?php if($contact) { echo $contact;} ?>" id="txtContact" max-length="10"/></p>
                            <div id="errContactMsg"></div>
                            <input type="submit" class="btn proSave"  value="Save" id="btnProfileSave" />
                            </div>
                        </div>
                        <div class="tab-pane" id="profile">

                            <span> language</span>
                            <span>
                                <select id="language" class="form-control">
                                    <option value="en">English</option>
                                    <option value="fr">French</option>
                                    <option value="hn">Hindi</option>

                                </select>
                            </span>
                            <p></p> 
                            <span> Theme</span>
                            <input type="button" class="bt btn-primary margin-left20" value="Blue" />
                            <input type="button" class="bt btn-success" value="Green" />
                            <input type="button" class="bt btn-warning"  value="Yellow" />
                            <input type="button" class="bt btn-danger"  value="Red" />
                            <input type="hidden" id="color" value="<?php echo $mytheme ?>">
                            <p></p>
                             <div id=langMsg class="text-success"></div>
                            <input type="submit" class="btn proSave"  value="Save" id="btnThemeLang"/>
                           



                        </div>
                        <div class="tab-pane" id="messages">
                        <h3>Set your new  Password here</h3>
                            <div id="generalContent2">
                            
                            <p>Password <input type="password" class="form-control" id="oldPassword"></p>                           
                            <p>New Password <input type="password" class="form-control" id="newPassword"></p>
                                <div id="errPassMsg2"></div> 
                            <p>Confirm Password <input type="password" class="form-control" id="confirmPassword"></p>
                            
                            </div>
                            <div id="errPassMsg" class="text-success"></div> 
                            <input type="submit" class="btn proSave"  value="Save" id="btnSave" name="btnSave" />
                        </div>
                        <div class="tab-pane" id="settings">No Users</div>
                        </div>
                
            </div>
        </div>
    
                </div>	<!-- end of content -->
     	   <?php require_once("footer.php"); ?>	<!-- include footer and scripts -->
        </div>	<!-- end of dash-right-content-->
    </div> <!-- end of dash-content -->

    <script>
        $("#btnSave").click(function(){

           var oldPassword = $("#oldPassword").val();
           var newPassword = $("#newPassword").val();
           var confirmPassword = $("#confirmPassword").val()
           //alert(passValue);
           $.ajax({

            type: "POST",
            data: {oldPassword:oldPassword,newPassword:newPassword,confirmPassword:confirmPassword},
            url: base_url +'index.php?controller=dashboard&function=profilePassword', 
            dataType: 'json',

             beforeSend: function() {
    
                },
                success: function(response) {
                        
                        if(response.passFlag == 1 ) {
                            $('#errPassMsg').html("Password cannot be blank").css("color","red");
                            
                                
                        } else if(response.passFlag == 2 ){
                            $('#errPassMsg').html("Password cannot be blank").css("color","red");

                        } else if(response.passFlag == 3 ){
                            $('#errPassMsg').html("Password cannot be blank").css("color","red");

                        } else if(response.passFlag == 4 ){
                            $('#errPassMsg').html("Invalid Password").css("color","red");

                        } else if(response.passFlag == 5 ){
                            $('#errPassMsg').html("Invalid Password").css("color","red");

                        } else if(response.passFlag == 6 ){
                            $('#errPassMsg').html("Invalid Password").css("color","red");

                        } else if(response.val == "" ){
                            $('#errPassMsg').html("Invalid Password");

                        } else {
                            $('#errCat').html("");
                            if(response.val== 1 ) {
                                $('#errPassMsg').html("Password Set!");
                            }
                        }
               
                },
               
                
                complete: function() {
    
                },
                error: function() {
    
                }

           });
        })


$("#btnProfileSave").click(function(){

var profileFname = $('#txtFname').val();
var profileLname = $('#txtLname').val();
var profileContact = $('#txtContact').val();


    $.ajax({

        type: "POST",
        data: {profileFirstName:profileFname,profileLastName:profileLname,profileContact:profileContact},
        url: base_url +'index.php?controller=dashboard&function=profileSettings',
        dataType: 'json',

        beforeSend:function(){

        },

        success:function(response){
            if(response.profileFlag == 1){
                $('#errContactMsg').html("");
                 $('#errLnameMsg').html("");
                $('#errFnameMsg').html("Firstname cannot be blank").css("color","red");
            } else if(response.profileFlag == 2){
                $('#errContactMsg').html("");
                 $('#errLnameMsg').html("");
                $('#errFnameMsg').html("First name is invalid").css("color","red");
            } else if(response.profileFlag == 3){
                 $('#errContactMsg').html("");
                 $('#errFnameMsg').html("");
                $('#errLnameMsg').html("Last name is invalid").css("color","red");
            } else if(response.profileFlag == 4){
                 $('#errLnameMsg').html("");
                 $('#errContactMsg').html("");
                $('#errContactMsg').html("Contact is invalid").css("color","red");
            } else if(response.val == ""){
                $('#errContactMsg').html("");
                 $('#errLnameMsg').html("");
                $('#errFnameMsg').html("Server error").css("color","red");
            } else if(response.val == 1){
                $('#errFnameMsg').html(" First Name Updated");
                $('#errLnameMsg').html(" Last Name Updated");
                $('#errContactMsg').html("");
            } else {
                $('#errLnameMsg').html("");
                $('#errContactMsg').html("");
                $('#errFnameMsg').html("");
            }
        }

    });

});

// $('#language').change(function(){
//     // alert($(this).val());
//     var lang = $(this).val();
// });

$('#btnThemeLang').click(function(){
    var langCode = $('#language').val();
    var Theme    = $('#color').val();

    var lang = ((langCode == 'en')?'English':((langCode == 'fr')?'French':'Hindi'));

    $.ajax({

        type: "POST",
        data: {profileLanguage:langCode,profileTheme:Theme},
        url: base_url + 'index.php?controller=dashboard&function=profileLanguage',
        dataType: 'json',

        success:function(response){
            if(response.val == 1) {
                $('#langMsg').html("Changes Saved! ");
               // location.reload();
            }

        }

    });

});

$('.bt').click(function(){
    var theme = $(this).attr("value");

    if(theme === "Red"){
        $('#color').attr("value","1");
        $('#theme').attr("href","http://local.testcube.com/css/themes/red.css");
    }   
    else if(theme === "Green"){
        $('#color').attr("value","2");
        $('#theme').attr("href","http://local.testcube.com/css/themes/green.css");
    }
    else if(theme === "Yellow"){
        $('#color').attr("value","3");
        $('#theme').attr("href","http://local.testcube.com/css/themes/yellow.css");
    }
    else {
        $('#color').attr("value","0");
        $('#theme').attr("href","http://local.testcube.com/css/themes/blue.css");
        
    }
    
});

    </script>
 
