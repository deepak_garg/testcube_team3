
</div><!-- end of dash-right-content-->
  <div id="dash-right-content">
    <div id="content-border" class="blue-background"></div>
      <div id="content-header">
         	<div id="text">Categories for the Test</div>
            <div id="heading"><span class="icon glyphicon glyphicon-th margin-right5"></span><span>CATEGORIES</span></div>
      </div> <!-- end of content header-->
    <div id="header-arrow" ></div>
            
    <div id="content">
      
              <div class="row-midlarge">
            <div id="table-content">
                <div id="d-table-menu">
            <a class="deletebtn" id="delMulCat" data-toggle="modal" data-target="#myModal" title="Delete Selected Test" href="#"></a>
            <a id="btnCreate" href="#" data-toggle="modal" data-target="#myModal" class="btn create-button fontapply">+ New Category</a>
        </div>
     <table cellpadding="0" cellspacing="0" border="0" class="LStable table-bordered" id="example">
      <thead>
    <tr>
        <th><input type="checkbox" id="chkAll" /></th>
            <th>Category Name</th>
            <th class="action">Actions</th>
    </tr>
     </thead>
     
       <tbody id="d-table-body">
        <?php 
            foreach($arrData as $categoryData){
                ?>
                <tr>
            <td><input type="checkbox" name="deleteall" value="<?php echo $categoryData['id'];?>"></td>
            <td id="hi"><?php echo $categoryData['categoryName'];?></td>
            <td class = "action">
            <a href="#" data-toggle="modal" data-target="#myModal" ><div class="delete-btn" id="<?php echo $categoryData['id'];?>"></div></a>
            <a href="#" data-toggle="modal" data-target="#myModal"><div class="edit-btn" id="<?php echo $categoryData['id'];?>"></div></a>
            <a href="#"><div class="view-btn" id="<?php echo $categoryData['id'];?>"></div></a>
            <div class="div-details" id="details<?php echo $categoryData['id'] ?>">
                <div class="Quesdetails-left">
                                        Created By : <strong><?php echo $categoryData['created_by']; ?></strong><br>
                                        Created On : <strong><?php $date = date_create($categoryData['catCreatedOn']); echo date_format($date,'d-M-Y H:i:s'); ?></strong><br>
                </div>
                <div class="Quesdetails-right margin-top0 ">Status :<strong> Active</strong><br>
                                                Updated By : <strong><?php echo $categoryData['updated_by']!=""?$categoryData['updated_by']:'none'; ?></strong><br>
                                                Updated On : <strong><?php $date = date_create($categoryData['catUpdatedOn']); echo date_format($date,'d-M-Y H:i:s'); ?></strong><br>
                </div>
            </div> <!-- end of div-details -->
            </td>
            
        </tr>
          <?php  }

       ?>
    
  </tbody>
</table>
</div>  <!-- end of table content-->

                </div>

            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                  </div>
                  <div id="modal-body" class="modal-body">
                    

                  </div>
                </div>
              </div>
            </div>
                </div>	<!-- end of content -->
     	   <?php require_once("footer.php"); ?>	<!-- include footer and scripts -->
        </div>	<!-- end of dash-right-content-->
    </div> <!-- end of dash-content -->
    
    <script type="text/javascript">
	/* Formating function for row details */
function fnFormatDetails ( oTable, nTr, ele )
{
    var elemnt = "#details"+ele;
    var aData = oTable.fnGetData( nTr );
    var sOut    =  $(elemnt).html();
    return sOut;
}

	
	/* API method to get paging information */
$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
{
    return {
        "iStart":         oSettings._iDisplayStart,
        "iEnd":           oSettings.fnDisplayEnd(),
        "iLength":        oSettings._iDisplayLength,
        "iTotal":         oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage":          oSettings._iDisplayLength === -1 ?
            0 : Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
        "iTotalPages":    oSettings._iDisplayLength === -1 ?
            0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
    };
}
 
/* Bootstrap style pagination control */
$.extend( $.fn.dataTableExt.oPagination, {
    "bootstrap": {
        "fnInit": function( oSettings, nPaging, fnDraw ) {
            var oLang = oSettings.oLanguage.oPaginate;
            var fnClickHandler = function ( e ) {
                e.preventDefault();
                if ( oSettings.oApi._fnPageChange(oSettings, e.data.action) ) {
                    fnDraw( oSettings );
                }
            };
 
            $(nPaging).addClass('pagination').append(
                '<ul>'+
                    '<li class="prev disabled"><a href="#">&larr; '+oLang.sPrevious+'</a></li>'+
                    '<li class="next disabled"><a href="#">'+oLang.sNext+' &rarr; </a></li>'+
                '</ul>'
            );
            var els = $('a', nPaging);
            $(els[0]).bind( 'click.DT', { action: "previous" }, fnClickHandler );
            $(els[1]).bind( 'click.DT', { action: "next" }, fnClickHandler );
        },
 
        "fnUpdate": function ( oSettings, fnDraw ) {
            var iListLength = 5;
            var oPaging = oSettings.oInstance.fnPagingInfo();
            var an = oSettings.aanFeatures.p;
            var i, j, sClass, iStart, iEnd, iHalf=Math.floor(iListLength/2);
 
            if ( oPaging.iTotalPages < iListLength) {
                iStart = 1;
                iEnd = oPaging.iTotalPages;
            }
            else if ( oPaging.iPage <= iHalf ) {
                iStart = 1;
                iEnd = iListLength;
            } else if ( oPaging.iPage >= (oPaging.iTotalPages-iHalf) ) {
                iStart = oPaging.iTotalPages - iListLength + 1;
                iEnd = oPaging.iTotalPages;
            } else {
                iStart = oPaging.iPage - iHalf + 1;
                iEnd = iStart + iListLength - 1;
            }
 
            for ( i=0, iLen=an.length ; i<iLen ; i++ ) {
                // Remove the middle elements
                $('li:gt(0)', an[i]).filter(':not(:last)').remove();
 
                // Add the new list items and their event handlers
                for ( j=iStart ; j<=iEnd ; j++ ) {
                    sClass = (j==oPaging.iPage+1) ? 'class="active"' : '';
                    $('<li '+sClass+'><a href="#">'+j+'</a></li>')
                        .insertBefore( $('li:last', an[i])[0] )
                        .bind('click', function (e) {
                            e.preventDefault();
                            oSettings._iDisplayStart = (parseInt($('a', this).text(),10)-1) * oPaging.iLength;
                            fnDraw( oSettings );
                        } );
                }
 
                // Add / remove disabled classes from the static elements
                if ( oPaging.iPage === 0 ) {
                    $('li:first', an[i]).addClass('disabled');
                } else {
                    $('li:first', an[i]).removeClass('disabled');
                }
 
                if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
                    $('li:last', an[i]).addClass('disabled');
                } else {
                    $('li:last', an[i]).removeClass('disabled');
                }
            }
        }
    }
} );

$(document).ready(function() {

					/* Formating function for row details */

	var oTable = $('#example').dataTable( {
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": true,
		"bAutoWidth": false,
		"sPaginationType": "bootstrap",
		"aoColumnDefs": [	//Initialse DataTables, with no sorting on the 'checkbox' column
            { "bSortable": false, "aTargets": [ 0 ] }
        ],
        //"aaSorting": [[1, 'asc']]
    } );
   	/* Add event listener for opening and closing details
     * Note that the indicator for showing which row is open is not controlled by DataTables,
     * rather it is done here
     */

    $('.view-btn, .hide-btn').click(function () {
         

         if ($(this).attr("class") == "view-btn") {
            $(this).removeClass("view-btn");
            $(this).addClass("hide-btn");
            
        }
        else{
            $(this).removeClass("hide-btn");
            $(this).addClass("view-btn");
        }

        var ele = $(this).attr('id');
        var nTr = $(this).parents('tr')[0];
        if ( oTable.fnIsOpen(nTr) )
        {
            /* This row is already open - close it */
            this.src = "../examples_support/details_open.png";
            oTable.fnClose( nTr );
        }
        else
        {
            /* Open this row */
            this.src = "../examples_support/details_close.png";
            oTable.fnOpen( nTr, fnFormatDetails(oTable, nTr,ele), 'details' );
        }
    } );
     
} );

$.fn.dataTableExt.oStdClasses["sFilter"] = "Dfilter";
$('#example_wrapper').css("margin-top","50px");
</script>
<script>

$(document).ready(function() {
     $('#chkAll').click (function () {
          $(':checkbox[name=deleteall]').prop('checked', this.checked);
        });
});  

//load create category page in popup on "Create Category" button click
$('#btnCreate').click(function(){
    $('#myModalLabel').html("Add New Category");
    $.ajax({
            type: "POST",
            url: base_url+'index.php?controller=category&function=createCategory', //the script to call to get data          
            data: "", //you can insert url argumnets here to pass to api.php for example "id=5&parent=6"
            dataType: 'html',
            
            success: function(data) {
                $('#modal-body').html(data);
            }, 
        });						   
});

//load edit category page in popup on "edit Category" button click
$('.edit-btn').click(function(){
    var id=$(this).attr('id');
    //alert(id);
    $('#myModalLabel').html("Edit Category");
    $.ajax({
            type: "POST",
            url: base_url+'index.php?controller=category&function=editCatName', //the script to call to get data          
            data: {categoryid:id}, //you can insert url argumnets here to pass to api.php for example "id=5&parent=6"
            dataType: 'html',
            
            success: function(data) {
                $('#modal-body').html(data);
            }, 
        });                        
});

//load delete category page in popup on "delete icon"
$('.delete-btn').click(function(){
    var id=$(this).attr('id');
    $('#myModalLabel').html("Delete Category");
    $.ajax({
            type: "POST",
             data: {categoryid:id}, //you can insert url argumnets here to pass to api.php for example "id=5&parent=6"
            url: base_url+'index.php?controller=category&function=delCatName', //the script to call to get data          
           
            dataType: 'html',
            
            success: function(data) {
                $('#modal-body').html(data);
                
                
            }, 
        });                        
});

$(document).on('click', '#btnDelCat', function() {
    var id=$(".form-control").attr('id');
   
    $.ajax({
                
                type: "POST",
                data: {categoryid:id},

                url: base_url +'index.php?controller=category&function=delCategory', //the script to call to get data          

                //data: "", //you can insert url argumnets here to pass to api.php for example "id=5&parent=6"
                dataType: 'html',
               
                
                beforeSend: function() {
    
                },
                success: function(response) {
                        if(response == 2) {
                            $('#errDeleteCat').html("First delete questions related to this category").css("color","red");
                                //window.location.replace(base_url + 'index.php?controller=dashboard&function=category&status=1');
                        } else if(response == 1) {
                       document.location.reload(true);
                   }
               
                },
                complete: function() {
    
                },
                error: function() {
    
                }
            });
});

$(document).on('click', '#btnEditCat', function() {                                                                                  
    var id=$(".form-control").attr('id');
    var name=$("input[name='txtCatEdit']").val();
   //alert(name);
    $.ajax({
                
                type: "POST",
                data: {categoryid:id,categoryname:name},

                url: base_url +'index.php?controller=category&function=editCategory', //the script to call to get data          

                //data: "", //you can insert url argumnets here to pass to api.php for example "id=5&parent=6"
                dataType: 'json',
               
                
                beforeSend: function() {
    
                },
                success: function(response) {
                    if(response.regFlag == 1 ) {
                            $('#errCatEdit').html("Category name cannot be blank").css("color","red");
                                
                        } else if(response.regFlag == 2 ){
                            $('#errCatEdit').html("Invalid Category name").css("color","red");

                        } else {
                            $('#errCatEdit').html("");
                            if(response.val== 1 ) {
                                document.location.reload(true);
                            }
                        }
               
                },
                complete: function() {
    
                },
                error: function() {
    
                }
            });
});



$("#delMulCat").click(function(){
    var id=$('input[name="deleteall"]:checked');
    
        $.ajax({
                
                type: "POST",
                data: "",

                url: base_url +'index.php?controller=category&function=deleteMulCatName', //the script to call to get data          

                //data: "", //you can insert url argumnets here to pass to api.php for example "id=5&parent=6"
                dataType: 'html',
               
                
                beforeSend: function() {
    
                },
                success: function(response) {
                    $('#myModalLabel').html("Delete Category");
                    $('#modal-body').html(response);
                      
               
                },
                complete: function() {
    
                },
                error: function() {
                    
                }
            });
   
    
});


function addClassCurrent(element)
{
	var el="#ico"+element;
	$(el).addClass("current");
}

function removeClassCurrent(element)
{
	var el="#ico"+element;
	$(el).removeClass("current");
}
$('#footer').css("padding-top","30px");
</script>
