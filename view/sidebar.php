<?php
$totalTest =0;
$totalCategory =0;
$totalQuestion =0;
$totalCertificate =0;
$totalUsers =0;
$totalResult =0;
$link = $arrData['link'];
$arrData = $arrData['data'];
if(isset($arrData['totalCategory']) && !empty($arrData['totalCategory'])) {
    $totalCategory = $arrData['totalCategory'];
}
if(isset($arrData['totalQuestion']) && !empty($arrData['totalQuestion'])) {
    $totalQuestion = $arrData['totalQuestion'];
}
if(isset($arrData['totalTest']) && !empty($arrData['totalTest'])) {
    $totalTest = $arrData['totalTest'];
}
if(isset($arrData['totalUsers']) && !empty($arrData['totalUsers'])) {
    $totalUsers = $arrData['totalUsers'];
}

?>
<ul id="nav-menu">
            	<a href="index.php?controller=dashboard&function=dashboard" id="ancrDash" onMouseOver="addClassCurrent('Dash');" onMouseOut="removeClassCurrent('Dash');">
                	<span id="icoDash" class="icon glyphicon glyphicon-inbox <?php if($link == "dashboard" ) { echo "page-current"; } ?>"></span>
                	<li id="lnkDash" class="<?php if($link == "dashboard" ) { echo "link-current"; } ?>">DASHBOARD</li>
                </a>
                <a href="index.php?controller=dashboard&function=category" id="ancrCategory" 
                onMouseOver="addClassCurrent('Category');" onMouseOut="removeClassCurrent('Category');">
                	<span id="icoCategory" class="icon glyphicon glyphicon-th <?php if($link == "category" ) { echo "page-current"; } ?>"></span>


                    <li id="lnkCategory" class="<?php if($link == "category" ) { echo "link-current"; } ?>">CATEGORIES<span class="badge badge-style blue-background"><?php echo $totalCategory?></span></li>

                </a>
                <a href="index.php?controller=dashboard&function=question" id="ancrQuestions" onMouseOver="addClassCurrent('Questions');" 
                	onMouseOut="removeClassCurrent('Questions');">
                	<span id="icoQuestions" class="icon glyphicon glyphicon-list-alt <?php if($link == "question" ) { echo "page-current"; } ?>"></span>


                	<li id="lnkQuestions" class="<?php if($link == "question" ) { echo "link-current"; } ?>">QUESTIONS<span class="badge badge-style blue-background"><?php echo $totalQuestion?></span></li>

                </a>
                <a href="index.php?controller=dashboard&function=test" id="ancrTests" onMouseOver="addClassCurrent('Tests');" 
                	onMouseOut="removeClassCurrent('Tests');">
                	<span id="icoTests" class="icon glyphicon glyphicon-file <?php if($link == "test" ) { echo "page-current"; } ?>"></span>


                    <li id="lnkTests" class="<?php if($link == "test" ) { echo "link-current"; } ?>">TESTS<span class="badge badge-style blue-background"><?php echo $totalTest?></span></li>

                 </a>
                <a href="index.php?controller=dashboard&function=certificate" id="ancrCertificates" onMouseOver="addClassCurrent('Certificates');" 
                	onMouseOut="removeClassCurrent('Certificates');">
                	<span id="icoCertificates" class="icon glyphicon glyphicon-certificate <?php if($link == "certificate" ) { echo "page-current"; } ?>"></span>
                    <li id="lnkCertificates" class="<?php if($link == "certificate" ) { echo "link-current"; } ?>">CERTIFICATES<span class="badge badge-style"><?php echo $totalCertificate ?></span></li>
                 </a>
                <a href="index.php?controller=dashboard&function=result" id="ancrResulsts" onMouseOver="addClassCurrent('Results');" 
                	onMouseOut="removeClassCurrent('Results');">
                	<span id="icoResults" class="icon glyphicon glyphicon-check <?php if($link == "result" ) { echo "page-current"; } ?>"></span>
                	<li id="lnkResults" class="<?php if($link == "result" ) { echo "link-current"; } ?>">RESULTS<span class="badge badge-style"><?php echo $totalResult ?></span></li>
                </a>
                <a href="index.php?controller=dashboard&function=profile" id="ancrUsers" onMouseOver="addClassCurrent('Users');" 
                	onMouseOut="removeClassCurrent('Users');">
                	<span id="icoUsers" class="icon glyphicon glyphicon-user <?php if($link == "users" ) { echo "page-current"; } ?>"></span>
                   

                    <li id="lnkUsers" class="<?php if($link == "users" ) { echo "link-current"; } ?>">USERS<span class="badge badge-style"><?php echo $totalUsers ?></span></li>

                </a>
            </ul><!-- end of dash-menu -->
            <script type="text/javascript" src="<?php echo JS_PATH;?>jquery-1.10.2.min.js"></script>
            <script>

            function removePageCurrent()
            {
                $('#icoDash').removeClass("page-current");
                $('#icoCategory').removeClass("page-current");
                $('#icoQuestions').removeClass("page-current");
                $('#icoTests').removeClass("page-current");
                $('#icoResults').removeClass("page-current");
                $('#icoCertificates').removeClass("page-current");
                $('#icoUsers').removeClass("page-current");

                $('#lnkDash').removeClass("link-current");
                $('#lnkCategory').removeClass("link-current");
                $('#lnkQuestions').removeClass("link-current");
                $('#lnkTests').removeClass("link-current");
                $('#lnkResults').removeClass("link-current");
                $('#lnkCertificates').removeClass("link-current");
                $('#lnkUsers').removeClass("link-current");
            }
            $('#ancrDash').click(function()
            {
                removePageCurrent();
                $('#icoDash').addClass("page-current");
                $('#lnkDash').addClass("link-current");
            });
            
            $('#ancrCategory').click(function()
            {
                removePageCurrent();
                $('#icoCategory').addClass("page-current");
                $('#lnkCategory').addClass("link-current");
            });

            $('#ancrQuestions').click(function()
            {
                removePageCurrent();
                $('#icoQuestions').addClass("page-current");
                $('#lnkQuestions').addClass("link-current");
            });

            $('#ancrTests').click(function()
            {
                removePageCurrent();
                $('#icoTests').addClass("page-current");
                $('#lnkTests').addClass("link-current");
            });

            $('#ancrCertificates').click(function()
            {
                removePageCurrent();
                $('#icoCertificates').addClass("page-current");
                $('#lnkCertificates').addClass("link-current");
            });

            $('#ancrResulsts').click(function()
            {
                removePageCurrent();
                $('#icoResults').addClass("page-current");
                $('#lnkResults').addClass("link-current");
            });

            $('#ancrUsers').click(function()
            {
                removePageCurrent();
                $('#icoUsers').addClass("page-current");
                $('#lnkUsers').addClass("link-current");
            });
            function addPageCurrent(element)
            {
                var el="#ico"+element;
                $(el).addClass("page-current");
            }
    
            function addClassCurrent(element)
            {
                var el="#ico"+element;
                $(el).addClass("current");
            }

            function removeClassCurrent(element)
            {
                var el="#ico"+element;
                $(el).removeClass("current");
            }
            $('#footer').css("padding-top","30px");
            </script>

            