<?php
$testName = "Test Name";
$time = "20 Minutes";
$passMarks ="50";
 if(isset($arrData) && !empty($arrData)) {
    $testName = $arrData[0];
    $totalQuestion = $arrData[1];
    $score = $arrData[2][0]['score']; 
    $percentage = round(($score/$totalQuestion)*100);
 }

?>
  <div id="content">

   <div id="main-container">
      
      <fieldset class="fieldbox"><legend class="legendtext">Result </legend>
        <table id="resTable"cellpadding="10" class="fontapply">
              <tr>
                <td width="250px">Title</td>
                <td><?php echo $testName?></td>
                </tr>
                <tr>
                <td width="250px">Score</td>
                <td><?php echo 'You have Scored '.$score."/".$totalQuestion?></td>
                </tr>
                <tr>
                <td width="250px">percentage</td>
                <td><?php echo $percentage."%"?></td>
                </tr>
                <tr>
                <td width="250px">Duration</td>
                <td>00:02:59</td>
                </tr>
                <tr>
                <td width="250px">Date Started</td>
                <td>04/25/2014, 05:10:12pm</td>
                </tr>
                <tr>
                <td width="250px">Date Finished</td>
                <td>04/25/2014, 05:13:11pm</td>
                </tr>
                </table>
        <br>

        <a href="javascript:window.close();" class="btn fontapply float-right">Exit</a>

      </fieldset> <!-- end of fieldset -->
   </div> <!-- end of main container -->
  </div> <!-- end of content -->
   <?php
require_once("student_footer.php")
?>
 </div> <!-- end of wrapper -->
<script type="text/javascript">
$('#btnStart').click(function(){
   $.ajax({
            type: "POST",
            url: "<?php echo SITE_PATH; ?>index.php?controller=quiz&function=startTest",     
            data: '',
            dataType: 'html',            
            success: function(data) {
                $('#content').replaceWith(data);
            }, 
        });       
});


</script>

</body>
</html>