
</div><!-- end of dash-right-content-->
  <div id="dash-right-content">
    <div id="content-border" class="blue-background"></div>
      <div id="content-header">
         	<div id="text">Question Bank for the Test</div>
            <div id="heading"><span class="icon glyphicon glyphicon-th margin-right5"></span><span>QUESTIONS</span></div>
      </div> <!-- end of content header-->
    <div id="header-arrow" ></div>
            
    <div id="content">
      	<div class="row-mid">
        <div id="content-head">
                          <div class="float-left" id="content-head-left"></div>
                          <!-- end of content-head-left -->
                          <div class="float-right" id="content-head-right">
                              <a class="btn font-apply inlineBlock" href="index.php?controller=dashboard&function=question">Back to
                                  Question Bank</a>
  
                          </div>
                          <!-- end of content-head-right -->
                      </div>
                      <span class="fontapply">Question Type: </span> <select
                          name="q_type" id="q_type" class="form-control width150">
                          <option value="mul" selected>Multiple Choice</option>
                          <option value="tf">True/False</option>
                      </select> 

                     <span class="fontapply margin-left20">Category <span class="red">*</span>:</span>
                        <select class="form-control width150" id="categoryselect" class="lstbox" required>
                            <option value="">--Select Category--</option>
                            <?php foreach($arrData as $x)
                              {
                             ?>
                            <option value="<?php echo $x['id']; ?>"><?php echo $x['categoryName']; ?></option>
                            <?php } ?>              
                        </select>
                      


                      <p></p>


                        <fieldset class="fontapply question-field">
                         <div id="divMCQ">
                              <form id="frmMCQ" action="index.php?controller=question&function=addQuestion" method="POST">
        
                                  <h3>Question</h3>
                                  <textarea id="mtxtQuestion" name="txtQuestion" class="question-txtarea" focus ></textarea>
  
                                <h3>Answers</h3>
                                <ol class="olAnswers">
                                  <li>
                                  <p>
                                   <input type="checkbox" name="answer[]" value='0'> Mark Correct
                                  </p>
                                  <textarea id="opt1" class="answer-txtarea" name="opt[]" ></textarea>
                                    </li>
                                  <li>
                                  <p>
                                    <input type="checkbox" name="answer[]" value='1'> Mark Correct
                                  </p>
                                  <textarea id="opt2" class="answer-txtarea" name="opt[]" ></textarea>
                                 </li>
                                 <li>
                                  <p>
                                    <input type="checkbox" name="answer[]" value='2'> Mark Correct
                                  </p>
                                  <textarea id="opt3" class="answer-txtarea" name="opt[]" ></textarea>
                                 </li>
                                 <li>
                                  <p>
                                    <input type="checkbox" name="answer[]" value='3'> Mark Correct
                                  </p>
                                  <textarea id="opt4" class="answer-txtarea" name="opt[]" ></textarea>
                                 </li>

                                </ol>                              
                                <input type="hidden" name="type" value="0">
                                <input id="category" type="hidden" name="category">
                                <span id="ptsText">Pts</span><input type="text" value="1" id="txtPoints" name="txtPoints" />
                                <input id="sbmtSaveQuestion" type="submit" value="Save" class="btn">
                            
                                  </span>
                              </form>
                              <a id="ancrAddOpt"> <div class="add-btn"><span>Add More Options</span></div></a>    
                        </div>
                          <!-- end of mcq-div -->

                          <div id="divTF">
                              <form id="frmTF" action="index.php?controller=question&function=addQuestion" method="POST">
                                  <h3>Question</h3>
                                  <textarea name="txtQuestion" id="txtQuestion" class="question-txtarea" focus ></textarea>
  
                                <h3>Answers</h3>
                                <ol class="olAnswersTF">
                                  <li>
                                  <p>
                                   <input type="checkbox" name="answer[]" value='0'> Mark Correct
                                  </p>
                                  <textarea id="opt1" class="answer-txtarea" name="opt[]" >true</textarea>
                                    </li>
                                  <li>
                                  <p>
                                    <input type="checkbox"  name="answer[]" value='1'> Mark Correct
                                  </p>
                                  <textarea id="opt2" class="answer-txtarea" name="opt[]" >false</textarea>
                                 </li>

                                </ol>                              
                                <input type="hidden" name="type" value="1">
                                <input id="category1" type="hidden" name="category">
                                <span id="ptsText">Pts</span><input type="text" id="txtPoints" value="1" name="txtPoints" />
                                <input id="sbmtSaveQuestion" type="submit" value="Save" class="btn">
                            
                                  </span>
                              </form>
                       </div>
                       <span id="allError" class="text-danger"></span>
                          <!-- end of mcq-div -->
                       
                      </fieldset>
                

                </div>
                   
            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Status</h4>
                  </div>
                  <div id="modal-body" class="modal-body">
                    <h3 class="fontapply h3Status">Question Added</h3>
                    <input type="button" data-dismiss="modal" aria-hidden="true" class="btn btnStatus" value="OK"> 
                  </div>
                </div>
              </div>
            </div>
                </div>	<!-- end of content -->
     	   <?php require_once("footer.php"); ?>	<!-- include footer and scripts -->
        </div>	<!-- end of dash-right-content-->
    </div> <!-- end of dash-content -->
    

    <script type="text/javascript">

    //function to check question add status
    <?php if(isset($_GET['success']) && ($_GET['success'] == 1)) {
      echo "$('#myModal').modal('show');";
    } ?>

//function to set category hidden type values from category dropdownlist
$('#categoryselect').change(function() {
  var val=$("#categoryselect option:selected").val();
        $('#category, #category1').attr("value",val);
});

//function to display mcq or tf forms based on dropdown selection
$('#q_type').change(function() {
  var typeList = $('#q_type option:selected').text();

  if(typeList == "True/False") {
    $('#divMCQ').css("display","none");
    $('#divTF').css("display","block");
    $("#frmMCQ")[0].reset();
    $('#allError').html("");
  }
  else if(typeList == "Multiple Choice") {
                $('#divTF').css("display","none");
                $('#divMCQ').css("display","block");
                $("#frmTF ")[0].reset();
                $('#allError').html("");
                }
});


//function to validate submission
$('#frmMCQ').submit(function(e){

  var ques=$('#mtxtQuestion').val();
  var cat = $('#categoryselect :selected').val();
  if(!cat)
  {
    e.preventDefault();
    $('#categoryselect').focus();
    $('#allError').html("* Please select category");
  }
  
  if(!ques) {
    e.preventDefault();
  $('#mtxtQuestion').focus();
   $('#allError').html("* Please enter question");
  }
  else
  {
    if($('#frmMCQ #opt1').val() == "" || $('#frmMCQ #opt2').val() =="" || $('#frmMCQ #opt3').val() =="" || $('#frmMCQ #opt4').val() =="")
      {
        e.preventDefault();
        $('#allError').html("* enter all options"); 
      } 
      else
      {
      var id=$('input[name="answer[]"]:checked').map(function() {
       return $(this).attr('value');
                }).get();
      
      if(id.length == 0)
      {
        
        e.preventDefault();
        $('#allError').html("* mark atleast one option"); 
      }
    }
  }

   

});

$('#frmTF').submit(function(e){

  var ques=$('#frmTF #txtQuestion').val();
  var cat = $('#categoryselect :selected').val();
  if(!cat)
  {
    e.preventDefault();
    $('#categoryselect').focus();
    $('#allError').html("* Please select category");
  }
  
  if(!ques) {
    e.preventDefault();
  $('#frmTF #txtQuestion').focus();
   $('#allError').html("* Please enter question");
  }
  else
  {
    if($('#frmTF #opt1').val() == "" || $('#frmTF #opt2').val() =="")
      {
        e.preventDefault();
        $('#allError').html("* enter all options"); 
      } 
      else
      {
      var id=$('#frmTF input[name="answer[]"]:checked').map(function() {
       return $(this).attr('value');
                }).get();
      
      if(id.length == 0)
      {
        
        e.preventDefault();
        $('#allError').html("* mark atleast one option"); 
      }
    }
  }

   

});

$(document).ready(function () {
        var counter = 4;
        $('#ancrAddOpt').click(function(){

            $('<li/>').html(
                $('<p>').html( $('<input />', { type: 'checkbox', name: 'answer[]', value: counter })).append(" Mark Correct"))
                .append( $('<textarea>').attr({'name':'opt[]'}).addClass("answer-txtarea") )
                .appendTo( '.olAnswers' );      
                counter++;
            });

  
});

 
$(document).ready(function() {


    $('#categoryselect').change( function() { 
            oTable.fnFilter( $(this).val() ); 
       });


    $('.view-btn, .hide-btn').click(function () {
         

         if ($(this).attr("class") == "view-btn") {
            $(this).removeClass("view-btn");
            $(this).addClass("hide-btn");
            
        }
        else{
            $(this).removeClass("hide-btn");
            $(this).addClass("view-btn");
        }


        var nTr = $(this).parents('tr')[0];
        if ( oTable.fnIsOpen(nTr) )
        {
            /* This row is already open - close it */
            this.src = "../examples_support/details_open.png";
            oTable.fnClose( nTr );
        }
        else
        {
            /* Open this row */
            this.src = "../examples_support/details_close.png";
            oTable.fnOpen( nTr, fnFormatDetails(oTable, nTr), 'details' );
        }
    } );
	 
} );

$.fn.dataTableExt.oStdClasses["sFilter"] = "Dfilter";
$('#example_wrapper').css("margin-top","50px");
</script>
<script>

$(document).ready(function() {
     $('#chkAll').click (function () {
          $(':checkbox[name=deleteall]').prop('checked', this.checked);
        });
});  


//load delete question page in popup on "delete icon"
$('.delete-btn, .deletebtn').click(function(){
    $('.modal-dialog').css("width","800px");
    $('#myModalLabel').html("Delete Question");
    $.ajax({
            type: "POST",
            url: base_url+'index.php?controller=question&function=questionDelete', //the script to call to get data          
            data: "", //you can insert url argumnets here to pass to api.php for example "id=5&parent=6"
            dataType: 'html',
            
            success: function(data) {
                $('#modal-body').html(data);
            }, 
        });                        
});

//load view question in popup on question name
$('.ancrQuestion').click(function(){
    $('.modal-dialog').css("width","800px");
    $('#myModalLabel').html("Question Details");
    $.ajax({
            type: "POST",
            url: base_url+'index.php?controller=question&function=questionView', //the script to call to get data          
            data: "", //you can insert url argumnets here to pass to api.php for example "id=5&parent=6"
            dataType: 'html',
            
            success: function(data) {
                $('#modal-body').html(data);
            }, 
        });                        
});

function addClassCurrent(element)
{
	var el="#ico"+element;
	$(el).addClass("current");
}

function removeClassCurrent(element)
{
	var el="#ico"+element;
	$(el).removeClass("current");
}
$('#footer').css("padding-top","30px");
</script>
