
<div id="divCreate">

	<?php $removeCharacter = array('\r\n'); ?>

	<p class="lblQuestion">Q. <?php echo str_replace($removeCharacter, "", htmlspecialchars(stripslashes($arrData['description']),ENT_QUOTES, 'UTF-8'));?></p>
	<?php /*echo "<pre>"; print_r($arrData); 
	
	echo "options";
	print_r($options);*/
	?>
	<ol class="lblQuestionOpt">
		<?php
	$options=explode(',', $arrData['options']);
	$len=count($options);
	for($i=0;$i<$len;$i++)
	{
		echo "<li";
		if($options[$i] == 1){
			echo " class='blue'>";
		}
		else echo ">";
		echo str_replace($removeCharacter, "", htmlspecialchars(stripslashes($options[$i+1]),ENT_QUOTES, 'UTF-8'))."</li>";
		$i++;
	}
	?>

	</ol>
	<p></p>
	<input type="hidden" class="quesid" id="<?php echo $arrData['id'];?>">
	<div id="errCat"></div>
	<input type="button" value="Confirm" class="btn margin-left50 margin-top10" id="btnDelQues"/>
	<input type="button" value="Cancel" class="btn margin-left20 margin-top10" aria-hidden="true" data-dismiss="modal"/>
	<p></p>

</div>
