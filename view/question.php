<?php
$questionData  = $arrData[0];
$categoryData  = $arrData[1];

?>
</div><!-- end of dash-right-content-->
  <div id="dash-right-content">
    <div id="content-border" class="blue-background"></div>
      <div id="content-header">
         	<div id="text">Question Bank for the Test</div>
            <div id="heading"><span class="icon glyphicon glyphicon-th margin-right5"></span><span>QUESTIONS</span></div>
      </div> <!-- end of content header-->
    <div id="header-arrow" ></div>
            
    <div id="content">
      	<div class="row-midlarge">
            <div id="table-content">
                <div id="d-table-menu">
                     <a id="btnAddques" href="index.php?controller=dashboard&function=questionAdd" class="btn addques-button fontapply">+ ADD QUESTIONS</a>
                     <a id="btnAddques" href="index.php?controller=dashboard&function=questionImport" class="btn addques-button fontapply">IMPORT QUESTIONS</a>  

            <a class="deletebtn" data-toggle="modal" data-target="#myModal" title="Delete Selected Test" href="#"></a>
            <select id="categoryselect" class="form-control quesDisplayList" name="categoryselect">
            <option value="">Display by Category</option>
            <?php foreach($categoryData as $x)
                              {
                             ?>
                            <option value="<?php echo $x['categoryName']; ?>"><?php echo $x['categoryName']; ?></option>
                            <?php } ?> 
     </select>

        </div>
            
     <table cellpadding="0" cellspacing="0" border="0" class="LSQuestable table-bordered" id="example">
      <thead>
		<tr>
		    <th><input type="checkbox" id="chkAll" /></th>
            <th>No.</th>
            <th>Question</th>
            <th>Pts</th>
            <th>Category</th>
            <th>Actions</th>
		</tr>
	   </thead>
	   
       <tbody>
        <?php 
            $rowcount = 0;
            foreach($questionData as $x){
                $rowcount++;
                $removeCharacter = array('\r\n','\r','\n','\\');

                $quesDesc = $x['description'];
                $len = strlen($quesDesc);
                if($len > 45) {
                  $quesDesc = substr($quesDesc, 0,60);
                  $quesDesc .="...";
                }
                ?>
		<tr>
			<td><input type="checkbox" name="deleteall" value="<?php echo $x['id'];?>"></td>
            <td><?php echo $rowcount ?></td>
            <td><a class="ancrQuestion" id='<?php echo $x['id']; ?>' href="#" data-toggle="modal" data-target="#myModal"><?php echo str_replace($removeCharacter, " ", htmlspecialchars($quesDesc,ENT_QUOTES, 'UTF-8'));?></a></td>
            
            <td><?php echo $x['points'];echo "pt"; ?></td>
            <td><?php echo $x['name'] ?></td>
            <td>
            <a href="#" data-toggle="modal" data-target="#myModal"><div class="delete-btn" id='<?php echo $x['id']; ?>' ></div></a>
            <a href="#"><div class="edit-btn" id="<?php echo $x['id'] ?>"></div></a>
            <a href="#"><div class="view-btn" id="<?php echo $x['id'] ?>"></div></a>

            <div class="div-details" id="details<?php echo $x['id'] ?>">
                <div class="Quesdetails-left">Type :<strong> <?php echo ($x['type']==0?"Multiple":"True/False") ?></strong><br>
                                        Created By : <strong><?php echo $x['first_name']; ?></strong><br>
                                        Created On : <strong><?php $date = date_create($x['created_on']); echo date_format($date,'d-M-Y'); ?></strong><br>
                </div>
                <div class="Quesdetails-right margin-top0 ">Status :<strong> Active</strong><br>
                                                Updated By : <strong><?php echo $x['first_name_updatedby']; ?></strong><br>
                                                Updated On : <strong><?php $date = date_create($x['updated_on']); echo date_format($date,'d-M-Y'); ?></strong><br>
                </div>
            </div> <!-- end of div-details -->
            </td>

            
		</tr>
              <?php  }

       ?>  
        
	</tbody>
</table>
</div>	<!-- end of table content-->

                </div>


            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                  </div>
                  <div id="modal-body" class="modal-body">
                    ...
                  </div>
                </div>
              </div>
            </div>
                </div>	<!-- end of content -->
     	   <?php require_once("footer.php"); ?>	<!-- include footer and scripts -->
        </div>	<!-- end of dash-right-content-->
    </div> <!-- end of dash-content -->
    
    <script type="text/javascript">

/* Formating function for row details */
function fnFormatDetails ( oTable, nTr, ele )
{
    var elemnt = "#details"+ele;
    var aData = oTable.fnGetData( nTr );
    var sOut    =  $(elemnt).html();
    return sOut;
}


	
	/* API method to get paging information */
$.fn.dataTableExt.oApi.fnPagingInfo = function ( oSettings )
{
    return {
        "iStart":         oSettings._iDisplayStart,
        "iEnd":           oSettings.fnDisplayEnd(),
        "iLength":        oSettings._iDisplayLength,
        "iTotal":         oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage":          oSettings._iDisplayLength === -1 ?
            0 : Math.ceil( oSettings._iDisplayStart / oSettings._iDisplayLength ),
        "iTotalPages":    oSettings._iDisplayLength === -1 ?
            0 : Math.ceil( oSettings.fnRecordsDisplay() / oSettings._iDisplayLength )
    };
}
 
/* Bootstrap style pagination control */
$.extend( $.fn.dataTableExt.oPagination, {
    "bootstrap": {
        "fnInit": function( oSettings, nPaging, fnDraw ) {
            var oLang = oSettings.oLanguage.oPaginate;
            var fnClickHandler = function ( e ) {
                e.preventDefault();
                if ( oSettings.oApi._fnPageChange(oSettings, e.data.action) ) {
                    fnDraw( oSettings );
                }
            };
 
            $(nPaging).addClass('pagination').append(
                '<ul>'+
                    '<li class="prev disabled"><a href="#">&larr; '+oLang.sPrevious+'</a></li>'+
                    '<li class="next disabled"><a href="#">'+oLang.sNext+' &rarr; </a></li>'+
                '</ul>'
            );
            var els = $('a', nPaging);
            $(els[0]).bind( 'click.DT', { action: "previous" }, fnClickHandler );
            $(els[1]).bind( 'click.DT', { action: "next" }, fnClickHandler );
        },
 
        "fnUpdate": function ( oSettings, fnDraw ) {
            var iListLength = 5;
            var oPaging = oSettings.oInstance.fnPagingInfo();
            var an = oSettings.aanFeatures.p;
            var i, j, sClass, iStart, iEnd, iHalf=Math.floor(iListLength/2);
 
            if ( oPaging.iTotalPages < iListLength) {
                iStart = 1;
                iEnd = oPaging.iTotalPages;
            }
            else if ( oPaging.iPage <= iHalf ) {
                iStart = 1;
                iEnd = iListLength;
            } else if ( oPaging.iPage >= (oPaging.iTotalPages-iHalf) ) {
                iStart = oPaging.iTotalPages - iListLength + 1;
                iEnd = oPaging.iTotalPages;
            } else {
                iStart = oPaging.iPage - iHalf + 1;
                iEnd = iStart + iListLength - 1;
            }
 
            for ( i=0, iLen=an.length ; i<iLen ; i++ ) {
                // Remove the middle elements
                $('li:gt(0)', an[i]).filter(':not(:last)').remove();
 
                // Add the new list items and their event handlers
                for ( j=iStart ; j<=iEnd ; j++ ) {
                    sClass = (j==oPaging.iPage+1) ? 'class="active"' : '';
                    $('<li '+sClass+'><a href="#">'+j+'</a></li>')
                        .insertBefore( $('li:last', an[i])[0] )
                        .bind('click', function (e) {
                            e.preventDefault();
                            oSettings._iDisplayStart = (parseInt($('a', this).text(),10)-1) * oPaging.iLength;
                            fnDraw( oSettings );
                        } );
                }
 
                // Add / remove disabled classes from the static elements
                if ( oPaging.iPage === 0 ) {
                    $('li:first', an[i]).addClass('disabled');
                } else {
                    $('li:first', an[i]).removeClass('disabled');
                }
 
                if ( oPaging.iPage === oPaging.iTotalPages-1 || oPaging.iTotalPages === 0 ) {
                    $('li:last', an[i]).addClass('disabled');
                } else {
                    $('li:last', an[i]).removeClass('disabled');
                }
            }
        }
    }
} );

$(document).ready(function() {

	var oTable = $('#example').dataTable( {
		"bLengthChange": false,
		"bFilter": true,
		"bInfo": true,
		"bAutoWidth": false,
		"sPaginationType": "bootstrap",
		"aoColumnDefs": [	//Initialse DataTables, with no sorting on the 'checkbox' column
            { "bSortable": false, "aTargets": [ 0 ], "aTargets": [ 4 ] }
        ],
        "aaSorting": [[0, 'asc']]
		} );

    $('#categoryselect').change( function() { 
            oTable.fnFilter( $(this).val() ); 
       });


   	/* Add event listener for opening and closing details
     * Note that the indicator for showing which row is open is not controlled by DataTables,
     * rather it is done here
     */



    $('.view-btn, .hide-btn').click(function () {

         if ($(this).attr("class") == "view-btn") {
            $(this).removeClass("view-btn");
            $(this).addClass("hide-btn");
            
        }
        else{
            $(this).removeClass("hide-btn");
            $(this).addClass("view-btn");
        }

        var ele = $(this).attr('id');
        var nTr = $(this).parents('tr')[0];
        if ( oTable.fnIsOpen(nTr) )
        {
            /* This row is already open - close it */
            this.src = "../examples_support/details_open.png";
            oTable.fnClose( nTr );
        }
        else
        {
            /* Open this row */
            this.src = "../examples_support/details_close.png";
            oTable.fnOpen( nTr, fnFormatDetails(oTable, nTr,ele), 'details' );
        }
    });

    $(document).on('click','.edit-btn',function () {
         var id = $(this).attr('id');
         //alert(id);

         $(location).attr('href', base_url+'index.php?controller=dashboard&function=questionEdit&id='+id);
    });
	 
});

$.fn.dataTableExt.oStdClasses["sFilter"] = "Dfilter";
$('#example_wrapper').css("margin-top","50px");
</script>
<script>

$(document).ready(function() {
     $('#chkAll').click (function () {
          $(':checkbox[name=deleteall]').prop('checked', this.checked);
        });
});  

//load create question in popup on "ADD QUESTION" button click
$('#btnCreate').click(function(){
    $('#myModalLabel').html("Add New Category");
    $.ajax({
            type: "POST",
            url: base_url+'index.php?controller=category&function=createCategory', //the script to call to get data          
            data: "", //you can insert url argumnets here to pass to api.php for example "id=5&parent=6"
            dataType: 'html',
            
            success: function(data) {
                $('#modal-body').html(data);
            }, 
        });						   
});

//load delete question page in popup on "delete icon"
$('.delete-btn').click(function(){
    $('.modal-dialog').css("width","800px");
    $('#myModalLabel').html("Delete Question");

    var id = $(this).attr('id');

    $.ajax({
            type: "POST",
            url: base_url+'index.php?controller=question&function=questionDelete', //the script to call to get data          
            data: "id="+id, //you can insert url argumnets here to pass to api.php for example "id=5&parent=6"
            dataType: 'html',
            
            success: function(data) {
                $('#modal-body').html(data);
            }, 
        });                        
});

//load delete question page in popup on "delete icon"
$('.deletebtn').click(function(){
    $('.modal-dialog').css("width","500px");
    $.ajax({
            type: "POST",
            url: base_url+'index.php?controller=question&function=mulquestionDelete', //the script to call to get data          
            data: "", //you can insert url argumnets here to pass to api.php for example "id=5&parent=6"
            dataType: 'html',
            
            success: function(data) {
                $('#myModalLabel').html("Delete Multiple")
                $('#modal-body').html(data);
            }, 
        });                        
});

//load view question in popup on question name
$('.ancrQuestion').click(function(){
    $('.modal-dialog').css("width","800px");
    $('#myModalLabel').html("Question Details");
    
    var id = $(this).attr('id');

    $.ajax({
            type: "POST",
            url: base_url+'index.php?controller=question&function=viewFullQuestion', //the script to call to get data          
            data: "id="+id, //you can insert url argumnets here to pass to api.php for example "id=5&parent=6"
            dataType: 'html',
            
            success: function(data) {
                $('#modal-body').html(data);
            }, 
        });                        
});

//Confirm Delete button functionality
$(document).on('click', '#btnDelQues', function() {
    var id=$(".quesid").attr('id');
   
    $.ajax({
                
                type: "POST",
                data: {quesid:id},

                url: base_url +'index.php?controller=question&function=delQuestion', //the script to call to get data          

                //data: "", //you can insert url argumnets here to pass to api.php for example "id=5&parent=6"
                dataType: 'html',
               
                
                beforeSend: function() {
    
                },
                success: function(response) {
                        document.location.reload(true);
               
                },
                complete: function() {
    
                },
                error: function() {
    
                }
            });
});


function addClassCurrent(element)
{
	var el="#ico"+element;
	$(el).addClass("current");
}

function removeClassCurrent(element)
{
	var el="#ico"+element;
	$(el).removeClass("current");
}
$('#footer').css("padding-top","30px");
</script>
