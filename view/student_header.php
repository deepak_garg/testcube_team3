<?php
$name ="";
if(isset($arrData) && !empty($arrData)) {
	$name = $arrData;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<html>
	<head>

	<title>TestCube - Common Adaptive Test</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">	
	<link rel="stylesheet" href="<?php echo CSS_PATH;?>bootstrap.css" />
	<link rel="stylesheet" href="<?php echo CSS_PATH;?>jquery.countdownTimer.css" />
	<link rel="stylesheet" href="<?php echo CSS_PATH;?>student_layout.css" />
	<link rel="stylesheet" href="<?php echo CSS_PATH;?>student_style.css" />
	<script type="text/javascript" src="<?php echo JS_PATH;?>jquery-1.10.2.min.js"></script>
	
	</head>
	<body>
	
	 <div id="wrapper">
<div id="header">
	   <div id="header-logo">
	   <span id="logo-text"><strong>Test</strong>Cube</span>
	   </div> <!-- end of header-logo -->
	   <div id="header-menu">
	     <div id="nav">
	     <ul class="nav-menu">
	     	<li class="none nav-menu-links "><span class="username" ><?php echo $name ;?></span></li>
	     	<li class="none nav-menu-links"><a href="#" class="nav-menul"><?php echo $lang->headerAboutText; ?></a></li>
	     	<li class="none nav-menu-links"><a href="#" class="nav-menul"><?php echo $lang->headerServiceText; ?></a></li>
	     	<li class="none nav-menu-links"><a href="#" class="nav-menul"><?php echo $lang->headerHelpText; ?></a></li>
	     </ul>
	     
	      </div> <!-- end of nav -->
	   </div> <!-- end of menu -->
	  </div> <!-- end of header -->

