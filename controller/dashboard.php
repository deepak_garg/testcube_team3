<?php


require_once('appcontroller.php');

require_once(LIBRARY_ROOT.'clientdbconnection.php');

	class dashboardController extends AppController
	{
		private $userId;
		private $errPasswordFlag;
		private $oldPassword;
		private $newPassword;
		private $confirmPassword;
		private $clientId;
		private $errProfileSettingsFlag;
		private $profileFirstName;
		private $profileLastName;
		private $profileContact;
		private $profileLanguage;
		private $profileTheme;
		function __construct()
		{
			
			parent::__construct();
			$this->userId = $this->sessionObj->get('userId');
			$this->clientId = $this->sessionObj->get('clientId');
				
			new clientDbConnection($this->clientId);

			if(!isset($this->userId) || empty($this->userId)) {		
				// redirect to login page
					header('location:'.SITE_PATH.'index.php?controller=login&function=login');
			} 

		}


		// this function will load header and sidebar
		public function getMenu($link)
		{
			$userData = loadModel('dashboard','dashboard',$this->userId);
			//$this->sessionObj->set('lang',$userData['lang']);
			
			loadView('main_header.php',$userData);
			$countData = loadModel('data','countTotal');
			
			$arrData = array('link' => $link , 'data' => $countData);
			loadView('sidebar.php',$arrData);
			
		}

		// loads contents of dashboard (landing page)
		public function dashboard()
		{
			$this->getMenu('dashboard');
			$countData = loadModel('data','countTotal'); 
			$testData = loadModel('test','recentTest'); 
			$linkData = loadModel('test','recentLink',$this->clientId);
			$updatesData = loadModel('category','getUpdates');
			$arrData = array('countData' => $countData, 'testData' => $testData, 'linkData' => $linkData, 'updatesData'=>$updatesData);
			 // echo "<pre>";
			 // print_r($arrData);
			 // exit;
			loadView('dashboard.php',$arrData);
		}	


		// display the list of categories
		public function category()
		{
			$this->getMenu('category');
			$categoryList=loadModel('category','categoryList');
			
			loadView('category.php',$categoryList);
		}	

		// dsiplay the list of questions
		public function question()
		{
			$this->getMenu('question');
			$catList=loadModel('category','categoryList');
			$quesList=loadModel('question','questionList');
			$PassArr = array( $quesList , $catList);
			loadView('question.php',$PassArr);
		}

		// display the list of tests
		public function test()	
		{
			$this->getMenu('test');
			$testList=loadModel('test','showTestList');
			loadView('test.php',$testList);
		}
	
		// display the user settings page
		public function users()	
		{
			$this->getMenu('users');
			loadView('users.php');
		}
	

		// display the list of certificates generated
		public function certificate()	
		{
			$this->getMenu('certificate');
			loadView('certificates.php');
		}

		// display the list of certificates generated
		public function result()	
		{
			$this->getMenu('result');
			loadView('result.php');
		}

		// loads the details of a test 	
		public function testDetail()
		{

			$this->getMenu('test');

			if(isset($_GET['testid']) && !empty($_GET['testid'])) {
			 	$testId =  $_GET['testid'];

			 	//getting all questions assigned to this test
			 	$quesList=loadModel('test','retrieveQuesForTest',$testId);

			 	$testName = loadModel('test','retrieveTestName',$testId);

			 	$categoryList =  loadModel('category','retrieveCatListForTest');

			 	$questListTestName = array($testName,$quesList,$testId,$categoryList);
				loadView('test_details.php',$questListTestName);

			}
			
		}

		// loads available questions to add in a test
		public function testQuestionAdd()
		{
			$this->getMenu('test');
			if(isset($_GET['testid']) && !empty($_GET['testid'])) {

				$testId =  $_GET['testid'];
				$testName = loadModel('test','retrieveTestName',$testId);

				// getting all questions except those which are already assigned to this test
				$allQuestionsList = loadModel('test','retrieveAllQuestions',$testId); 

				$categoryList =  loadModel('category','retrieveCatListForTest');

				$allQuestionsListTestName = array($testName,$allQuestionsList,$testId,$categoryList);

				loadView('test_question_add.php',$allQuestionsListTestName);

			}
			
		}

		// allows to add a question
		public function questionAdd() {
			$this->getMenu('question');

			$queList=loadModel('category','categoryList');
			loadView('question_add.php',$queList);
		}

		// allows to import questions from excel sheet
		public function questionImport() {
			$this->getMenu('question');
			//$queList=loadModel('category','categoryList');
			loadView('question_import.php');
		}

		// allows to edit a question
		public function questionEdit(){
			$this->getMenu('question');
			$quesId = $_GET['id'];
			$quesList=loadModel('question','questionFullEditList',$quesId);
			$catList=loadModel('category','categoryList');
			$PassArr = array( $quesList , $catList);
			loadView('question_edit.php',$PassArr);
		}


		// display the user profile
		public function profile(){
			$this->getMenu('users');
			$profileDetails=loadModel('dashboard','getProfile',$this->userId);
			
			loadView('profile.php',$profileDetails);

		}

		// allows to change the password for current user
		public function profilePassword(){
				$this->errPasswordFlag = 0;	
				$status="";
				$return="";
				if(isset($_POST['oldPassword']) && !empty($_POST['oldPassword'])){
					if(isset($_POST['newPassword']) && !empty($_POST['newPassword'])){
						if(isset($_POST['confirmPassword']) && !empty($_POST['confirmPassword'])){

							$this->oldPassword = mysql_real_escape_string($_POST['oldPassword']);
							$oldPasswordRes = $this->validationObj->validatePassword($this->oldPassword);

								if($oldPasswordRes === true) {

									$this->newPassword = mysql_real_escape_string($_POST['newPassword']);
									$newPasswordRes = $this->validationObj->validatePassword($this->newPassword);

									if($newPasswordRes === true) {

										$this->confirmPassword = mysql_real_escape_string($_POST['confirmPassword']);
										$confirmPasswordRes = $this->validationObj->validatePassword($this->confirmPassword);

										if($confirmPasswordRes === true) {

											$data = array(
												'oldPassword' => $this->oldPassword,
												'newPassword' => $this->newPassword,
												'userId' => $this->userId

												);

											$status= loadmodel('profile','profileDetails',$data);

										} else {
											$this->errPasswordFlag = 6; //confrim password is invalid
										}

									} else {
										$this->errPasswordFlag = 5; //new password is invalid
									}

								} else {
									$this->errPasswordFlag = 4; //old password is invalid
								}

						} else {
							$this->errPasswordFlag = 3; //confirm password cannot be blank
						}

					} else {
						$this->errPasswordFlag = 2; // new password cannot be blank
					}					
				} else {
					$this->errPasswordFlag = 1; //old password cannot be blank
				}

				$return=array('passFlag' => $this->errPasswordFlag , 'val' => $status);

				//send the return value encoded in json datatype
				echo json_encode($return);

		}

		// displays the links generated for a test

			public function linkSettings(){
				$arrData="";
			
				if(isset($_GET['id']) && !empty($_GET['id'])) {

					$testId = $_GET['id'];

					$arrData = loadmodel('test','linkDetails',$testId);
					
					
				}
			$this->getMenu('test');
			loadView('links.php',$arrData);
		}
		
		

		// load the settings  for review
		public function linkReview(){
			$arrData="";
			
				if(isset($_GET['id']) && !empty($_GET['id'])) {

					$linkId = $_GET['id'];
					$host 	= $this->clientId;

					$arrData = loadmodel('test','getLinkDetail',$linkId);
					$status=loadModel('test','linkHistory',$linkId);
					
					$arrData['host'] = $host;
					$recentLinkDetail = array('linkDetails' => $arrData , 'recentLink' => $status);
					// echo "<pre>";
					// print_r($recentLinkDetail);
					// exit;
					
				}

		$this->getMenu('test');
	  
		loadView('link_review.php',$recentLinkDetail);

		}

		// allows to change the user profile
		public function profileSettings(){

			$this->errProfileSettingsFlag=0;
			$return="";
			$status="";

			if(isset($_POST['profileFirstName']) && !empty($_POST['profileFirstName'])){
				$this->profileFirstName = mysql_real_escape_string($_POST['profileFirstName']);
				$this->profileLastName = mysql_real_escape_string($_POST['profileLastName']);
				$this->profileContact = mysql_real_escape_string($_POST['profileContact']);

				$profileFirstNameRes = $this->validationObj->validateFirstName($this->profileFirstName);
				$profileLastNameRes = $this->validationObj->validateLastName($this->profileLastName);
				$profileContactRes = $this->validationObj->validateContact($this->profileContact);

				if($profileFirstNameRes === true) {
					if($profileLastNameRes === true){
						if($profileContactRes === 1){
							$this->profileContact = 0;
							$data = array(
									'firstName' => $this->profileFirstName,
									'lastName' => $this->profileLastName,
									'contact' => $this->profileContact,
									'userId' => $this->userId,
									'clientId' => $this->clientId);
						
							$status = loadModel('profile','profileSettings',$data);	
						} else if($profileContactRes === true){

							
							$data = array(
									'firstName' => $this->profileFirstName,
									'lastName' => $this->profileLastName,
									'contact' => $this->profileContact,
									'userId' => $this->userId,
									'clientId' => $this->clientId);
						
							$status = loadModel('profile','profileSettings',$data);	
						} else {
							$this->errProfileSettingsFlag = 4; //contact not valid
						}
							
					} else {
						$this->errProfileSettingsFlag = 3; // last name not valid
					}
					
				} else {
					$this->errProfileSettingsFlag = 2; //first name is not valid
				}
			} else {
				$this->errProfileSettingsFlag = 1;	//first name cannot be blank
			}
			$return=array('profileFlag' => $this->errProfileSettingsFlag , 'val' => $status);

			echo json_encode($return);


		}


		public function profileLanguage(){
				$return = "";
				$status = "";

			if(isset($_POST['profileTheme']) && !empty($_POST['profileTheme'])){
				$this->profileTheme = $_POST['profileTheme'];
			}

			if(isset($_POST['profileLanguage']) && !empty($_POST['profileLanguage'])){

				$this->profileLanguage = mysql_real_escape_string($_POST['profileLanguage']);

				$data = array(
						'profileTheme' => $this->profileTheme,
						'profileLanguage' => $this->profileLanguage,
						'userId' => $this->userId
					);
				$status = loadModel('profile','profileLanguage',$data);


			} else {
				// set default language as english
			}

			$return = array('val' => $status);
			echo json_encode($return);

		}

		


	}



