<?php 

ini_set("display_errors","1");

require_once('appcontroller.php');  
require_once(LIBRARY_ROOT.'resetpasssword_info.php');
require_once(LIBRARY_ROOT.'email_info_to_new_user.php');

class loginController extends Appcontroller
{
	
	private $userName;
	private $email;
	private $firstName;
	private $lastName;
	private $password;
	private $validateLoginFlag ;
	private $validateRegisterFlag ;


	function __construct() 
	{
		parent::__construct();
			$userId = $this->sessionObj->get('userId');
			if(isset($userId) && !empty($userId)){		
					header('location:'.SITE_PATH.'index.php?controller=dashboard&function=dashboard');
			} 
		$this->validateLoginFlag =0;
		$this->validateRegisterFlag =0;
	}
	
	// display login page
	public function login($arrData ="") 
	{
		loadView('header.php');
		loadView('login.php',$arrData);
		loadView('login-footer.php');
	}
	
	// password sent to the email so allow to login using password and username/email
	public function verifyClient()
	{
		header("location: ".SITE_PATH."index.php?controller=login&function=login&success=1");
	}

		// function is handeling the forget password situation
	public function forgetPasswordGenerate()
	{

		if(isset($_POST['emailForgetPassword']) && !empty($_POST['emailForgetPassword'])) {
		$this->email=mysql_real_escape_string($_POST['emailForgetPassword']);
		}
		$emailRes = $this->validationObj->validateEmail($this->email);
	
	if($emailRes) {
			$data = array (
					'email' => $this->email
					);
			 $return = loadModel('user','isAvaialableEmail',$data);
	
		if(!$return) {
			
				
				$generatedPasswd = loadModel ('user','resetPassword',$data);	
					

			if(isset($generatedPasswd) && !empty($generatedPasswd))
			{
			
				$emailObj = new resetPasswordMail($generatedPasswd,$this->email);
				$emailObj->sendEmail();
				header("location: ".SITE_PATH."index.php?controller=login&function=login&reset=1");
	
			} else {
				header("location: ".SITE_PATH."index.php?controller=login&function=login&reset=2");
			}
				
		} else {
				header("location: ".SITE_PATH."index.php?controller=login&function=login&reset=3");
		}
	}



	}

	// display forget password window
	public function forgetPassword()
		{
			loadView('forgetpassword.php');	
		}

	// this function will authenticate a user

	public function loginValidate() 
	{
		$this->validateLoginFlag = 0;

		if(isset($_POST['txtUserName']) && !empty($_POST['txtUserName'])) {

			$this->userName = mysql_real_escape_string($_POST['txtUserName']);
			$this->email = mysql_real_escape_string($_POST['txtUserName']);
		} 

		if(isset($_POST['txtPassword']) && !empty($_POST['txtPassword'])) {

			$this->password = mysql_real_escape_string($_POST['txtPassword']);
		}
		
		$unameRes = $this->validationObj->validateUserName($this->userName);
		$emailRes = $this->validationObj->validateEmail($this->email);
		$pswdRes  = $this->validationObj->validatePassword($this->password);

		if($pswdRes === true) {
			
			$data;
			if($unameRes === true) {
			
			// send data to user model
			$data = array(

					'username' => $this->userName,
					'password' => $this->password
				);

			} elseif($emailRes === true) {

		
				$data =  array(

					'email' => $this->email,
					'password' => $this->password

				);
			} else {
			// validation not succesfull

			$this->validateLoginFlag = 1;

			
			}

			$userData = loadModel('user','findUserId',$data);
			if(isset($userData) && !empty($userData)) {
				$this->sessionObj->set('userId',$userData['userId']);
				$this->sessionObj->set('clientId',$userData['clientId']);		 // set client id of the user to get access of clientdb	
				header('location:'.SITE_PATH.'index.php?controller=dashboard&function=dashboard');

				exit;
			} else {
			
			// User does not exist
			$this->validateLoginFlag = 1;

			}

		} else {
			
			// validation not succesfull
			$this->validateLoginFlag = 1;

		}
		 $arrData = array(
		 			'validateLoginFlag' => $this->validateLoginFlag,
		 			 'username' => $this->userName 
		 			);

		 $this->login($arrData);

	}



	// this function will register a new client

	public function registerValidate() 
	{
		
		if(isset($_POST['txtUsername']) && !empty($_POST['txtUsername'])) {
			$this->userName = mysql_real_escape_string($_POST['txtUsername']);
		}

		if(isset($_POST['txtEmail']) && !empty($_POST['txtEmail'])) {
			$this->email = mysql_real_escape_string($_POST['txtEmail']);
		}
	
		if(isset($_POST['txtFirstName']) && !empty($_POST['txtFirstName'])) {
			$this->firstName = mysql_real_escape_string($_POST['txtFirstName']);
		}

		if(isset($_POST['txtLastName']) && !empty($_POST['txtLastName'])) {
			$this->lastName = mysql_real_escape_string($_POST['txtLastName']);
		}


		$unameRes = $this->validationObj->validateUserName($this->userName);
		$emailRes = $this->validationObj->validateEmail($this->email);
		$fnameRes = $this->validationObj->validateFirstName($this->firstName);
		$lnameRes = $this->validationObj->validateLastName($this->lastName);

		
		// it is used to validate capcha
		require_once(LIBRARY_ROOT."verifyCaptcha.php");

		if(	$unameRes === true 
		   	&& $emailRes === true
			&& $fnameRes === true
			&& $lnameRes === true
			&& $resp->is_valid){

			
			// send data to the user model

			$data = array(

					'username' => $this->userName,
					'email' => $this->email,
					'firstname' => $this->firstName,
					'lastname' => $this->lastName

				);


			$generatedPasswd = loadModel('user','createAccount',$data);

			if(isset($generatedPasswd) && !empty($generatedPasswd))
			{
			
				$emailObj = new sendEmailToNewUser($generatedPasswd,$this->email);
	
				if($emailObj->sendEmail())	
					{
						$this->verifyClient();	
					}

			} else {

				// Unable to create new account
				//$this->validateRegisterFlag = 2;
				header("location: ".SITE_PATH."index.php?controller=login&function=login&success=0");
			}

		} else {

			// validation not successful

			//$this->validateRegisterFlag = 1;
			header("location: ".SITE_PATH."index.php?controller=login&function=login&success=0");	

		}

		$arrData = array(
		 			'validateRegisterFlag' => $this->validateRegisterFlag,
		 			 
		 			);
		 $this->login($arrData);


	}

	// validate username
	public function checkUserName() {


		if(isset($_POST['txtUsername']) && !empty($_POST['txtUsername'])) {
			$this->userName = mysql_real_escape_string($_POST['txtUsername']);
		}

		$unameRes = $this->validationObj->validateUserName($this->userName);

		if($unameRes) {

			 $userNameData =  array('username'=>$this->userName);

			 $return = loadModel('user','isAvaialableUsername',$userNameData);

			 echo $return;
		}

	}

	// validate email
	public function checkEmail() {


		if(isset($_POST['txtEmail']) && !empty($_POST['txtEmail'])) {
			$this->email = mysql_real_escape_string($_POST['txtEmail']);
		}
	
		$emailRes = $this->validationObj->validateEmail($this->email);

		if($emailRes) {
			
			$emailData =  array('email'=>$this->email);

			$return = loadModel('user','isAvaialableEmail',$emailData);

			echo $return;
		}

	}

	// logout from testcube
	public function logout() 
	{
		$this->sessionObj->destroy();
		$this->login();
	}


	// validate re-captcha
	public function validateCaptcha() {

		require_once(LIBRARY_ROOT."verifyCaptcha.php");

		$capchaValue = 0;
		
		if($resp->is_valid) {

			$capchaValue = 1;
			return $capchaValue;

		} else {

			return $capchaValue;
		}

	}

	// set language as english
	public function langEnglish() {
		$this->sessionObj->set('lang','en');
		header('location: '.SITE_PATH.'index.php?controller=login&function=login');
	}

 	// set language as hindi
	public function langHindi() {
		$this->sessionObj->set('lang','hn');
		header('location: '.SITE_PATH.'index.php?controller=login&function=login');
	}

	// set language as french
	public function langFrench() {
		$this->sessionObj->set('lang','fr');
		header('location: '.SITE_PATH.'index.php?controller=login&function=login');
	}
}
