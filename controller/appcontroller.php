<?php 

require_once(LIBRARY_ROOT.'session.php');
require_once(LIBRARY_ROOT.'language.php');
require_once(LIBRARY_ROOT.'error_info.php');
require('validation.php');

class AppController
{
	
	protected $validationObj = null;
	protected $sessionObj = null;
	public static $langCode;
	protected $errorReportObj = null;
	
	function __construct()
	{

		$this->sessionObj = new Session();
		$this->validationObj = new validation();
		$this->errorReportObj = new emailErrorReport();
	
		// get registerd language in session
		$curLang=$this->sessionObj->get('lang');

		if(isset($curLang) && !empty($curLang)) {
			// set current language
			self::$langCode=$curLang;
		} else {
			// set default language as english
			self::$langCode='en';
		}
		// register language in session 
		$this->sessionObj->set('lang',self::$langCode);

	}
		

}
