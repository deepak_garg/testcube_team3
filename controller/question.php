<?php
require_once('appcontroller.php');
require_once('dashboard.php');

require_once(LIBRARY_ROOT.'clientdbconnection.php');


	class questionController extends AppController
	{
		private $userId;
		private $categoryName;
		private $validateCategoryFlag;
		private $categoryId;
		private $quesid;
		private $txtQuestion;

		function __construct()
		{
			
			parent::__construct();
			$this->userId = $this->sessionObj->get('userId');
			
			if(!isset($this->userId) || empty($this->userId)){		
					header('location:'.SITE_PATH.'index.php?controller=login&function=login');
			}
			
			$clientId = $this->sessionObj->get('clientId');
			new clientDbConnection($clientId);
			
		}

		// display the complete list of questions
		public function viewFullQuestion(){
			$id=$_POST['id'];
			$quesFullList=loadModel('question','questionFullList',$id);
			loadView('question_view.php',$quesFullList);
		}

		// allows to add a question
		public function addQuestion(){
			$return = "";
			$status = 0;
			$this->validateQuestionFlag =0;
			//echo $this->userId;
			$currentUserId=loadModel('userid', 'getUserId',$this->userId);

			if(isset($_POST['txtQuestion']) && !empty($_POST['txtQuestion']) 
			&& isset($_POST['opt']) && !empty($_POST['opt']) 
			&& isset($_POST['type']) 
			&& isset($_POST['category']) && !empty($_POST['category']) ){

				
				
				$this->txtQuestion = mysql_real_escape_string($_POST['txtQuestion']);
				//$catNameRes = $this->validationObj->validateCategoryName($this->categoryName);

					$quesdata = array(
							'cat_id'	 => $_POST['category'],
							'question'   => $this->txtQuestion ,
							'type' 		 => $_POST['type'] ,
							'created_by' => $currentUserId,
							'points'	 => $_POST['txtPoints'],
							'options' 	 => $_POST['opt'],
							'answers'	 => $_POST['answer']
						);
					$status = loadModel('question','addquestion',$quesdata);	
					
				}	
			 else {
				$this->validateQuestionFlag = 1;
		}

			header("location: ".SITE_PATH."index.php?controller=dashboard&function=questionAdd&success=".$status."&valid=".$validateQuestionFlag);
			
			
	}

	//edit question
	public function editQuestion(){
			$return = "";
			$status = 0;
			$this->validateQuestionFlag =0;
			//echo $this->userId;
			$currentUserId=loadModel('userid', 'getUserId',$this->userId);
			if(isset($_POST['txtQuestion']) && !empty($_POST['txtQuestion']) 
			&& isset($_POST['opt']) && !empty($_POST['opt']) 
			&& isset($_POST['type']) 
			&& isset($_POST['category']) && !empty($_POST['category'])
			&& isset($_POST['optid'])    && !empty($_POST['optid']) ) {

		    $this->txtQuestion = mysql_real_escape_string($_POST['txtQuestion']);
				
					$quesdata = array(
							'ques_id'	 =>	$_POST['id'],		
							'cat_id'	 => $_POST['category'],
							'question'   => $this->txtQuestion ,
							'type' 		 => $_POST['type'] ,
							'updated_by' => $currentUserId,
							'points'	 => $_POST['txtPoints'],
							'optid'		 => $_POST['optid'],
							'options' 	 => (array_filter($_POST['opt'])),
							'answers'	 => $_POST['answer']
						);


					$status = loadModel('question','editQuestion',$quesdata);	
					
				}	
			 else {
				$this->validateQuestionFlag = 1;
		}

			 header("location: ".SITE_PATH."index.php?controller=dashboard&function=question&success="
				.$status."&valid=".$validateQuestionFlag);
				
	}


		//popup question delete with delete page
		public function questionDelete(){
			$id=$_POST['id'];
			$quesFullList=loadModel('question','questionFullList',$id);
			loadView('question_delete.php',$quesFullList);
		}

		//popup multiple question delete with delete page
		public function mulquestionDelete(){
			loadView('mul_question_delete.php');
		}

		//delete question set status = 1;
		public function delQuestion()
		{
				
			$currentUserId=loadModel('userid', 'getUserId',$this->userId);

			if(isset($_POST['quesid']) && !empty($_POST['quesid'])) {
				$this->quesId = $_POST['quesid'];
				$arr=loadModel('question','delQuestion',$this->quesId);
				if($arr == 1){
				$questionList=loadModel('question','questionList');
			  	loadView('question.php',$questionList);
		  		}
					
			}
			 

		}

		// display the question detail
		public function viewQuestion(){
			loadView('main_header.php');
			loadView('sidebar.php');
			 $id=$_GET['id'];
      		$arrArgument=array('id'=>$id);
			$quesData=loadModel('questions','viewQuestion');
			loadView('questions.php',$quesData);
		}


		// used to delete multiple questions
		public function deleteMulques()
		{

			$currentUserId=loadModel('userid', 'getUserId',$this->userId);
			

			if(isset($_POST['quesid']) && !empty($_POST['quesid'])) {
				$this->quesid = $_POST['quesid'];
				
				$arr=loadModel('question','deleteMulques',$this->quesid);
				if($arr == 1){
				$questionList=loadModel('question','questionList');
			  loadView('question.php',$questionList);
		  }
					
			}	

		}		


}
?>
