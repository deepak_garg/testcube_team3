<?php

	require_once('appcontroller.php');

	//include_once LIBRARY_ROOT . "userconfig.php";
	require_once(LIBRARY_ROOT.'clientdbconnection.php');

	class testController extends AppController 
	{
		
		 private $userId;
		 private $clientId;
		 private $testId;
		 private $testName;
		 private $testDescription;
		 private $testErrMsg;
		 private $testNameRes=false;
		 private $testDescRes=false;


		 // it is used to check the session 
		 public function __construct()
		 {
		 	 parent::__construct();

			$this->userId = $this->sessionObj->get('userId');
		
			if(!isset($this->userId) || empty($this->userId)){		
					header('location:'.SITE_PATH.'index.php?controller=login&function=login');
			}

			$clientId = $this->sessionObj->get('clientId');
			new clientDbConnection($clientId);

			

		 }	


		 
		 //it is used to show create test popup 		 
		 public function createTest()
		 {
		 	loadView('test_create.php');
		 }


		// it is used to send the data for the new test to test model	
		 public function addTest()
		 {
		 		$return = "";
		 		$this->testErrMsg = 0;
		 		$status = 0;

		 		
				$currentUserId=loadModel('userid', 'getUserId',$this->userId);

		 		/*	
				 it is used to check whether testname and testdescription
				 are set and not empty.
				 if both are set and not empty send the data to the test model
				 */ 		
				if(isset($_POST['txtTestName']) && !empty($_POST['txtTestName'])) {

					 	if(isset($_POST['txtTestDescription']) && !empty($_POST['txtTestDescription'])) {

							$this->testDescription = mysql_real_escape_string($_POST['txtTestDescription']);
							$this->testName = mysql_real_escape_string($_POST['txtTestName']);	

							$this->testNameRes = $this->validationObj->validateTestName($this->testName);
							$this->testDescRes = $this->validationObj->validateTestDescription($this->testDescription);
								
								if($this->testNameRes===true && $this->testDescRes===true) {

											$testData = array(

												'testname' => $this->testName,
												'testdescription' => $this->testDescription,
												'created_by' =>$currentUserId
											);	
				 	
				 						   $status = loadModel('test','addTest',$testData);

				  						} else {

				   						$this->testErrMsg = 3;   //invalid test and description 

				   					}	

				   } else {

				   	   $this->testErrMsg = 2; // test description can't be blank
				   } 	

				} else {

						$this->testErrMsg = 1; // test name can't be blank
					}
			
				
				$return = array('regFlag' => $this->testErrMsg, 'val' => $status);
				echo json_encode($return);

					// function end
				}



		// it is used to show delete test popup 		
		public function deleteTestPopUp()					
		{
			 loadView('test_delete.php');			
		}


		// it is used to send the test id to the test model for soft delete 				
		public function deleteTest()
		{

			$returnMsg = 0;

			
			if(isset($_POST['txtId']) && !empty($_POST['txtId'])) {

				$this->testId = $_POST['txtId'];
				
				$returnMsg = loadModel('test','deleteTest',$this->testId);		

			}
			$return = array('regFlag' => $returnMsg);
			echo json_encode($return);

		}		


		// it is  used to show multiple delete test page

		public function multipleDeleteTestPopUp()
		{
			loadView('test_multiple_delete.php');
		}


		
		// used to send multiple test id to the test model for soft delete
		public function multipleDeleteTest()
		{

			if(isset($_POST['testId']) && !empty($_POST['testId'])) {

				$returnMsg = loadModel('test','multipleDeleteTest',$_POST['testId']);
			}		

			echo $returnMsg;
		}




		// it is used to show edit test popup with the corresponding test name 
		// and its description
		public function editTestPopUp()	

		{
			loadView('test_edit.php');			

		}


		// it is used to update/edit the test name and description
		// this function validates the new name and description and 
		// then load the modal to update the test name and description
		public function editTest()
		{

				$return = "";
		 		$this->testErrMsg = 0;
		 		$status = 0;

				
				$currentUserId = loadModel('userid', 'getUserId',$this->userId);

				if(isset($_POST['txtTestId']) && !empty($_POST['txtTestId'])) {

						$this->testId = $_POST['txtTestId'];
				}


				if(isset($_POST['txtTestName']) && !empty($_POST['txtTestName'])) {

					 	if(isset($_POST['txtTestDescription']) && !empty($_POST['txtTestDescription'])) {

							$this->testDescription = mysql_real_escape_string($_POST['txtTestDescription']);
							$this->testName = mysql_real_escape_string($_POST['txtTestName']);	

							$this->testNameRes = $this->validationObj->validateTestName($this->testName);
							$this->testDescRes = $this->validationObj->validateTestDescription($this->testDescription);
								
								if($this->testNameRes===true && $this->testDescRes===true) {

											$testUpdatedData = array(

												'testname' => $this->testName,
												'testdescription' => $this->testDescription,
												'updated_by' =>$currentUserId,
												'testid' => $this->testId
											);	
				 	
				 						   $status = loadModel('test','editTest',$testUpdatedData);

				  						} else {

				   						$this->testErrMsg = 3; //invalid test and description
		 

				   					}	

				   		} else {

				   	   	$this->testErrMsg = 2; // test description can't be blank
				   	   
				   } 	

				} else {

						$this->testErrMsg = 1; // test name can't be blank
						
					}
							
					$return = array('regFlag' => $this->testErrMsg, 'val' => $status);
					echo json_encode($return);

					// function end
		}


		

		// used to pass questions ids and test id to the test modal's addQuestionsToTest function  
		public function assignQuestionsToTest()
		{

			$returnMsg = 0;

			if(isset($_POST['testId']) && !empty($_POST['testId']) 
				&& isset($_POST['quesId']) && !empty($_POST['quesId'])) {

				$questionIdTestId = array(

									'quesid' =>$_POST['quesId'],
									'testid' =>$_POST['testId']

					);	

				$returnMsg = loadModel('test','assignQuestionsToTest',$questionIdTestId);

			}

			echo $returnMsg;

		}	



		// used to load delete_question_from_test.php
		public function deleteQuestionFromTestPopUp()
		{
			loadView('delete_question_from_test.php');
		}


		// used to send question id and test id to test model's removeQuestionFromTest function 
		// to remove the question from the test

		public function removeQuestionFromTest()
		{
			$returnMsg = 0;

			if(isset($_POST['quesId']) && !empty($_POST['quesId']) 
				&& isset($_POST['testId']) && !empty($_POST['testId'])) {	

				$questionIdTestId = array(

									'quesid' =>$_POST['quesId'],
									'testid' =>$_POST['testId']

					);	

					$returnMsg = loadModel('test','removeQuestionFromTest',$questionIdTestId);					

			}

			echo $returnMsg;

		} 

		// this function will generate a new link
		public function createNewLink()
		{

			$returnMsg = 0;
			$testId = "";
			$linkName = "";
			$availability=0;
			$attempts = 1;
			$custom_instruction = "";
			$totalQue=0;
			$password="";
			$rqrFirstName =0;
			$rqrLastName =0;
			$rqrEmail=1;
			$guidlines =0;
			$timeLimit = 30;
			$randomize = 0;
			$rqrAnswer = 1;
			$passMark = 50;
			$emailResult = 1;
			$arrLinkData ="";
			if(isset($_POST['testId']) && !empty($_POST['testId'])
				&& isset($_POST['linkName']) && !empty($_POST['linkName'])) {
					$testId = $_POST['testId'];
					$linkName = $_POST['linkName'];

					$currentUserId=loadModel('userid', 'getUserId',$this->userId);

					if(isset($_POST['availability']) && !empty($_POST['availability'])) {
						$availability = $_POST['availability'];
					}

					if(isset($_POST['attempts']) && !empty($_POST['attempts']))  {
							$attempts = $_POST['attempts'];
					}
					if(isset($arrData['password']) && !empty($arrData['password']))  {
							$password = $_POST['password'];
					}
					if(isset($_POST['limit']) && !empty($_POST['limit']))  {


							$timeLimit = $_POST['limit'];
					}
					if(isset($_POST['random']) && !empty($_POST['random']))  {

							$randomize = $_POST['random'];
					}
					if(isset($_POST['fname']) && !empty($_POST['fname']))  {

							$rqrFirstName = $_POST['rqrFirstName'];
					}
					if(isset($_POST['lname']) && !empty($_POST['lname']))  {

							$rqrLastName = $_POST['rqrLastName'];
					}
					if(isset($_POST['email']) && !empty($_POST['email']))  {

							$rqrEmail = $_POST['email'];
					}
					if(isset($_POST['defaultins']) && !empty($_POST['defaultins']))  {

							$guidlines = $_POST['defaultins'];

					if(isset($_POST['customins']) && !empty($_POST['customins']))  {

							$custom_instruction = $_POST['custom_instruction'];
					}
					}
					if(isset($arrData['rqrAnswer']) && !empty($arrData['rqrAnswer']))  {

							$rqrAnswer = $arrData['rqrAnswer'];
					}
					if(isset($_POST['passingmarks']) && !empty($_POST['passingmarks']))  {

							$passMark = $_POST['passingmarks'];
					}
					if(isset($_POST['total-questions']) && !empty($_POST['total-questions']))  {

							$totalQue = $_POST['total-questions'];
					}

						

					$arrLinkData = array('linkData'=>array('testId' => $testId,
												 	'name' => $linkName,
										 			'created_by' => $currentUserId),
										 'availability' =>$availability,
										 'attempts' =>$attempts,
										 'custom_instruction' => $custom_instruction,
										 'password' =>	$password,
										 'rqrFirstName' =>	$rqrFirstName,
										 'rqrLastName' =>	$rqrLastName,
										 'rqrEmail' =>	$rqrEmail,
										 'guidlines' =>	$guidlines,
										 'timeLimit' =>	$timeLimit,
										 'totalQue' => $totalQue,
										 'randomize' =>	$randomize,
										 'rqrAnswer' =>	$rqrAnswer,
										 'passMark' =>	$passMark,
										 'emailResult' =>	$emailResult);
					
						
						$success = loadmodel('settings','newSettings',$arrLinkData);
						

						if($success) {
							$returnMsg =1;
						}
					}


					echo $returnMsg;

				
		}	

		public function generateLinkCode()
		{
			$return="";
			$linkId = "";
			$linkCode ="";
			$fromDate ="";
			$toDate ="";
			if(isset($_POST['linkId']) && !empty($_POST['linkId'])
					&& isset($_POST['from']) && !empty($_POST['from'])
					&& isset($_POST['to']) && !empty($_POST['to'])) {

				include_once LIBRARY_ROOT.'passwordGenerator.php';
				$linkCode = generate_code(12);
				$linkId = $_POST['linkId'];
				$fromDate = $this->dateTimeFormat($_POST['from']);
				$toDate = $this->dateTimeFormat($_POST['to']);

				$data = array('linkId' => $linkId,
								'linkCode' => $linkCode,
								'from' => $fromDate,
								'to' => $toDate);
				
				$success = loadModel('test','generateLinkCode',$data);
				
				if($success) {
					$return = $linkCode;

				} else {
					$return =-1;
				}

			} else {
				$return = 0;
			}

			echo $return;
		}

		// this function will format the date and time to desired datetime format of mysql
		public function dateTimeFormat($date)
		{
			$newDate ="";
			if(isset($date) && !empty($date)) {
				$date = explode(" ", $date);
				$meridian = $date[2];
				$time = $date[1];
				$date = $date[0];
				$date = explode("/",$date);				
				$mm = $date[0];
				$dd = $date[1];
				$yyyy = $date[2];				
				$time = explode(":", $time);
				$hr = $time[0];
				$min = $time[1];
				$sec = '00';
				if($meridian == 'PM') {
					$hr += 12;
				}
								
				$newDate = $yyyy.'-'.$mm.'-'.$dd.' '.$hr.':'.$min.':'.$sec;
			}
			return $newDate;
		}

		public function linksListing(){
			$linkId = $_POST['linkId'];
			if(isset($linkId) && !empty($linkId)){
				$linkData = $linkId;
				echo $linkData;
			}
			
		}

		


	}

