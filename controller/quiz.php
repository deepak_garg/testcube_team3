<?php

	require_once('appcontroller.php');

	require_once(LIBRARY_ROOT.'clientdbconnection.php');

	class quizController extends AppController 
	{
		
		 private $userId;
		 private $clientId;
		 private $testId;
		 private $testName;
		 private $linkId;
		 private $linkName;
		 private $linkCode;
		 private $testDescription;


		 // initialize the app
		 public function __construct()
		 {

		 	 parent::__construct();

			$this->userId = $this->sessionObj->get('userId');

			if(isset($this->userId) && !empty($this->userId)){		
				$this->sessionObj->set('mode','preview');	
			} else {					
				$this->sessionObj->set('mode','test');	
			}
		

			$this->clientId = $this->sessionObj->get('clientId');
			if(!isset($this->clientId) || empty($this->clientId)) {
				if(isset($_GET['client']) && !empty($_GET['client'])) {
					$this->clientId = $_GET['client'];
					$this->sessionObj->set('clientId',$this->clientId);
				} else {
					header('location:'.SITE_PATH.'index.php?controller=login&function=login');
				}
			}
			new clientDbConnection($this->clientId);

			//self::$counter++;

		 }	


		 //use http://local.testcube.com/index.php?controller=quiz&function=preview&client=1&id=3

		 public function preview()
		 {
			 	$linkId ="";



			 	$this->sessionObj->remove('studentId');
				$this->sessionObj->remove('studName');
			 	$mode = $this->sessionObj->get('mode');

			 	if(isset($_GET['id']) && !empty($_GET['id'])) {
			 		$linkId = $_GET['id'];

			 		$arrData = loadModel('test','quizDetails',$linkId);	

			 		$arrData = $arrData[0];
					$arrData['time'] *= 60;	

			 		$this->sessionObj->set('quizData',$arrData);


			 	}	

			 	if(isset($_GET['code']) && !empty($_GET['code'])) {


					$data = loadModel('test','getLinkCodeTime',$_GET['code']);

					
					$currentTime = date('Y-m-d H:i:s');

						if($data['showfrom'] <= $currentTime && $data['showuntill'] >=$currentTime) {	
							  		
							  if($mode =='test') {

							  		$this->studentLogin();
							  }

						}					
						else {
							
							echo 'Sorry,The link is expired!!! Better luck next time';  
						}	

			 	} else {

				 	if($mode == 'preview') {
				 		loadView('student_header.php');
				 		loadView('student_instructions.php',$arrData);
				 	}	

			 	}

		 }

		// display student login page
		public function studentLogin()
		{		
			$quizData = $this->sessionObj->get('quizData');
			loadView('student_header.php');
			loadView('student_login.php',$quizData);
		}

		// display student login page
		public function studentResult()
		{
						
			loadView('student_header.php');
			loadView('student_result.php');
		}

		// display student test
		public function startTest()
		{

			$mode = $this->sessionObj->get('mode');

		 	if($mode === 'preview') {
		 		$studentId = 0; 
		 	} else {
				$studentId = $this->sessionObj->get('studentId');

		 	}

			$quizData = $this->sessionObj->get('quizData');	
			$arrQueId = loadModel('test','getArrQuestion',$quizData);

			$this->sessionObj->set('arrQueId',$arrQueId);

			$startTime=date_create(date("H:i:s"));
			$this->sessionObj->set('startTime',$startTime);

			header('location:'.SITE_PATH.'index.php?controller=quiz&function=giveTest&counter=0');


		}

		
		public function giveTest()
		{
			$studName = $this->sessionObj->get('studName');
			if(isset($studName) && !empty($studName)) {
				loadView('student_header.php',$studName);
			} else {
				loadView('student_header.php');
			}
			$mode = $this->sessionObj->get('mode');
		 	if($mode == 'preview') {
		 		$studentId = 0;
		 	} else {
				$studentId = $this->sessionObj->get('studentId');
		 	}

			$quizData = $this->sessionObj->get('quizData');

			$arrQueId = $this->sessionObj->get('arrQueId');

			$flagForNextButton = 0;	
			$startTime = $this->sessionObj->get('startTime');
			$startTimeDate = $startTime->date;
			$currentTime = date_create(date("H:i:s"));			
			$interval = date_diff($currentTime,$startTime);
			
			$lapse = $interval->s + $interval->i *60 + $interval->h *3600;
			$quizData['totalTime'] = $quizData['time'];
			$quizData['time'] = $quizData['time'] - $lapse;
			$lengthQuestionIdArray = count($arrQueId);

			if(isset($_GET['counter']) && !empty($_GET['counter'])) {

					$counter = $_GET['counter'];
			} else {

				 	$counter = 0;
			}

				$this->sessionObj->set('currentQuestionId',$arrQueId[$counter]);
				$data = array($studentId,$arrQueId[$counter]);
				$questionData = loadModel('teststart','loadQuestion',$data);

			if($counter < $lengthQuestionIdArray -1) {

			 	 	$returnData = array($quizData,$questionData[0],$counter+1,$flagForNextButton,$questionData[1]);

			 		} elseif($counter < $lengthQuestionIdArray) {

			 			$flagForNextButton = 1;

						$returnData = array($quizData,$questionData[0],$counter+1,$flagForNextButton,$questionData[1]);

			 	}
			 	$returnData['startDate'] = $startTimeDate; 

			 	loadView('student_test.php',$returnData);

			 	loadView('student_footer.php');

		}

		public function addSelectedOption()
		{
			
			$mode = $this->sessionObj->get('mode');
		 	if($mode == 'preview') {
		 		$studentId = 0;
		 	} else {
				$studentId = $this->sessionObj->get('studentId');
		 	}

		 	$currentQuestionId = $this->sessionObj->get('currentQuestionId');

		 	if(isset($_POST['optionId']) && !empty($_POST['optionId'])) {

		 			$selectedOptionId = $_POST['optionId'];

		 	}

		 	$linkCode = "qwertytuio";	

		 	$data = array($studentId,$currentQuestionId,$selectedOptionId,$linkCode);

		 	$return = loadModel('teststart','addSelectedOption',$data);

		 	echo $return;

		}

		public function resultProcessing()
		{
			$studName = $this->sessionObj->get('studName');
			if(isset($studName) && !empty($studName)) {
				loadView('student_header.php',$studName);
			} else {
				loadView('student_header.php');
			}
			$quizData = $this->sessionObj->get('quizData');

			$testName = $quizData['testName'];
			$totalQuestions = $quizData['queTotal'];	

			$returnData = "";

			$mode = $this->sessionObj->get('mode');
		 	if($mode == 'preview') {
		 		$studentId = 0;
		 	} else {
				$studentId = $this->sessionObj->get('studentId');
		 	}	

		 	$linkCode = "qwertytuio";

		 	$data = array($linkCode,$studentId);

			$correctCount = loadModel('teststart','resultProcessing',$data);

			$returnData = array($testName,$totalQuestions,$correctCount);	 

			loadView('student_result.php',$returnData);

			loadView('student_footer.php');
			 
		}	

		public function showQuiz()
		{
			$studentId = "";
			$fname = "";
			$lname ="";
			$email ="";
			if(isset($_POST['txtFname']) && !empty($_POST['txtFname'])) {
				$fname = $_POST['txtFname'];
			}
			if(isset($_POST['txtLname']) && !empty($_POST['txtLname'])) {
				$lname = $_POST['txtLname'];
			}
			if(isset($_POST['txtUserName']) && !empty($_POST['txtUserName'])) {
				$email = $_POST['txtUserName'];
			}

				
			$arrData = array('fname' => $fname,
							'lname' => $lname,
							'email' => $email);
			$studentId = loadModel('test','registerStudent',$arrData);
			$this->sessionObj->set('studentId',$studentId);
			$this->sessionObj->set('studName',$fname." ".$lname);
			$quizData = $this->sessionObj->get('quizData');

			
			$linkId = $quizData['linkId'];
			$arrData = loadModel('test','quizDetails',$linkId);

			$arrData = $arrData[0];
			$arrData['time'] *= 60;	
			$this->sessionObj->set('quizData',$arrData);
			
			loadView('student_header.php',$fname." ".$lname);
	 		loadView('student_instructions.php',$arrData);
		}

		
}
?>
