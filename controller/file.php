<?php


require_once('appcontroller.php');

require_once(LIBRARY_ROOT.'clientdbconnection.php');

	class fileController extends AppController
	{
		private $userId;
		private $errFileFlag;
		private $msgFileFlag;

		function __construct()
		{
			
			parent::__construct();
			$this->userId = $this->sessionObj->get('userId');
			$clientId = $this->sessionObj->get('clientId');
				
			new clientDbConnection($clientId);

			if(!isset($this->userId) || empty($this->userId)) {		
					header('location:'.SITE_PATH.'index.php?controller=login&function=login');
			} 

		}

		// this function is used to load questions from excel to mysql database
		public function	addQuestion() 
		{
			
			$filePath = $_POST['fileP'];
			
			$currentUserId=loadModel('userid', 'getUserId',$this->userId);
			$arrData = array('file' => $filePath,'userId'=>$currentUserId,'testId'=>2);
			$arr=loadModel('data','loadDataFromExcel',$arrData);
			if($arr) {
				//question added successfully
				echo 1;
				unlink($filePath);

			} 
		}

		


		// this function is used to upload an excel file
		public function fileUpload(){

			$this->errFileFlag = 0;
			$fileExt=explode('.',$_FILES['file']['name']);
			if($fileExt[1] == "xls" || $fileExt[1] == "xlsx" ){
			
			// max file size : 20MB
				if($_FILES['file']['size']/(1024*1024) <= 20) {
					$_FILES['file']['name'] = $this->userId.$_FILES['file']['name'];
					
					
					
					
					if(move_uploaded_file($_FILES['file']['tmp_name'],"data/".$_FILES['file']['name'])){
						$filePath = SITE_ROOT.'data/'.$_FILES['file']['name'];	
						$this->msgFileFlag = 3; // file uploaded successfully
						
						echo $filePath; // print the complet file path
						

					} else {
						$this->errFileFlag = 4; // file not uploaded
					}

				} else {
					$this->errFileFlag = 2; //invalid file size 
				}

		}

	}
}

?>