<?php


require_once('appcontroller.php');
require_once('dashboard.php');

require_once(LIBRARY_ROOT.'clientdbconnection.php');


	class categoryController extends AppController
	{
		private $userId;
		private $categoryName;
		private $validateCategoryFlag;
		private $categoryId;
		private $catId;
		private $testErrMsg;
		private $categoryData;
		function __construct()
		{
			
			parent::__construct();
			$this->userId = $this->sessionObj->get('userId');
			
			if(!isset($this->userId) || empty($this->userId)){		
					header('location:'.SITE_PATH.'index.php?controller=login&function=login');
			}
			
			$clientId = $this->sessionObj->get('clientId');
			new clientDbConnection($clientId);
			
		}

// used to send the data for creating new category to the category model 
		public function addCategory(){

			$return = "";
			$status = 0;
			$this->validateCategoryFlag =0;
			$currentUserId=loadModel('userid', 'getUserId',$this->userId);

			if(isset($_POST['txtCategoryName']) && !empty($_POST['txtCategoryName'])){

				$this->categoryName = mysql_real_escape_string(strtoupper($_POST['txtCategoryName']));

				$catNameRes = $this->validationObj->validateCategoryName($this->categoryName);

				if($catNameRes === true) {
					$data = array(
							'name' => $this->categoryName ,
							'created_by' => $currentUserId
						);
					$status = loadModel('category','addCategory',$data);	
					
					
			} else {
					$this->validateCategoryFlag = 2; //invalid cat name
				}	
			} else {
				$this->validateCategoryFlag = 1; //category name is blank

		}
			$return = array('regFlag' => $this->validateCategoryFlag,'val' => $status);
			echo json_encode($return);

		}	
		

// used to show create category popup
		public function createCategory()
		{
			loadView('category_create.php');	
		}

		

// used to send the data to retrieve category name to the category model for deletion
		public function delCatName()
		{
				
			$currentUserId=loadModel('userid', 'getUserId',$this->userId);
			

			if(isset($_POST['categoryid']) && !empty($_POST['categoryid'])) {
				$this->cId = $_POST['categoryid'];
				$arr=loadModel('category','delCatName',$this->cId);
				loadView('category_delete.php',$arr);
				

		} 	
	}
		
		// used to send the data to delete category to the category model
		public function delCategory()
		{
				
			$currentUserId=loadModel('userid', 'getUserId',$this->userId);
			

			if(isset($_POST['categoryid']) && !empty($_POST['categoryid'])) {
				$this->cId = $_POST['categoryid'];
				$arr=loadModel('category','delCategory',$this->cId);
				echo $arr;
				exit;
				if($arr == 1){
				$categoryList=loadModel('category','categoryList');
			  	loadView('category.php',$categoryList);

		  		} else if($arr == 2){
		  			echo $arr;
		  		}
					
			} 
		  		

		}		
	
	// used to send the data to retrieve category name to the category model for editing		
		public function editCatName()	
		{
			$currentUserId=loadModel('userid', 'getUserId',$this->userId);
			if(isset($_POST['categoryid']) && !empty($_POST['categoryid'])) {

				$this->categoryId = $_POST['categoryid'];

			 	$arr = loadModel('category','editCatName',$this->categoryId);

			 	loadView('category_edit.php',$arr);

			}

		}

// used to send the data to edit category to the category model
		public function editCategory()
		{
			$return = "";
			$status = 0;
			$this->validateCategoryFlag =0;

			$currentUserId=loadModel('userid', 'getUserId',$this->userId);

				if(isset($_POST['categoryid']) && !empty($_POST['categoryid'])) {

						$this->catId = $_POST['categoryid'];

				if(isset($_POST['categoryname']) && !empty($_POST['categoryname'])) {

							$this->categoryName = mysql_real_escape_string($_POST['categoryname']);	

							$categoryNameRes = $this->validationObj->validateCategoryName($this->categoryName);

								
								if($categoryNameRes===true) {

											$this->categoryData = array(

												'categoryname' => $this->categoryName,
												'updated_by' => $currentUserId,
												'categoryid' => $this->catId	
											);	

				 						   $status = loadModel('category','editCategory',$this->categoryData);
				 						   
				 						   
				   		

								} else {

										$this->validateCategoryFlag = 2; // test name can't be blank
						
								}
							
					

			} else {
				$this->validateCategoryFlag = 1;	
			}		// function end
		} else {
			$this->validateCategoryFlag = 1;
		}
		$return = array('regFlag' => $this->validateCategoryFlag,'val' => $status);
			echo json_encode($return);

	}

	// public function viewCat()	
	// 	{
	// 		$currentUserId=loadModel('userid', 'getUserId',$this->userId);
	// 		if(isset($_POST['categoryid']) && !empty($_POST['categoryid'])) {

	// 			$this->categoryId = $_POST['categoryid'];


	// 		 	$arr = loadModel('category','viewCat',$this->categoryId);

	// 		 	loadView('category.php',$arr);

	// 		}

	// 	}


// used to show delete multiple category popup
		public function deleteMulCatName()
		{
				loadView('mul_category_delete.php');
			
		}	


// used to show multiple category deletion error popup
		public function deleteMulCatError()
		{
				loadView('mul_category_delete_error.php');
			
		}		


// used to send the data to delete multiple category to the category model
			public function deleteMulCat()
		{
				// /loadView('mul_category_delete.php');
			$currentUserId=loadModel('userid', 'getUserId',$this->userId);
			

			if(isset($_POST['categoryid']) && !empty($_POST['categoryid'])) {
				$this->cId = $_POST['categoryid'];
				
				$arr=loadModel('category','deleteMulCat',$this->cId);
				if($arr == 1){
				$categoryList=loadModel('category','categoryList');
			  loadView('category.php',$categoryList);
		  }
					
			}	

		}			


}
