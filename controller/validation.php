<?php
ini_set("display_errors", "1");

class validation
{
    
    public function __construct()
    {
    }


    // it is used for validating username
    public function validateUserName($userName)
    {
        if (strlen($userName) === 0) { //username should not be empty
            return false;
        } elseif (!preg_match('/^([a-zA-Z0-9_]{5,20})$/', $userName)) { // username should be valid 
            return false;
        } else {
            return true;
        }
    }


    // it is used for validating email 
    public function validateEmail($email)
    {
        if (strlen($email) === 0) { // email should not be empty
            return false;
        } elseif (strlen($email) > 150) { // email can't be greater than 150 characters
            return false;
        } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) { // email should be valid
            return false;
        } else {
            return true;
        }
    }


    // it is used for validating first name 
    public function validateFirstName($firstName)
    {
        if (strlen($firstName) === 0) { // firstname should not be empty
            return false;
        } elseif (!preg_match('/^[a-zA-Z]{1,20}$/', $firstName)) { //firstname contains characters only
            return false;
        } else {
            return true;
        }
    }


    // it is used for validating first name
    public function validateLastName($lastName)
    {
        if (!preg_match('/^[a-zA-Z]{0,20}$/',$lastName)) { //lastname contains characters only
            return false;
        } else {
            return true;
        }
    }


    // it is used for validating password
    public function validatePassword($password)
    {
        if (strlen($password) === 0) {
            return false;
        } elseif (strlen($password) < 5 || strlen($password) > 30) {
            return false;
        } else {
            return true;
        }
    }



    // it is used to validate test name

    public function validateTestName($test)
    {
        if(strlen($test) ===0) {
            return false;
        } elseif(!preg_match('/^[a-zA-Z0-9-_ ]{1,30}$/',$test)) {
            return false;

        } else {
            return true;
        }

    }

    // it is used to validate test description
    public function validateTestDescription($testDesc)
    {

        if(strlen($testDesc) === 0) {

                return false;

        } elseif(!preg_match('/^[a-zA-Z0-9- ]{1,500}$/',$testDesc)) {
            return false;
        } else {
            return true;
        }

    }






    // it is used to validate category name

    public function validateCategoryName($categoryName) {
        if(strlen($categoryName) === 0) {
            return false;
        } elseif (strlen($categoryName) > 30) {
            return false;
        } elseif(!preg_match('/^[a-zA-Z0-9 +-]{1,30}$/',$categoryName)) {
            return false;    
        } else {
            return true;
        }
    }

    //it is used to validate contact number
    public function validateContact($contact){
        $a=1;
        if(strlen($contact) != 0){
             if(!preg_match('/^[0-9]{10}$/i',$contact)) {
            return false;
            } else {
                return true;
            }
        } else {
            return $a;
        }
       
    }


    public function __destruct()
    {
    }
}
