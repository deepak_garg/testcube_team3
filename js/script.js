var base_url = "http://local.testcube.com/";
$(window).ready(function () {
    
    $("#txtUserName").blur(function () {
        var e = $(this).attr("id");
        if (isNull(e) === true) {
            $("#errUserName").html("Please enter your UserName/Email.")
        } else if (validateUserName(e) === false && validateEmail(e) === false) {
            $("#errUserName").html("Invalid UserName/Email.")
        } else {
            $("#errUserName").html("")
        }
    }).focus(function () {
        $("#errUserName").html("")
    });
    $("#txtPassword").blur(function () {
        var e = $(this).attr("id");
        if (isNull(e) === true) {
            $("#errPassword").html("Please enter password")
        } else if (validatePassword(e) === false) {
            $("#errPassword").html("Invalid Password")
        }
    }).focus(function () {
        $("#errPassword").html("")
    });
    $("#txtRegUsername").blur(function () {
        var e = $(this).attr("id");
        if (isNull(e) === true) {
            $("#errRegUserName").html("Please enter your UserName.")
        } else if (validateUserName(e) === false) {
            $("#errRegUserName").html("Invalid UserName.")
        } else {
            var t = "#" + e;
            var n = $(t).val().trim();
            $.ajax({
                type: "POST",
                data: {
                    txtUsername: n
                },
                url: base_url + "index.php?controller=login&function=checkUserName",
                dataType: "html",
                beforeSend: function () {},
                success: function (e) {
                    if (e == 1) {
                        $("#errRegUserName").html("Username available").css("color", "green")
                    } else {
                        $("#errRegUserName").html("Username already exists").css("color", "red");
                        $("#frmRegister").submit(function (e) {
                            e.preventDefault()
                        })
                    }
                },
                complete: function () {},
                error: function () {}
            })
        }
    }).focus(function () {
        $("#errRegUserName").html("")
    });
    $("#txtEmail").blur(function () {
        var e = $(this).attr("id");
        if (isNull(e) === true) {
            $("#errEmail").html("Please enter your Email.")
        } else if (validateEmail(e) === false) {
            $("#errEmail").html("Invalid Email.")
        } else {
            var t = "#" + e;
            var n = $(t).val().trim();
            $.ajax({
                type: "POST",
                data: {
                    txtEmail: n
                },
                url: base_url + "index.php?controller=login&function=checkEmail",
                dataType: "html",
                beforeSend: function () {},
                success: function (e) {
                    if (e == 1) {
                        $("#errEmail").html("Email available").css("color", "green")
                    } else {
                        $("#errEmail").html("Email already exists").css("color", "red");
                        $("#frmRegister").submit(function (e) {
                            e.preventDefault()
                        })
                    }
                },
                complete: function () {},
                error: function () {}
            })
        }
    }).focus(function () {
        $("#errEmail").html("")
    });
    $("#txtFirstName").blur(function () {
        var e = $(this).attr("id");
        if (isNull(e) === true) {
            $("#errFirstName").html("Please enter your First Name.")
        } else if (validateFirstName(e) === false) {
            $("#errFirstName").html("Invalid First Name.")
        } else {
            $("#errFirstName").html("")
        }
    }).focus(function () {
        $("#errFirstName").html("")
    });
    $("#txtLastName").blur(function () {
        var e = $(this).attr("id");
        if (validateLastName(e) === false) {
            $("#errLastName").html("Invalid Last Name.")
        } else {
            $("#errLastName").html("")
        }
    }).focus(function () {
        $("#errLastName").html("")
    });
    $("#login").submit(function (e) {
        var t = 1;
        $("#login :input").each(function () {
            var n = $(this).attr("id");
            if (isNull(n) === true) {
                if (t === 1) {
                    $("#errUserName").html("Please enter your UserName/Email.")
                } else if (t === 2) {
                    $("#errPassword").html("Please enter password")
                }
                e.preventDefault()
            } else if (t === 1) {
                if (validateUserName(n) === false && validateEmail(n) === false) {
                    e.preventDefault()
                }
            } else if (t === 2) {
                if (validatePassword(n) === false) {
                    e.preventDefault()
                }
            }
            t++
        })
    });
    $("#frmRegister").submit(function (e) {
        var t = 1;
        $("#frmRegister :text").each(function () {
            var n = $(this).attr("id");
            if (isNull(n) === true) {
                if (t === 1) {
                    $("#errRegUserName").html("Please enter your UserName.")
                } else if (t === 2) {
                    $("#errEmail").html("Please enter your Email.")
                } else if (t === 3) {
                    $("#errFirstName").html("Please enter your First Name.")
                } else if (t === 5) {
                    $("#errCaptcha").html("Captcha cannot be blank")
                }
                if (t != 4) {
                    e.preventDefault()
                }
            }
            if (t === 1) {
                if (validateUserName(n) === false) {
                    e.preventDefault()
                }
            } else if (t === 2) {
                if (validateEmail(n) === false) {
                    e.preventDefault()
                }
            } else if (t === 3) {
                if (validateFirstName(n) === false) {
                    e.preventDefault()
                }
            } else {}
            t++
        })
    });
    $(document).on("click", "#btnAddCategory", function (e) {
        var t = $("#txtCategoryName").val().trim();
        var status = $
        $.ajax({
            type: "POST",
            data: {
                txtCategoryName: t
            },
            url: base_url + "index.php?controller=category&function=addCategory",
            dataType: "json",
            beforeSend: function () {},
            success: function (e) {
                if (e.regFlag == 1) {
                    $("#errCat").html("Category name cannot be blank").css("color", "red")
                } else if (e.regFlag == 2) {
                    $("#errCat").html("Invalid Category name").css("color", "red")
                } else if (e.val == 2) {
                    $("#errCat").html("Category already exists").css("color", "red")
                } else if (e.val == 1) {
                    	 $("#errCat").html("");
                        document.location.reload(true)
                    
                } else if(e.val == ""){
                	 $("#errCat").html("Internal Server Error").css("color", "red")
                }
            },
            complete: function () {},
            error: function () {}
        })
    });
    $(document).on("click", "#btnAddTest", function (e) {
        var t = $("#txtTestName").val().trim();
        var n = $("#txtTestDescription").val().trim();
        $.ajax({
            type: "POST",
            data: {
                txtTestName: t,
                txtTestDescription: n
            },
            url: base_url + "index.php?controller=test&function=addTest",
            dataType: "json",
            beforeSend: function () {},
            success: function (e) {
                if (e.regFlag == 1) {
                    $("#errTestName").html("Test name cannot be blank").css("color", "red")
                } else if (e.regFlag == 2) {
                    $("#errTestDescription").html("Test Description cannot be blank").css("color", "red")
                } else if (e.regFlag == 3) {
                    $("#errTestDescription").html("Invalid Test Name and Description").css("color", "red")
                } else {
                    $("#errTestName").html("");
                    $("#errTestDescription").html("");
                    if (e.val == 1) {
                        document.location.reload(true)
                    }
                }
            },
            complete: function () {},
            error: function () {}
        })
    });
    $(document).on("click", "#btnEditTest", function (e) {
        var t = $("#txtId").val().trim();
        var n = $("#txtTestName").val().trim();
        var r = $("#txtTestDescription").val().trim();
        $.ajax({
            type: "POST",
            data: {
                txtTestId: t,
                txtTestName: n,
                txtTestDescription: r
            },
            url: base_url + "index.php?controller=test&function=editTest",
            dataType: "json",
            beforeSend: function () {},
            success: function (e) {
                if (e.regFlag == 1) {
                    $("#errTestName").html("Test name cannot be blank").css("color", "red")
                } else if (e.regFlag == 2) {
                    $("#errTestDescription").html("Test Description cannot be blank").css("color", "red")
                } else if (e.regFlag == 3) {
                    $("#errTestDescription").html("Invalid Test Name and Description").css("color", "red")
                } else {
                    $("#errTestName").html("");
                    $("#errTestDescription").html("");
                    if (e.val == 1) {
                        document.location.reload(true)
                    }
                }
            },
            complete: function () {},
            error: function () {}
        })
    });
    $("#oldPassword").blur(function () {
        var e = $(this).attr("id");
        if (isNull(e) === true) {
            $("#errPassMsg3").html("Password cannot be blank.")
        } else {
            $("#errUserName").html("")
        }
    }).focus(function () {
        $("#errUserName").html("")
    });
    $("#newPassword").blur(function () {
        var e = $(this).attr("id");
        if (isNull(e) === true) {
            $("#errPassMsg3").html("Password cannot be blank.")
        } else {
            $("#errUserName").html("")
        }
    }).focus(function () {
        $("#errUserName").html("")
    });
    $("#confirmPassword").blur(function () {
        var e = $(this).attr("id");
        if (isNull(e) === true) {
            $("#errPassMsg3").html("Password cannot be blank.")
        } else {
            $("#errUserName").html("")
        }
    }).focus(function () {
        $("#errUserName").html("")
    });
    $("#btnReset").click(function () {
        $(".regiser-errorTxt").html("")
    });
    $("#ancrRegister").click(function () {
        $("#frmRegister")[0].reset();
        $(".regiser-errorTxt").html("");
        $("#fade").fadeIn();
        $("#light").show()
    });
    $("#closePopup").click(function () {
        $("#fade").hide();
        $("#light").hide()
    });
    $(".jTip").popover({
        placement: "right",
        html: true,
        trigger: "hover",
        delay: {
            show: "300",
            hide: "100"
        }
    });
    $("#ancrForgetPassword").click(function () {
        $("#myModalLabel").html("Generate New Password");
        $.ajax({
            type: "POST",
            url: base_url + "index.php?controller=login&function=forgetPassword",
            data: "",
            dataType: "html",
            success: function (e) {
                $("#modal-body").html(e)
            }
        })
    })
   



});