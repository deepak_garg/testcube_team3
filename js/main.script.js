
		//it is used to validate if a field is null or not
	function isNull(Id){
		var user = "#" + Id;
		var txtVal=$(user).val().trim();
		if(txtVal.length==0){
			return true;
		} else {
			return false;
		}	
	}

		//it is used to validate username
	function validateUserName(Id){
		var user = "#" + Id;
		var txtVal=$(user).val().trim();
		
		if(txtVal.match(/^[a-zA-Z0-9_]{5,20}$/)){
			return true;
		} else {
			return false;	
		}		
	}
	
		//it is used to validate email
	function validateEmail(Id){
		var user = "#" + Id;
		var txtVal=$(user).val().trim();
		if(!txtVal.match(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})$/)){
			return false;
		} else {
			return true;
		}	
	}

		//it is used to validate password
	function validatePassword(Id){
		
		var user = "#" + Id;
		var txtVal=$(user).val().trim();
		if(txtVal.length<5){
			return false;
		} else {
			return true;
		}
	
	}	

		//it is used to validate firstname and lastname
	function validateFirstName(Id){
		
		var user = "#" + Id;
		var txtVal=$(user).val().trim();
		if(txtVal.match(/^([a-zA-Z]{1,20})$/)){
			return true;
		} else {
			return false;
		}
	
	}	

	function validateLastName(Id){
		
			var user = "#" + Id;
			var txtVal=$(user).val().trim();
			if(txtVal.match(/^([a-zA-Z]{0,20})$/)){
				return true;
			} else {
				return false;
			}
	
		}	

	function validateDateFormat(Id){

		var user = "#" + Id;
		var txtVal = $(user).val();
		if(txtVal.length !=10){
			return false;
		} else if(txtVal.match(/^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/)){
			return true;
		} else {
			return false;
		}
	}

	function validateTimeFormat(Id){

		var user="#" + Id;
		var txtVal = $(user).val();
		if(txtVal.length!=7){
			return false;
		} else if(txtVal.match(/^(0?[1-9]|1[012])(:[0-5]\d) [AP][M]/)){
			return true;
		} else {
			return false;
		}
	}
