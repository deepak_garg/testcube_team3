<?php

require_once("phpmailer/class.phpmailer.php"); //includes PHPMailer class

class email
{
	private $username;
	private $password;
	private $from;
	private $fromName;
	private $subject;
	private $to;
	private $body;


    //constructor to initialize the email class variables

	public function __construct($subject,$to,$body='') 
	{

		$this->username = "testcube2014@gmail.com";
		$this->password = "testcube123";
		$this->from = "testcube2014@gmail.com";
		$this->fromName = "Testcube Team";
		$this->subject = $subject;
		$this->to = $to;
		$this->body = $body;

	}

	//function for sending an email
	public function sendEmail() {
	    $mailer = new PHPMailer();
		$mailer->IsSMTP();
		$mailer->Host ='ssl://smtp.gmail.com:465';
		$mailer->SMTPAuth = TRUE;
		$mailer->Username = $this->username;  // Change this to your gmail adress
		$mailer->Password = $this->password;  // Change this to your gmail password
		$mailer->From = $this->from;  // This HAVE TO be your gmail adress
		$mailer->FromName = $this->fromName; // This is the from name in the email, you can put anything you like here
		$mailer->isHTML(true);
		$mailer->Body = $this->body;
		$mailer->Subject = $this->subject;
		$mailer->AddAddress($this->to);  // This is where you put the email adress of the person you want to mail
		$return;


	if(!$mailer->Send()) {
		$return = false;
	} else {
		$return = true;
	}
	 return $return;
	}

}
