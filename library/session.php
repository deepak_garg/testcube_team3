<?php


class Session
{
  
    // it is used to start the session

   function __construct()
    {
        // if no session exist, start the session
	if(!isset($_SESSION)) {
        	session_start();
	}
    }

    
    // it is used to set the session value   
    public function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    // it is used to unset the session value   
    public function remove($key)
    {
        if (isset($_SESSION[$key])) {
            unset($_SESSION[$key]);
        }
      
    }

    
    // it is used to get the session value
    public function get($key)
    {
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        }
        return false;
    }


    // it is used to destroy the session
    public function destroy()
    {
        session_destroy();
    }

}
