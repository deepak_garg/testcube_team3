<?php
function generate_password( $length = 10 ) {
$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
$password = substr( str_shuffle( $chars ), 0, $length );
return $password;
}
function generate_code( $length = 6 ) {
$chars = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz";
$code = substr( str_shuffle( $chars ), 0, $length );
return $code;
}
?>
