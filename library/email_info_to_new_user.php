<?php

	require_once('email.php');

	class sendEmailToNewUser extends email
	{

		private $emailBody;
		private $newUserEmail;
		private $newUserPassword;
		private	$emailUserName;
		private	$emailPasword;
		private	$emailFrom;
		private	$emailFromName;
		private	$emailSubject;

		public function __construct($newUserPassword,$newUserEmail)
		{
			$this->newUserPassword = $newUserPassword;
			$this->newUserEmail = $newUserEmail;
			$this->emailSubject = "Thanks for registering with us : Testcube Team.";
			$this->emailBody = $this->setBody();
			parent::__construct($this->emailSubject,$this->newUserEmail,$this->emailBody);
			
		}

		public function setBody()
		{

			$this->emailBody = "<p>Congratulations from Testcube.<p><br />
						<span> Now you can use our services free of cost.</br></span>
						<span> Your access code is : ".$this->newUserPassword." </br></span>";
							   

			return $this->emailBody;				    
		}

	}


