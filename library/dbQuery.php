<?php 
$createDbQuery = "CREATE SCHEMA IF NOT EXISTS `clientdb0".$clientId."` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;";
$createDbQuery .= "use `clientdb0".$clientId."` ;";
$createDbQuery .= "CREATE USER 'client0".$clientId."'@'localhost' IDENTIFIED BY 'client0".$clientId."'; ";
$createDbQuery .= "GRANT SELECT, INSERT, UPDATE ON clientdb0".$clientId.".* TO 'client0".$clientId."'@'localhost';";
$createDbQuery .= "GRANT SELECT, INSERT, UPDATE ON clientdb0".$clientId.".attempts TO 'client0".$clientId."'@'localhost';";
?>
