<?php 
	
	require_once(CONTROLLER_PATH.'appcontroller.php');

	class clientDbConnection extends AppController {

		public static $conn;
		private $errorMsg;

		public function __construct($clientId) {

			parent::__construct();

			include_once LIBRARY_ROOT . "userconfig.php";
			
			$this->dbHost = $userConfig['host'];
			$this->dbUser = $userConfig['user'];
			$this->dbPass = $userConfig['pass'];
			$dbName = $userConfig['dbase'];
			try {
				
			self::$conn = new PDO ( "mysql:host={$this->dbHost};dbname={$dbName}", $this->dbUser, $this->dbPass );

			} catch ( Exception $e ) {
			// report error : connection error in dashboard model of Testcube
				//echo "bye";

					$this->errorMsg = 'connection error in dashboard model of Testcube';
					$this->errorReportObj->sendErrorReport($this->errorMsg);

				}

		}

	}


