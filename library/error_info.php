<?php

	require_once('email.php');
	 // this class will report the errors
	class emailErrorReport extends email
	{

		private $emailBody;
		private $emailTo;
		private	$emailSubject;

		public function __construct(){}

		public function sendErrorReport($errorToReport)
		{
			$this->emailTo = "testcube2014@gmail.com";
			$this->emailSubject = "Error Notification : Testcube Team.";
			$this->emailBody = $this->setErrorReportBody($errorToReport);
			parent::__construct($this->emailSubject,$this->emailTo,$this->emailBody);			
			$this->sendEmail();			
		}

		 // this function set the body of Error reporting mail
		public function setErrorReportBody($error)
		{

			$emailBody = "<p>Oops! You encountered an error.<p><br />
						<span>An error has been detected.</br></span>
						<span>Error: ".$error." </br></span>";
			return $emailBody;				    
		}

	}


