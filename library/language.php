<?php
//class to reflect the selected language changes
class language
{
      // login page variables
	public $langCode; // stores language code i.e. "en" for English,"fr" for French
	public $textFooter; // stores footer'sontent

	 // login header variabes
	public $testcubeLogo; // stores TESTCUBE text displyed next ot the testcube logo
	public $headerLoginText; // stores "Login" text of the  header tab
	public $headerAboutText; // stores "About" text of the  header tab
	public $headerServiceText; // stores "Service" text of the  header tab
	public $headerHelpText; // stores "Help" text of the  header tab

	  // login panel  variables
	public $loginText; // stores "LOGIN" text of login panel
	public $placeHolderUsername; // stores Username's placeholder text 
	public $placeHolderPassword; // stores Password's placeholder text 
	public $signinText; // stores "Sign In" text 
	public $registerText; // stores "Register" text 
	public $forgetpasswordText; // stores "Forget Password" text 
	public $loginGoogle; // stores "or Login using Google" text 

	 // login error variables
	public $errorWrongCred; // stores "Wrong Credentials" text
	public $errorBlankUsername; // stores blank username error message
	public $errorInvalidUsername; // stores invalid username error message
	public $errorBlankPassword; // stores blank password error message
	public $errorInvalidPassword; // stores invalid password error message	

	 // registration page variables
	public $headingRegisterNewUser; // stores "Register New User" text
	public $labelUsername; // stores Username label text
	public $labelEmailText; // stores Email  label text
	public $labelFirstName; // stores First name label text
	public $labelLastName; // stores Last name label text
	public $placeholderUsername; // stores Username placeholder's text
	public $placeholderEmail; // stores Emails placeholder's text
	public $placeholderFirstname; // stores FirstName placeholder's text
	public $placeholderLasttname; // stores LastName placeholder's text
	public $placeholderRecaptcha;     //placeholder for captcha 
	public $buttonRegister;// stores the text on reset button
	public $buttonReset; // stores the text on reset button

	// register error variables
	public $errorRegBlankUsername; // stores error message for blank Username
	public $errorRegInvalidUsername; // stores error message for invalid Username
	public $errorRegBlankEmail; // stores error message for blank Email
	public $errorRegInvalidEmail;  // stores error message for invalid Email
	public $errorRegBlankFirstName; // stores error message for blank FirstName
	public $errorRegInvalidFirstName; // stores error message for invalid FirstName
	public $errorRegBlankLastName; // stores error message for blank LastName
	public $errorRegInvalidLastName; // stores error message for invalid LastName

	 // Registaration Information Variables
	public $infoUnameInfoHead; // stores heading text of Username-Info
	public $infoUnameInfo1; // stores info 1 for entering username
	public $infoUnameInfo2; // stores info 2 for entering username
	public $infoUnameInfo3; // stores info 3 for entering username
	public $infoUnameInfo4; // stores info 4 for entering username
	public $infoEmailInfoHead; // stores heading text of Email-Info
	public $infoEmailInfo1; // stores info 1 for entering Email
	public $infoEmailInfo2; // stores info 1 for entering Email
	public $infoNameInfoHead; // stores heading text of Email-Info
	public $infoNameInfo1; // stores info 1 for entering Name
	public $infoNameInfo2; // stores info 2 for entering Name
	public $infoNameInfo3; // stores info 3 for entering Name
	public $infoNameInfo4; // stores info 4 for entering Name

	// Forget Password Variables
	public $labelFgtPwdEnterEmail; // stores the lable text for entering email 
	public $placeholderFgtPwd; // stores place holders text to enter an email
	public $butnResetPwd; // stores reset button's text

	 // Dashboard  header variables
	public $textHeaderTestcube; // store the testcube text which is present on the Dashboard's header
	public $textHeaderHelpSupport; // stores help/support text of the Dashboard's header's menu
	public $textHeaderSettings; // stores settings text of the Dashboard's header's menu

	 // Dashboard header's Dropdown's variable
	public $textAccountDetails; // stores account details text of the Dashboard header's Dropdown
	public $textSettings; // stores settings text of the Dashboard header's Dropdown
	public $textPlaneInfo; // stores Plane Info of the Dashboard header's Dropdown
	public $textSignOut; // stores Singn Out text of the Dashboard header's Dropdown

	 // Dashboard right pane variables
	public $textSideBarDashboard; // stores dashboard text for the Dashboard's SideBar
	public $textSideBarSideBarSideBarategory; // stores Category text for the Dashboard's SideBar
	public $textSideBarSideBaruestion; // stores Question text for the Dashboard's SideBar
	public $textSideBarTest; // stores Test text for the Dashboard's SideBar
	public $textSideBarSideBarSideBarResults; // stores Results text for the Dashboard's SideBar
	public $textSideBarSideBarCertificates; // stores Certificates text for the Dashboard's SideBar
	public $textSideBarUsers; // stores users text for the Dashboard's SideBar

	 // Dashboard's  Module Info bar's varibale
	public $textModuleInfo; // stores modules Info text
	public $textModuleName; // Stores module name text i.e. Dashboard, Category etc.

	 // Dashboard's Date-Box varibales
	//public $textMonthName; // stores month name

	 // Dashboard's Donut statistics varibales
	public $textStatHeading; //  stores Statistics heading's text
	public $textStatCategory; // stores Category text in Statistics Donut
	public $textStatQuestion; // stores Question text in Statistics Donut
	public $textStatTest; // stores  Test text in Statistics Donut
	public $textStatResult; // stores Result text in Statistics Donut
	public $textCertificates; // stores Certificates text in Statistics Donut

	 // Dashbboard Graph varibales
	public $textGraphHeading; // stores Graph's heading
	public $textGraphTestElement; // stores graphs's Test element
	public $textGraphResultElement ;// stores graphs's Result element

	 // variable for WEEKDAYS NAME
	public $textSun; // stores Sunday text
	public $textMon; // stores Monday text
	public $textTues; // stores Tuesday text
	public $textWed; // stores Wednesday text
	public $textThus; // stores Thursday text
	public $textFri; // stores Friday text
	public $textSat; // stores Saturday text

	 // Dashboard's Update-Box variables 
	public $textUpadteHeading; // stores updates-box Heading
	public $textUpdateMore; // stores "See AL  Upadtes" text
	public $textCreated; //  stores created text

	 // Dashboard's Recent Box Variables 
	public $textRecentHeading; //  stores Recent-Box Heading
	public $textRecentTest; // stores Recent Test text
	public $textRecentLinks; // stores Recent Links text
	public $textRecentResult; // stores Recent Results text

	 //Category variables
	public $butnCreateTest; // stores the Create Test button's text
	public $captionCategoryName; // stores the Category Name caption's text
	public $captionAction; // stores Action text
	public $butnPrevious; // stores previous button's text
	public $butnNext; // stores Next button's text	


	 // function to initialize the variable according to Engliish language
	function languageEnglish() 
	{
		$this->textFooter = "All rights reserved &copy; OSSCube.com";
		//login page variables
		$this->testcubeLogo = "TestCube";
		$this->headerLoginText = "Login";
		$this->headerAboutText = "About";
		$this->headerServiceText = "Service";
		$this->headerHelpText = "Help";
		$this->loginText = "LOGIN";
		$this->placeHolderUsername = "Enter your Email / Username";
		$this->placeHolderPassword = "Enter your password";
		$this->signinText = "SIGN IN";
		$this->registerText = "Register";
		$this->forgetpasswordText = "Forgot Password?";
		$this->loginGoogle = "or Login using Google";
		// login error variables
		$this->errorWrongCred = "Wrong Credentials";
		$this->errorBlankUsername = "Please enter your Email/Username";
		$this->errorInvalidUsername = "Invalid Email/Username";
		$this->errorBlankPassword = "Please enter password";
		$this->errorInvalidPassword = "Invalid password";
		//registration page variables
		$this->headingRegisterNewUser = "Register New User";
		$this->labelUsername = "Username";
		$this->labelEmailText = "Email";
		$this->labelFirstName = "First Name";
		$this->labelLastName = "Last Name";
		$this->placeholderUsername = "Enter Username";
		$this->placeholderEmail = "Enter Email";
		$this->placeholderFirstname = "Enter First Name";
		$this->placeholderLasttname = "Enter Last Name";
		$this->buttonRegister = "Register";
		$this->buttonReset = "Reset";
		// register error variables
		$this->errorRegBlankUsername = "Please enter your Username";
		$this->errorRegInvalidUsername = "Invalid Username";
	   	$this->errorRegBlankEmail = "Please enter your Email";
	   	$this->errorRegInvalidEmail = "Invalid Email";
		$this->errorRegBlankFirstName = "Please enter your First Name";
		$this->errorRegInvalidFirstName = "Invalid First Name";
		$this->errorRegBlankLastName = "Please enter your Last Name";		
		$this->errorRegInvalidLastName = "Invalid Last Name";
	  	 // Registration Information Variables
		$this->infoUnameInfoHead = "Username must follow these rules:";
		$this->infoUnameInfo1 = "Username must be of atleast 5 characters";
		$this->infoUnameInfo2 = "Username must be of maximum 20 characters";
		$this->infoUnameInfo3 = "Username can contain only underscore(_) as special character";
		$this->infoUnameInfo4 = "Username cannot be the same as the email";
		$this->infoEmailInfoHead = "Email must follow these rules:";
		$this->infoEmailInfo1 =  "Email cannot be left blank";
		$this->infoEmailInfo2 = "Maximum length can be upto 150 characters";
		$this->infoNameInfoHead = "Name must follow these rules";
		$this->infoNameInfo1 = "Name must contain at least one character";
		$this->infoNameInfo2 = "Maximum length of name is upto 20 characters";
		$this->infoNameInfo3 = "Name cannot contain any special characters(@, %,_ etc)";
		$this->infoNameInfo4 = "Mame cannot be the same as the email or username";
		 // Forget Password Variables
		$this->labelFgtPwdEnterEmail = "Enter your Email";
		$this->placeholderFgtPwd = "Enter your Email here";
		$this->btnResetPwd = "Reset Password";
		 // Dashboard  header variables
		$this->textHeaderTestcube = "TestCube"; 
		$this->textHeaderHelpSupport = "HELP/SUPPORT";
		$this->textHeaderSettings = "SETTINGS"; 
		// Dashboard header's Dropdown's variable
		$this->textAccountDetails = "Account Details"; 
		$this->textSettings = "Settings"; 
		$this->textPlaneInfo = "Plan info";
		$this->textSignOut = "Sign Out"; 
		// Dashboard right pane variables
		$this->textSideBarDashboard = "DASHBOARD";
		$this->textSideBarCategory = "CATEGORIES"; 
		$this->textSideBarQuestion = "QUESTIONS"; 
		$this->textSideBarTest = "TESTS"; 
		$this->textSideBarResults = " RESULTS"; 
		$this->textSideBarCertificates = "CERTIFICATES"; 
		$this->textSideBarUsers = "USERS"; 
		// Dashboard's  Module Info bar's varibale
		$this->textModuleInfo = "Information about all modules in one place"; 
		//$this->textModuleName = "DASHBOARD"; 
		// Dashboard's Date-Box varibales
		// $this->textMonthName; // stores month name
		// Dashboard's Donut statistics varibales
		$this->textStatHeading = "TESTCUBE STATISTICS";
		$this->textStatCategory = "Category";
		$this->textStatQuestion = "Questions";
		$this->textStatTest = "Tests"; 
		$this->textStatResult = "Results";
		$this->textCertificates = "Certificates"; 
		// Dashbboard Graph varibales
		$this->textGraphHeading = "Activity Daily Test";
		$this->textGraphTestElement = "Tests"; 
		$this->textGraphResultElement = "Results";
		// variable for WEEKDAYS NAME
		$this->textSun = "Sunday"; 
		$this->textMon = "Monday"; 
		$this->textTues = "Tuesday"; 
		$this->textWed = "Wednesday"; 
		$this->textThus = "Thursday"; 
		$this->textFri = "Friday"; 
		$this->textSat = "Saturday";
		// Dashboard's Update-Box variables 
		$this->textUpadteHeading = "UPDATES";
		$this->textUpdateMore = "SEE ALL UPDATES"; 
		$this->textCreated = "Created"; 
		// Dashboard's Recent Box Variables 
		$this->textRecentHeading = "RECENT"; 
		$this->textRecentTest = "RECENT TESTS"; 
		$this->textRecentLinks = "RECENT LINKS"; 
		$this->textRecentResult = "RECENT RESULTS"; 
		//Category variables
		$this->butnCreateTest = "CREATE CATEGORY";
		$this->captionCategoryName = "Category Name";
		$this->captionAction = "Actions"; 
		$this->butnPrevious = "Previous"; 
		$this->butnNext = "Next"; 
		}


     // function to initialize the variable according to Hindi language
	function languageHindi() 
	{
		$this->textFooter = "सभी अधिकार सुरक्षित © OSSCube.com";
		//login page variables
		$this->testcubeLogo = "टेस्टक्यूब";
		$this->headerLoginText = "लॉगिन";
		$this->headerAboutText = "बारे में";
		$this->headerServiceText = "सेवाएं";
		$this->headerHelpText = "मदद";
		$this->loginText = "लॉगिन";
		$this->placeHolderUsername = "अपना ईमेल / उपयोगकर्ता नाम दर्ज";
		$this->placeHolderPassword = "अपना पासवर्ड दर्ज करें";
		$this->signinText = "साइन इन करें";
		$this->registerText = "रजिस्टर";
		$this->forgetpasswordText = "पासवर्ड भूल गए?";
		$this->loginGoogle = "या गूगल का  लॉगिन उपयोग";
		// login error variables
		$this->errorWrongCred = "गलत साख";
		$this->errorBlankUsername = "अपने उपयोगकर्ता नाम / ईमेल दर्ज करें";
		$this->errorInvalidUsername = "अमान्य उपयोगकर्ता नाम / ईमेल";
		$this->errorBlankPassword = "पासवर्ड दर्ज करें";
		$this->errorInvalidPassword = "अवैध पासवर्ड";
		//registration page variables
		$this->headingRegisterNewUser="नया उपभोक्ता";
		$this->labelUsername = "प्रयोक्ता नाम";

		$this->labelEmailText = "ईमेल  <br>";
		$this->labelFirstName = "प्रथम नाम";
		$this->labelLastName = "अंतिम नाम";
		$this->buttonRegister = "रजिस्टर";
		$this->buttonReset = "रीसेट करें";
		$this->placeholderUsername = "यूजर का नाम लिखें";
		$this->placeholderEmail = "ईमेल दर्ज करें";
		$this->placeholderFirstname = "पहला नाम दर्ज करें";
		$this->placeholderLasttname = "अंतिम नाम दर्ज करें";
		$this->buttonRegister = "रजिस्टर";
		$this->buttonReset = "रीसेट करें";
		// register error variables
		$this->errorRegBlankUsername = "आपके उपयोगकर्ता नाम दर्ज करें";
		$this->errorRegInvalidUsername = "अवैध प्रयोगकर्ता का नाम";
		$this->errorRegBlankEmail = "अपने ईमेल दर्ज करें";
		$this->errorRegInvalidEmail = "अवैध ईमेल";
		$this->errorRegBlankFirstName = "आपका पहला नाम दर्ज करें ";
		$this->errorRegInvalidFirstName = "अवैध पहला नाम";
		$this->errorRegBlankLastName = "आपका अंतिम नाम दर्ज करें ";		
		$this->errorRegInvalidLastName = "अवैध अंतिम नाम";
		 // Registration Information Variables
		$this->infoUnameInfoHead = "यूजर का नाम इन नियमों का पालन करना होगा:";
		$this->infoUnameInfo1 = "यूजर का नाम कम से कम 5 अक्षर का होना चाहिए";
		$this->infoUnameInfo2 = "यूजर का नाम अधिकतम 20 अक्षरों का होना चाहिए";
		$this->infoUnameInfo3 = "उपयोगकर्ता नाम ईमेल के रूप में ही नहीं हो सकता";
		$this->infoUnameInfo4 = "यूजर का नाम ही अंडरस्कोर (_) के रूप में विशेष वर्ण शामिल कर सकते हैं";
		$this->infoEmailInfoHead = "ईमेल इन नियमों का पालन करना होगा:";
		$this->infoEmailInfo1 = "ईमेल खाली नहीं छोड़ा जा सकता";
		$this->infoEmailInfo2 = "अधिकतम लंबाई 150 वर्णों तक हो सकता है";
		$this->infoNameInfoHead = "नाम इन नियमों का पालन करना होगा";
		$this->infoNameInfo1 = "नाम कम से कम एक वर्ण होना चाहिए";
		$this->infoNameInfo2 = "नाम की अधिकतम लंबाई 20 वर्णों तक है";
		$this->infoNameInfo3 = "नाम किसी विशेष वर्ण (@%, _ आदि) को शामिल नहीं कर सकते";
		$this->infoNameInfo4 = "नाम ईमेल या उपयोगकर्ता नाम के रूप में एक ही नहीं हो सकता";
		// Forget Password Variables
        $this->labelFgtPwdEnterEmail = "अपने ईमेल दर्ज करें";
        $this->placeholderFgtPwd = "यहाँ अपना ईमेल दर्ज करें";
        $this->btnResetPwd = "पासवर्ड रीसेट";
		 // Dashboard  header variables
		$this->textHeaderTestcube = "टेस्क्यूब"; 
		$this->textHeaderHelpSupport = "मदद / सहायता";
		$this->textHeaderSettings = "सैटिंग्स";
		// Dashboard header's Dropdown's variable
		$this->textAccountDetails = "खाता विवरण"; 
		$this->textSettings ="सैटिंग्स"; 
		$this->textPlaneInfo = "जानकारी योजना";
		$this->textSignOut = "साइन आउट"; 
		// Dashboard right pane variables
		$this->textSideBarDashboard = "डैशबोर्ड";
		$this->textSideBarCategory = "श्रेणी"; 
		$this->textSideBarQuestion = "सवाल" ;
		$this->textSideBarTest = "टेस्ट"; 
		$this->textSideBarResults = "परिणामों के लिए"; 
		$this->textSideBarCertificates = "प्रमाण पत्र"; 
		$this->textSideBarUsers = "उपयोगकर्ता "; 
		// Dashboard's  Module Info bar's varibale
		$this->textModuleInfo = "एक स्थान में सभी मॉड्यूल के बारे में सूचना"; 
		$this->textModuleName = "डैशबोर्ड"; 
		// Dashboard's Date-Box varibales
		// $this->textMonthName; // stores month name
		// Dashboard's Donut statistics varibales
		$this->textStatHeading = "टेस्क्यूब सांख्यिकी";
		$this->textStatCategory = "श्रेणी";
		$this->textStatQuestion = "सवाल";
		$this->textStatTest  = "टेस्ट"; 
		$this->textStatResult= "परिणामों के लिए";
		$this->textCertificates = "प्रमाण पत्र";
		// Dashbboard Graph varibales
		$this->textGraphHeading = "दैनिक गतिविधि टेस्ट";
		$this->textGraphTestElement = "टेस्ट"; 
		$this->textGraphResultElement = "परिणाम";
		// variable for WEEKDAYS NAME
		$this->textSun = "रविवार";
		$this->textMon = "सोमवार"; 
		$this->textTues = "मंगलवार"; 
		$this->textWed = "बुधवार"; 
		$this->textThus = "बृहस्पतिवार"; 
		$this->textFri = "शुक्रवार";
		$this->textSat = "शनिवार";
		// Dashboard's Update-Box variables 
		$this->textUpdadeHeading = " अद्यतन";
		$this->textUpdateMore = "सभी अद्यतन देखें"; 
		$this->textCreated = "बनाया"; 
		// Dashboard's Recent Box Variables 
		$this->textRecentHeading = "हाल के"; 
		$this->textRecentTest = "हाल के टेस्ट"; 
		$this->textRecentLinks = "हाल के लिंक"; 
		$this->textRecentResult = "हाल के परिणाम"; 
		//Category variables
		$this->butnCreateTest = "श्रेणी बनाने";
		$this->captionCategoryName = "श्रेणी नाम";
		$this->captionAction = "क्रियाएँ"; 
		$this->butnPrevious = "पिछला"; 
		$this->butnNext = "अगला"; 
		}

		
     // function to initialize the variable according to French language
	function languageFrench() 
	{
		$this->textFooter = "Tous droits réservés © OSSCube.com" ;
		//login page variables
		$this->testcubeLogo = "TestCube";
		$this->headerLoginText = "Connexion";
		$this->headerAboutText = "sur";
		$this->headerServiceText = "Service";
		$this->headerHelpText = "aider";
		$this->loginText = "CONNEXION";
		$this->placeHolderUsername = "Entrez votre e-mail / nom d'utilisateur";
		$this->placeHolderPassword = "Entrez votre mot de passe";
		$this->signinText = "CONNECTER";
		$this->registerText = "enregistrer";
		$this->forgetpasswordText = "Mot de passe oublié?";
		$this->loginGoogle = "ou Connectez-vous avec Google";
		// login error variables
		$this->errorWrongCred = "Mauvaise vérification des pouvoirs";
		$this->errorBlankUsername = "Votre nom d'utilisateur / Email Entrez";
		$this->errorInvalidUsername = "UserName invalide / Email";
		$this->errorBlankPassword = "S'il vous plaît entrez le mot de passe";
		$this->errorInvalidPassword = "Mot de passe incorrect";
		//registration page variables
		$this->headingRegisterNewUser = "S'inscrire Nouvel utilisateur";
		$this->labelUsername = "Nom d'utilisateur";
		$this->labelEmailText = "Email";
		$this->labelFirstName = "Prénom";
		$this->labelLastName = "nomFamille";
		$this->placeholderUsername = "Entrez Nom d'utilisateur";
		$this->placeholderEmail = "Entrez votre email";
		$this->placeholderFirstname = "Entrez le prénom";
		$this->placeholderLasttname = "Entrez le nom";
		$this->buttonRegister = "Enregistrer";
		$this->buttonReset = "Remettre";
		// register error variables
		$this->errorRegBlankUsername = "S'il vous plaît entrez votre nom d'utilisateur";
		$this->errorRegInvalidUsername = "Invalid UserName";
		$this->errorRegBlankEmail = "S'il vous plaît, entrez votre e-mail";
		$this->errorRegInvalidEmail = "Courriel invalide";
		$this->errorRegBlankFirstName = "S'il vous plaît, entrez votre Prénom";
		$this->errorRegInvalidFirstName = "Invalide Prénom";
		$this->errorRegBlankLastName = "S'il vous plaît entrez votre nom de famille";		
		$this->errorRegInvalidLastName = "Nom invalide";
		 // Registration Information Variables
		$this->infoUnameInfoHead = "Nom d'utilisateur doit suivre les règles suivantes:";
		$this->infoUnameInfo1= "nom d'utilisateur doit être atleast 5 caractères";
		$this->infoUnameInfo2 = "nom d'utilisateur doit être d'un maximum de 20 caractères";
		$this->infoUnameInfo3 = "nom d'utilisateur ne peut pas être le même que l'e-mail";
		$this->infoUnameInfo4 = "nom d'utilisateur ne peut contenir qu'un trait de soulignement (_) de caractère spécial";
		$this->infoEmailInfoHead = "email doit suivre ces règles:";
		$this->infoEmailInfo1 = "e-mail ne peut pas être laissé en blanc";
		$this->infoEmailInfo2 = "longueur maximale peut être jusqu'à 150 caractères";
		$this->infoNameInfoHead = "Le nom doit suivre ces règles";
		$this->infoNameInfo1 = "nom doit contenir au moins un caractère";
		$this->infoNameInfo2 = "longueur maximale du nom est jusqu'à 20 caractères";
		$this->infoNameInfo3 = "nom ne peut pas contenir de caractères spéciaux (@,%, _ etc)";
		$this->infoNameInfo4 = "nom ne peut pas être le même que l'e-mail ou nom d'utilisateur";
		// Forget Password Variables
        $this->labelFgtPwdEnterEmail = "Entrez votre e-mail";
        $this->placeholderFgtPwd = "Entrez votre e-mail ici";
        $this->btnResetPwd = "réinitialiser le mot de";
		 // Dashboard  header variables
		$this->textHeaderTestcube = "TestCube"; 
		$this->textHeaderHelpSupport = "AIDE / SUPPORT";
		$this->textHeaderSettings = "REGLAGES"; 
		// Dashboard header's Dropdown's variable
		$this->textAccountDetails = "Détails du compte"; 
		$this->textSettings = "reglages"; 
		$this->textPlaneInfo = "Planifiez infos";
		$this->textSignOut = "Déconnexion"; 
		// Dashboard right pane variables
		$this->textSideBarDashboard = "TABLEAU DE BORD";
		$this->textSideBarCategory = "CATÉGORIE";
		$this->textSideBarQuestio = "QUESTIONS" ;
		$this->textSideBarTest = "ESSAIS"; 
		$this->textSideBarResult = "RÉSULTATS";
		$this->textSideBarCertificates = "CERTIFICATS"; 
		$this->textSideBarUsers = "UTILISATEURS";
		// Dashboard's  Module Info bar's varibale
		$this->textModuleInfo = "Informations sur tous les modules en un seul endroit"; 
		$this->textModuleName = "TABLEAU DE BORD"; 
		// Dashboard's Date-Box varibales
		// $this->textMonthName; // stores month name
		// Dashboard's Donut statistics varibales
		$this->textStatHeading = "TestCube STATISTIQUES";
		$this->textStatCategory = "CATÉGORIE";
		$this->textStatQuestion = "QUESTIONS" ;
		$this->textStatTest = "ESSAIS"; 
		$this->textStatResult = "RÉSULTATS";
		$this->textCertificates = "CERTIFICATS"; 
		// Dashbboard Graph varibales
		$this->textGraphHeading = "Test d'activité quotidien";
		$this->textGraphTestElement = "Essais"; 
		$this->textGraphResultElement = "Resultats";
		// variable for WEEKDAYS NAME
		$this->textSun = "dimanche";
		$this->textMon = "lundi"; 
		$this->textTues = "mardi"; 
		$this->textWed = "mercredi"; 
		$this->textThus = "jeudi"; 
		$this->textFri = "vendredi"; 
		$this->textSat = "samedi";
		// Dashboard's Update-Box variables 
		$this->textUpdateHeading = "MISES À JOUR";
		$this->textUpdateMore = "Voir toutes les mises"; 
		$this->textCreated = "établi"; 
		// Dashboard's Recent Box Variables 
		$this->textRecentHeading = "RÉCENTE"; 
		$this->textRecentTest = "Des tests récents"; 
		$this->textRecentLinks = "Liens récents"; 
		$this->textRecentResult = "Les résultats récents"; 
		//Category variables
		$this->butnCreateTest = "Créer une catégorie";
		$this->captionCategoryName = "Nom de la catégorie";
		$this->captionAction = "Actes"; 
		$this->butnPrevious = "Précédent"; 
		$this->butnNext = "Suivant"; 
		}		
		
	 // function to initialize the language code
	function __construct($code)
	{
  		if (isset($code) && !empty($code)) {
  			$this->langCode = $code;       
  		} else {
  			$this->langCode = 'en';
  		}
		$this->getLanguage();
	}
	 // funciton for getting the selected language
	function getLanguage() 
	{
		if ($this->langCode == 'hn') {
		$this->languageHindi();
		} else if ($this->langCode == 'fr') {
		$this->languageFrench();
		} else {
		$this->languageEnglish();
		} 

	}
} 












