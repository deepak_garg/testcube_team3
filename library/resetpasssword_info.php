<?php

	require_once('email.php');

	class resetPasswordMail extends email
	{

		private $emailBody;
		private $newUserEmail;
		private $newUserPassword;
		private	$emailUserName;
		private	$emailPasword;
		private	$emailFrom;
		private	$emailFromName;
		private	$emailSubject;

		public function __construct($newUserPassword,$newUserEmail)
		{
			$this->newUserPassword = $newUserPassword;
			$this->newUserEmail = $newUserEmail;
			$this->emailSubject = "New Password Reset: Testcube Team.";
			$this->emailBody = $this->setBody();
			parent::__construct($this->emailSubject,$this->newUserEmail,$this->emailBody);
			
		}

		public function setBody()
		{

			$this->emailBody = "<p>Hey! Your password has been resetted successsfully.<p><br/>						
						<span> Your new password is : ".$this->newUserPassword." </br></span>";
							   

			return $this->emailBody;				    
		}

	}


